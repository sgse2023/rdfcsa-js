# RDFCSA-JS

This repository contains an implementation of the RDFCSA RDF-Store data structure,
and an accompanying visualisation tool.

## Running

See [USAGE.md](USAGE.md) for details on running the visualisation tool.

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md) for details on how development of the project is organized.

## READMEs

In addition to this README, most subdirectories include another README,
describing their purpose in more detail.
