# Snapshot report for `test/query/QueryExecute.mjs`

The actual snapshot is saved in `QueryExecute.mjs.snap`.

Generated by [AVA](https://avajs.dev).

## SingleTripleFound

> Snapshot 1

    RdfGraphData {
      links: [
        Triple {
          id: 0,
          name: 'filmed in',
          object: 12,
          predicateId: 9,
          subject: 0,
        },
      ],
      nodes: [
        SubjectObject {
          id: 0,
          name: 'Inception',
        },
        SubjectObject {
          id: 12,
          name: 'L.A.',
        },
      ],
    }

## OneBoundFound

> Snapshot 1

    RdfGraphData {
      links: [
        Triple {
          id: 0,
          name: 'filmed in',
          object: 1,
          predicateId: 9,
          subject: 0,
        },
      ],
      nodes: [
        SubjectObject {
          id: 0,
          name: 'Inception',
        },
        SubjectObject {
          id: 1,
          name: 'L.A.',
        },
      ],
    }

## TwoBoundFound

> Snapshot 1

    RdfGraphData {
      links: [
        Triple {
          id: 0,
          name: 'born in',
          object: 15,
          predicateId: 7,
          subject: 3,
        },
        Triple {
          id: 1,
          name: 'born in',
          object: 15,
          predicateId: 7,
          subject: 4,
        },
      ],
      nodes: [
        SubjectObject {
          id: 3,
          name: 'J.Gordon',
        },
        SubjectObject {
          id: 15,
          name: 'USA',
        },
        SubjectObject {
          id: 4,
          name: 'L.DiCaprio',
        },
      ],
    }

## TwoBoundNotFound

> Snapshot 1

    RdfGraphData {
      links: [],
      nodes: [],
    }

## FullGraph

> Snapshot 1

    RdfGraphData {
      links: [
        Triple {
          id: 0,
          name: 'filmed in',
          object: 1,
          predicateId: 9,
          subject: 0,
        },
        Triple {
          id: 1,
          name: 'city of',
          object: 15,
          predicateId: 8,
          subject: 1,
        },
        Triple {
          id: 2,
          name: 'appears in',
          object: 0,
          predicateId: 5,
          subject: 2,
        },
        Triple {
          id: 3,
          name: 'born in',
          object: 13,
          predicateId: 7,
          subject: 2,
        },
        Triple {
          id: 4,
          name: 'appears in',
          object: 0,
          predicateId: 5,
          subject: 3,
        },
        Triple {
          id: 5,
          name: 'born in',
          object: 15,
          predicateId: 7,
          subject: 3,
        },
        Triple {
          id: 6,
          name: 'lives in',
          object: 1,
          predicateId: 10,
          subject: 3,
        },
        Triple {
          id: 7,
          name: 'appears in',
          object: 0,
          predicateId: 5,
          subject: 4,
        },
        Triple {
          id: 8,
          name: 'awarded',
          object: 14,
          predicateId: 6,
          subject: 4,
        },
        Triple {
          id: 9,
          name: 'born in',
          object: 15,
          predicateId: 7,
          subject: 4,
        },
      ],
      nodes: [
        SubjectObject {
          id: 0,
          name: 'Inception',
        },
        SubjectObject {
          id: 1,
          name: 'L.A.',
        },
        SubjectObject {
          id: 15,
          name: 'USA',
        },
        SubjectObject {
          id: 2,
          name: 'E.Page',
        },
        SubjectObject {
          id: 13,
          name: 'Canada',
        },
        SubjectObject {
          id: 3,
          name: 'J.Gordon',
        },
        SubjectObject {
          id: 4,
          name: 'L.DiCaprio',
        },
        SubjectObject {
          id: 14,
          name: 'Oscar2015',
        },
      ],
    }

## BasicJoin

> Snapshot 1

    RdfGraphData {
      links: [
        Triple {
          id: 0,
          name: 'appears in',
          object: 0,
          predicateId: 5,
          subject: 3,
        },
        Triple {
          id: 1,
          name: 'appears in',
          object: 0,
          predicateId: 5,
          subject: 4,
        },
        Triple {
          id: 2,
          name: 'born in',
          object: 15,
          predicateId: 7,
          subject: 3,
        },
        Triple {
          id: 3,
          name: 'born in',
          object: 15,
          predicateId: 7,
          subject: 4,
        },
      ],
      nodes: [
        SubjectObject {
          id: 3,
          name: 'J.Gordon',
        },
        SubjectObject {
          id: 0,
          name: 'Inception',
        },
        SubjectObject {
          id: 4,
          name: 'L.DiCaprio',
        },
        SubjectObject {
          id: 15,
          name: 'USA',
        },
      ],
    }

## BasicJoinNotFound

> Snapshot 1

    RdfGraphData {
      links: [],
      nodes: [],
    }

## BasicJoinDifferentElement

> Snapshot 1

    RdfGraphData {
      links: [
        Triple {
          id: 0,
          name: 'filmed in',
          object: 1,
          predicateId: 9,
          subject: 0,
        },
        Triple {
          id: 1,
          name: 'appears in',
          object: 0,
          predicateId: 5,
          subject: 2,
        },
        Triple {
          id: 2,
          name: 'appears in',
          object: 0,
          predicateId: 5,
          subject: 3,
        },
        Triple {
          id: 3,
          name: 'appears in',
          object: 0,
          predicateId: 5,
          subject: 4,
        },
      ],
      nodes: [
        SubjectObject {
          id: 0,
          name: 'Inception',
        },
        SubjectObject {
          id: 1,
          name: 'L.A.',
        },
        SubjectObject {
          id: 2,
          name: 'E.Page',
        },
        SubjectObject {
          id: 3,
          name: 'J.Gordon',
        },
        SubjectObject {
          id: 4,
          name: 'L.DiCaprio',
        },
      ],
    }

## BasicJoinMerge

> Snapshot 1

    RdfGraphData {
      links: [
        Triple {
          id: 0,
          name: 'appears in',
          object: 0,
          predicateId: 5,
          subject: 3,
        },
        Triple {
          id: 1,
          name: 'appears in',
          object: 0,
          predicateId: 5,
          subject: 4,
        },
        Triple {
          id: 2,
          name: 'born in',
          object: 15,
          predicateId: 7,
          subject: 3,
        },
        Triple {
          id: 3,
          name: 'born in',
          object: 15,
          predicateId: 7,
          subject: 4,
        },
      ],
      nodes: [
        SubjectObject {
          id: 3,
          name: 'J.Gordon',
        },
        SubjectObject {
          id: 0,
          name: 'Inception',
        },
        SubjectObject {
          id: 4,
          name: 'L.DiCaprio',
        },
        SubjectObject {
          id: 15,
          name: 'USA',
        },
      ],
    }

## BasicJoinNotFoundMerge

> Snapshot 1

    RdfGraphData {
      links: [],
      nodes: [],
    }

## BasicJoinLeftChaining

> Snapshot 1

    RdfGraphData {
      links: [
        Triple {
          id: 0,
          name: 'born in',
          object: 15,
          predicateId: 7,
          subject: 3,
        },
        Triple {
          id: 1,
          name: 'born in',
          object: 15,
          predicateId: 7,
          subject: 4,
        },
        Triple {
          id: 2,
          name: 'appears in',
          object: 0,
          predicateId: 5,
          subject: 3,
        },
        Triple {
          id: 3,
          name: 'appears in',
          object: 0,
          predicateId: 5,
          subject: 4,
        },
      ],
      nodes: [
        SubjectObject {
          id: 2,
          name: 'E.Page',
        },
        SubjectObject {
          id: 0,
          name: 'Inception',
        },
        SubjectObject {
          id: 3,
          name: 'J.Gordon',
        },
        SubjectObject {
          id: 4,
          name: 'L.DiCaprio',
        },
        SubjectObject {
          id: 15,
          name: 'USA',
        },
      ],
    }

## BasicJoinNotFoundLeftChaining

> Snapshot 1

    RdfGraphData {
      links: [],
      nodes: [],
    }

## BasicJoinDifferentElementLeftChaining

> Snapshot 1

    RdfGraphData {
      links: [
        Triple {
          id: 0,
          name: 'appears in',
          object: 0,
          predicateId: 5,
          subject: 2,
        },
        Triple {
          id: 1,
          name: 'appears in',
          object: 0,
          predicateId: 5,
          subject: 3,
        },
        Triple {
          id: 2,
          name: 'appears in',
          object: 0,
          predicateId: 5,
          subject: 4,
        },
        Triple {
          id: 3,
          name: 'filmed in',
          object: 1,
          predicateId: 9,
          subject: 0,
        },
      ],
      nodes: [
        SubjectObject {
          id: 0,
          name: 'Inception',
        },
        SubjectObject {
          id: 1,
          name: 'L.A.',
        },
        SubjectObject {
          id: 2,
          name: 'E.Page',
        },
        SubjectObject {
          id: 3,
          name: 'J.Gordon',
        },
        SubjectObject {
          id: 4,
          name: 'L.DiCaprio',
        },
      ],
    }

## BasicJoinRightChaining

> Snapshot 1

    RdfGraphData {
      links: [
        Triple {
          id: 0,
          name: 'appears in',
          object: 11,
          predicateId: 5,
          subject: 3,
        },
        Triple {
          id: 1,
          name: 'appears in',
          object: 11,
          predicateId: 5,
          subject: 4,
        },
        Triple {
          id: 2,
          name: 'born in',
          object: 15,
          predicateId: 7,
          subject: 3,
        },
        Triple {
          id: 3,
          name: 'born in',
          object: 15,
          predicateId: 7,
          subject: 4,
        },
      ],
      nodes: [
        SubjectObject {
          id: 3,
          name: 'J.Gordon',
        },
        SubjectObject {
          id: 15,
          name: 'USA',
        },
        SubjectObject {
          id: 4,
          name: 'L.DiCaprio',
        },
        SubjectObject {
          id: 11,
          name: 'Inception',
        },
      ],
    }

## BasicJoinNotFoundRightChaining

> Snapshot 1

    RdfGraphData {
      links: [],
      nodes: [],
    }

## BasicJoinDifferentElementRightChaining

> Snapshot 1

    RdfGraphData {
      links: [
        Triple {
          id: 0,
          name: 'filmed in',
          object: 12,
          predicateId: 9,
          subject: 0,
        },
        Triple {
          id: 1,
          name: 'appears in',
          object: 0,
          predicateId: 5,
          subject: 2,
        },
        Triple {
          id: 2,
          name: 'appears in',
          object: 0,
          predicateId: 5,
          subject: 3,
        },
        Triple {
          id: 3,
          name: 'appears in',
          object: 0,
          predicateId: 5,
          subject: 4,
        },
      ],
      nodes: [
        SubjectObject {
          id: 2,
          name: 'E.Page',
        },
        SubjectObject {
          id: 0,
          name: 'Inception',
        },
        SubjectObject {
          id: 3,
          name: 'J.Gordon',
        },
        SubjectObject {
          id: 4,
          name: 'L.DiCaprio',
        },
        SubjectObject {
          id: 12,
          name: 'L.A.',
        },
      ],
    }

## TripleJoinDifferentElement

> Snapshot 1

    RdfGraphData {
      links: [
        Triple {
          id: 0,
          name: 'appears in',
          object: 0,
          predicateId: 5,
          subject: 4,
        },
        Triple {
          id: 1,
          name: 'filmed in',
          object: 1,
          predicateId: 9,
          subject: 0,
        },
        Triple {
          id: 2,
          name: 'city of',
          object: 15,
          predicateId: 8,
          subject: 1,
        },
      ],
      nodes: [
        SubjectObject {
          id: 4,
          name: 'L.DiCaprio',
        },
        SubjectObject {
          id: 0,
          name: 'Inception',
        },
        SubjectObject {
          id: 1,
          name: 'L.A.',
        },
        SubjectObject {
          id: 15,
          name: 'USA',
        },
      ],
    }

## TripleJoin

> Snapshot 1

    RdfGraphData {
      links: [
        Triple {
          id: 0,
          name: 'filmed in',
          object: 1,
          predicateId: 9,
          subject: 0,
        },
        Triple {
          id: 1,
          name: 'appears in',
          object: 0,
          predicateId: 5,
          subject: 4,
        },
        Triple {
          id: 2,
          name: 'appears in',
          object: 0,
          predicateId: 5,
          subject: 2,
        },
      ],
      nodes: [
        SubjectObject {
          id: 0,
          name: 'Inception',
        },
        SubjectObject {
          id: 1,
          name: 'L.A.',
        },
        SubjectObject {
          id: 4,
          name: 'L.DiCaprio',
        },
        SubjectObject {
          id: 2,
          name: 'E.Page',
        },
      ],
    }
