// @ts-checked

// always use posix semantics for file operations
// (cf. https://nodejs.org/api/path.html#windows-vs-posix)
import path from 'path';

import { build } from 'esbuild';

import config from './config.mjs';
import { cleanBuildDir, insertScriptLoading } from './helper.mjs';

// clean build directories

await cleanBuildDir(config.buildOut);

// finally: run esbuild

const fixN3LoadingPlugin = {
    name: 'aah!',
    setup(build) {
        build.onResolve({ filter: /^n3$/ }, async (args) => {
            if (args.pluginData) {
                return;
            }
            const { ...rest } = args;
            delete rest.path;
            rest.pluginData = true; // prevent infinite recursion

            const result = await build.resolve('n3', {
                kind: 'import-statement',
                ...rest,
            });
            if (result.errors.length > 0) {
                return { errors: result.errors };
            }

            const n3 = path.join(result.path, '..', '..', 'browser', 'n3.min.js');

            return { path: n3 };
        });
    },
};

const result = await build({
    entryPoints: [config.bootstrapFile],
    outdir: config.buildOut,
    // allow source code to be readable in the browser
    sourcemap: 'linked',
    // do all optimizations that do not actually remove code (i.e. tree shaking)
    minify: true,
    bundle: true,
    splitting: true,
    treeShaking: true,
    // transpile the dependencies to work with the required browser versions
    platform: 'browser',
    target: ['chrome67', 'firefox68', 'edge79', 'safari16'],
    format: 'esm',
    // the files are es-modules, so name them appropriately
    outExtension: { '.js': '.mjs' },
    // allow to use css files as Constructed Stylesheets via Polyfill
    // (and: load them globally (see helper.insertScriptLoading)
    loader: { '.css': 'copy' },
    metafile: true,
    define: {
        // remove trailing slash
        PAGE_ROOT: `"${config.scriptLoading.pageRoot.slice(0, -1)}"`,
    },
    plugins: [fixN3LoadingPlugin],
});

// and last but not least: insert the code for loading the scripts
// into an html file
await insertScriptLoading(config, result.metafile.outputs);
