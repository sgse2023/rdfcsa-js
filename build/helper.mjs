// @ts-checked

import * as fs from 'fs/promises';
// always use posix semantics for file operations
// (cf. https://nodejs.org/api/path.html#windows-vs-posix)
import * as _path from 'path';
const path = _path.posix;

const PACKAGE_JSON = JSON.parse(await fs.readFile('package.json'));

async function cleanBuildDir(dir) {
    try {
        const files = await fs.readdir(dir);
        await Promise.all(
            files.map(async (file) => {
                try {
                    await fs.unlink(path.join(dir, file));
                } catch {
                    console.error(`Failed to delete '${file}'`);
                }
            }),
        );
    } catch {
        console.warn(`${dir} does not exist. creating...`);
        await fs.mkdir(dir);
    }
}

/**
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#escaping
 * Thanks MDN contributors :)
 */
function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}

/**
 * @param {string} cssFileName file name of the css without extensions
 * @param {import('./config.mjs').ConfigOptions} options
 * @param {Record<string, any>} outputs
 * @returns string
 */
function insertGlobalCss(cssFileName, options, outputs) {
    let cssLoading = '';

    // regex to account for the hash added by esbuild
    // prefix because we check for output files
    const regExp = RegExp(`^${path.join(options.buildOut, cssFileName)}-.*.css$`);
    const holidayCssDist = Object.keys(outputs).filter((filename) => filename.match(regExp));

    if (holidayCssDist.length === 0) {
        console.warn(`Found no output file for loading global css '${cssFileName}'. Skipping...`);
    } else {
        if (holidayCssDist.length > 1) {
            console.warn(
                `Found more than one candidate for loading global css '${cssFileName}'. Loading all...`,
            );
        }
        cssLoading = holidayCssDist.reduce(
            (loading, file) => `${loading}
        <link type="text/css" href="${options.scriptLoading.pageRoot}${file}" rel="stylesheet" />`,
            '',
        );
    }

    return cssLoading;
}

/**
 * @param options {import('./config.mjs').ConfigOptions}
 * @param outputs {Record<string, any>}
 */
async function insertScriptLoading(options, outputs) {
    // FIXME(Florian) the indentations of the generated code are often wrong.
    // Since in this case the code lands in index.html,
    // it's necessary to run `npm run format` after `npm run build`

    const cssLoading = options.scriptLoading.globalCssFileNames.reduce(
        (loading, fileName) => loading + insertGlobalCss(fileName, options, outputs),
        '',
    );

    let loadingHtml = `${options.scriptLoading.startTag}${cssLoading}

        <script type="esms-options">
            ${JSON.stringify(options.scriptLoading.esmsOptions)}
        </script>
`;

    await Promise.all(
        options.shimPkgs.map(async (pkg) => {
            if (!(pkg in PACKAGE_JSON.dependencies)) {
                console.warn(`'${pkg} listed in 'shimPkgs', but not in package.json. Ignoring!`);
                return;
            }

            const pkgLoc = `./node_modules/${pkg}`;
            const shimMainLoc = JSON.parse(
                await fs.readFile(path.join(pkgLoc, 'package.json')),
            ).main;
            const shimMain = path.basename(shimMainLoc);
            fs.copyFile(path.join(pkgLoc, shimMainLoc), `${options.buildOut}/${shimMain}`);

            loadingHtml += `
        <script async src="${options.scriptLoading.pageRoot}${options.buildOut}/${shimMain}"></script>`;
        }),
    );

    const importmap = {
        imports: {
            rdfcsaBootstrap: `${options.scriptLoading.pageRoot}${options.buildOut}/${path.basename(
                options.bootstrapFile,
            )}`,
        },
    };

    loadingHtml += `

        <script type="importmap-shim">
            ${JSON.stringify(importmap)}
        </script>
        <script type="module-shim" src="${options.scriptLoading.pageRoot}${
        options.buildOut
    }/rdfcsa-bootstrap.mjs"></script>
        <!-- since the polyfill does not share the modules loaded by the browser, import a bootstrap
            module that then pulls in the real startup script via the polyfill -->
        ${options.scriptLoading.endTag}`;

    const originalHtml = await fs.readFile(options.scriptLoading.targetHtmlFile, {
        encoding: 'utf-8',
    });
    await fs.copyFile(
        options.scriptLoading.targetHtmlFile,
        `${options.scriptLoading.targetHtmlFile}.original`,
    );
    // match any character that either is a space, or is a not a space (.* would not match newlines)
    const regex = new RegExp(
        `${escapeRegExp(options.scriptLoading.startTag)}[\\s\\S]*${escapeRegExp(
            options.scriptLoading.endTag,
        )}`,
    );
    await fs.writeFile(
        options.scriptLoading.targetHtmlFile,
        originalHtml.replace(regex, loadingHtml),
    );

    const bootstrapContent = `
        // import for sideffects only
        import 'rdfcsaBootstrap';
    `;
    await fs.writeFile(`${options.buildOut}/rdfcsa-bootstrap.mjs`, bootstrapContent);
}

export { cleanBuildDir, insertScriptLoading };
