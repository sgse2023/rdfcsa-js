# Build Helpers

This directory contains the scripts and modules that customise linting/type checking/build
functionality.

A large part of the code is responsible for bundling our code with the dependencies
and loading stylesheets and our bootstrap file in [index.html](../index.html).
