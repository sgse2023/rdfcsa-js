// @ts-check

module.exports = {
    meta: {
        type: 'problem',
    },
    create: function (context) {
        return {
            Program: function (ast) {
                if (ast.body.length > 0) {
                    const comments = context.getSourceCode().getCommentsBefore(ast.body[0]);
                    if (
                        comments.filter((comment) => comment.value.includes('@ts-check')).length ===
                        0
                    ) {
                        context.report({
                            message: 'Missing @ts-check comment at beginning of file',
                            node: ast,
                        });
                    }
                }
            },
        };
    },
};
