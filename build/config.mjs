// @ts-check

/**
 * @typedef {object} ScriptLoadingOptions
 * @property pageRoot {string}
 * @property targetHtmlFile {string}
 * @property startTag {string}
 * @property endTag {string}
 * @property globalCssFileNames {Array<string>}
 * @property esmsOptions {Record<string, any>} These options are passed through to es-module-shims as an inline JSON array.
 */

/**
 * @typedef {object} ConfigOptions
 * @property buildOut {string}
 * @property bootstrapFile {string}
 * @property scriptLoading {ScriptLoadingOptions}
 * @property shimPkgs {Array<string>}
 */

/**
 * @type {ConfigOptions}
 *
 * Note to Florian, because he keeps forgetting it:
 * You need to run `npm run build` after changing stuff here!
 *
 * Note: Relative paths are supposed to be relative to the
 * root directory of the repo (i.e. where `package.json` is located).
 * (The fact they *are*, might just be a side effect of build
 * being called with cwd==root of repo).
 */
export default {
    buildOut: './dist',
    /**
     * Imported for side effects only.
     * Must import all code that needs to be bundled!
     */
    bootstrapFile: './js/bootstrap.mjs',
    shimPkgs: ['es-module-shims', 'construct-style-sheets-polyfill'],
    scriptLoading: {
        /**
         * Must end with a slash!
         */
        pageRoot: process.env['PAGE_ROOT'] || '/',
        targetHtmlFile: './index.html',
        startTag: '<!-- generated content -->',
        endTag: '<!-- generated content end -->',
        globalCssFileNames: ['holiday', 'custom', 'dialog-polyfill'],
        esmsOptions: {
            shimMode: true,
        },
    },
};
