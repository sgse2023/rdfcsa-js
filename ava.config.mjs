const inCi = 'CI' in process.env;

export default {
    files: ['test/**/*.mjs', '!test/lib'],
    failFast: inCi,
    cache: !inCi,
    snapshotDir: './testSnapshots',
};
