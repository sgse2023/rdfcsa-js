// @ts-check

import { LitElement, html } from 'lit';
import { createRef, ref } from 'lit/directives/ref.js';
import { ContextConsumer } from '@lit-labs/context';
import holidayCss from 'holiday.css';

import './polyfill.mjs';
import { nonNull } from '../lib/assertions.mjs';
import {
    dialogPolyfilledElement,
    htmlAnchorElement,
    htmlDialogElement,
    htmlInputElement,
} from '../assertions-frontend.mjs';
import { NTriplesFormat } from '../lib/conversion/NTriplesFormat.mjs';
import { TurtleFormat } from '../lib/conversion/TurtleFormat.mjs';
import { rdfGraphData as graphDataContext, rdfStore as storeContext } from '../context.mjs';
import { repeat } from 'lit/directives/repeat.js';
import { Exporter } from '../lib/conversion/Exporter.mjs';
import { QUERY_ALL } from '../lib/query/SimpleQuery.mjs';

/** @typedef {import('./polyfill.mjs').DialogPolyfilled} DialogPolyfilled */
/** @typedef {import('../lib/conversion/Export.d.ts').Export} IExport */

export class Export extends LitElement {
    /**
     * @override
     */
    static styles = [holidayCss];

    dialogRef = createRef();

    applyQuery = createRef();

    aRef = createRef();

    #graphData = new ContextConsumer(this, {
        context: graphDataContext,
        subscribe: true,
    });
    #rdfStore = new ContextConsumer(this, {
        context: storeContext,
        subscribe: true,
    });

    /**
     * @override
     */
    static properties = {
        format: { type: Object, state: true },
    };

    #supportedFormats = [new NTriplesFormat(), new TurtleFormat()];

    constructor() {
        super();
        /**
         * @type {IExport | null}
         */
        this.format = null;
    }

    /**
     * @override
     */
    render() {
        const userInputValid = this.format;

        const supportedFormats = this.#supportedFormats.concat(
            this.#rdfStore.value ? [this.#rdfStore.value] : [],
        );

        return html`
            <pf-dialog id="export-dialog" ${ref(this.dialogRef)}>
                <form>
                    <h1>Export</h1>
                    <fieldset @change=${this.#formatChanged}>
                        <legend>Format:</legend>
                        ${repeat(
                            supportedFormats,
                            (format) => format.mimetype,
                            (format) => html`
                                <div>
                                    <label
                                        >${format.formatName}:
                                        <input
                                            type="radio"
                                            name="format"
                                            value=${format.mimetype}
                                        />
                                    </label>
                                </div>
                            `,
                        )}
                    </fieldset>
                    <label
                        >Apply current query?
                        <input ${ref(this.applyQuery)} type="checkbox" checked />
                    </label>
                    <div>
                        <a ${ref(this.aRef)} href="/internal-error" style="display:none">
                            Auto-Download Helper
                        </a>
                        <button @click=${this.#cancelExport}>Cancel</button>
                        <button
                            @click=${this.#doExport}
                            id="import-button"
                            value="default"
                            ?disabled="${!userInputValid}"
                        >
                            Export
                        </button>
                    </div>
                </form>
            </pf-dialog>
        `;
    }

    /**
     * @param {Event} e
     */
    #formatChanged(e) {
        const input = htmlInputElement(nonNull(/** @type {Element} */ (e.target)));
        const rdfstoreFormat =
            input.value === this.#rdfStore.value?.mimetype ? this.#rdfStore.value : null;
        this.format =
            this.#supportedFormats.find((f) => f.mimetype === input.value) ?? rdfstoreFormat;
    }

    /**
     * @param {Event} event
     */
    #cancelExport(event) {
        event.preventDefault(); // do not submit fake form

        htmlDialogElement(
            nonNull(dialogPolyfilledElement(nonNull(this.dialogRef.value)).dialogRef.value),
        ).close();
    }

    /**
     * @param {Event} event
     */
    async #doExport(event) {
        event.preventDefault(); // do not submit fake form

        // graphData should always have a value after the page
        // has initially loaded.
        // This is always the case when the import dialog is opened.
        const applyQuery = htmlInputElement(nonNull(this.applyQuery.value)).checked;
        const graphData = applyQuery
            ? nonNull(this.#graphData.value)
            : nonNull(this.#rdfStore.value).execute(QUERY_ALL);
        const format = nonNull(this.format);
        const downloadAnchor = htmlAnchorElement(nonNull(this.aRef.value));

        const exporter = new Exporter(graphData, format);
        const data = exporter.export();
        const blob = new Blob([data], { type: format.mimetype });
        const url = window.URL.createObjectURL(blob);
        downloadAnchor.href = url;
        downloadAnchor.download = `rdfexport-${new Date().toISOString().replaceAll(':', '_')}${
            format.fileEnding
        }`;
        downloadAnchor.click();

        // objectURLs need to be revoked
        // in order to not leak memory
        downloadAnchor.href = '/internal-error';
        downloadAnchor.removeAttribute('download');
        window.URL.revokeObjectURL(url);

        // success: cleanup
        this.#cancelExport(event);
    }
}

customElements.define('dl-export', Export);
