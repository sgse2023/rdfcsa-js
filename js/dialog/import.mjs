// @ts-check

import { LitElement, html } from 'lit';
import { createRef, ref } from 'lit/directives/ref.js';
import { ContextConsumer } from '@lit-labs/context';
import holidayCss from 'holiday.css';

import { RdfData as RdfDataEvent } from '../events/rdf-data.mjs';
import './polyfill.mjs';
import { nonNull } from '../lib/assertions.mjs';
import {
    dialogPolyfilledElement,
    htmlDialogElement,
    htmlInputElement,
} from '../assertions-frontend.mjs';
import { Importer } from '../lib/conversion/Importer.mjs';
import { NTriplesFormat } from '../lib/conversion/NTriplesFormat.mjs';
import { TurtleFormat } from '../lib/conversion/TurtleFormat.mjs';
import { rdfStore as storeContext } from '../context.mjs';
import { RDFCSA } from '../lib/rdfstore/RDFCSA.mjs';
import { QUERY_ALL } from '../lib/query/SimpleQuery.mjs';
import { RdfGraphData } from '../lib/rdfstore/RdfGraphData.mjs';
import { Triple } from '../lib/rdfstore/Triple.mjs';
import { SubjectObject } from '../lib/rdfstore/SubjectObject.mjs';
import { clearRdfStore, loadMockData } from '../component/StoreMockData.mjs';

/** @typedef {import('./polyfill.mjs').DialogPolyfilled} DialogPolyfilled */

class Import extends LitElement {
    /**
     * @override
     */
    static styles = [holidayCss];

    dialogRef = createRef();

    #rdfStore = new ContextConsumer(this, {
        context: storeContext,
        subscribe: true,
    });

    /**
     * @override
     */
    static properties = {
        file: { type: File, state: true },
        replaceData: { type: Boolean, state: true },
        dataSrc: { type: String, state: true },
    };

    #supportedFormats = [NTriplesFormat, TurtleFormat, RDFCSA];

    constructor() {
        super();
        /**
         * @type {File | null}
         */
        this.file = null;
        /**
         * @type {boolean}
         */
        this.replaceData = true;

        /**
         * @type {'empty' | 'paper' | 'file'}
         */
        this.dataSrc = 'file';
    }

    /**
     * @override
     */
    render() {
        const userInputValid = (this.dataSrc === 'file' && this.file) || this.dataSrc !== 'file';
        const fileSelectable = this.dataSrc === 'file';
        const fileTypes = this.#supportedFormats
            .flatMap((format) => [format.fileEnding, format.mimetype])
            .join(',');

        return html`
            <pf-dialog id="import-dialog" ${ref(this.dialogRef)}>
                <form>
                    <h1>Import</h1>
                    <!-- The data in accept should match the data used
                    for identifying the correct importer below -->
                    <!-- data src to use (file, empty, paper) -->
                    <fieldset @change=${this.#dataSrcChanged}>
                        <legend>Datenquelle</legend>
                        <label>
                            <input
                                type="radio"
                                name="dataSrc"
                                value="file"
                                ?checked=${this.dataSrc === 'file'}
                            />
                            Daten aus Datei einlesen
                        </label>
                        <label>
                            <input
                                type="radio"
                                name="dataSrc"
                                value="empty"
                                ?checked=${this.dataSrc === 'empty'}
                            />
                            Leeres Datenset
                        </label>
                        <label>
                            <input
                                type="radio"
                                name="dataSrc"
                                value="paper"
                                ?checked=${this.dataSrc === 'paper'}
                            />
                            Daten des Papers
                        </label>
                    </fieldset>
                    <!-- file -->
                    <label
                        >File:
                        <input
                            @change="${this.#fileChanged}"
                            type="file"
                            accept="${fileTypes}"
                            ?disabled=${!fileSelectable}
                        />
                    </label>
                    <!-- replace or expand -->
                    <fieldset @change=${this.#replaceDataChanged}>
                        <legend>Existierende Daten</legend>
                        <label>
                            <input
                                type="radio"
                                name="replaceData"
                                value="replace"
                                ?checked=${this.replaceData}
                            />
                            ersetzen
                        </label>
                        <label>
                            <input
                                type="radio"
                                name="replaceData"
                                value="keep"
                                ?checked=${!this.replaceData}
                            />
                            behalten; importierte Daten hinzufügen
                        </label>
                    </fieldset>
                    <!-- actions -->
                    <div>
                        <button @click=${this.#cancelImport}>Cancel</button>
                        <button
                            @click=${this.#doImport}
                            id="import-button"
                            value="default"
                            ?disabled="${!userInputValid}"
                        >
                            Import
                        </button>
                    </div>
                </form>
            </pf-dialog>
        `;
    }

    /**
     * @param {Event} e
     */
    #fileChanged(e) {
        const input = htmlInputElement(nonNull(/** @type {Element} */ (e.target)));
        if (input.files && input.files.length > 0) {
            this.file = nonNull(input.files[0]);
        }
    }

    /**
     * @param {Event} e
     */
    #replaceDataChanged(e) {
        const input = htmlInputElement(nonNull(/** @type {Element} */ (e.target)));
        if (input.value === 'keep') {
            this.replaceData = !input.checked;
        } else {
            this.replaceData = input.checked;
        }
    }

    /**
     * @param {Event} e
     */
    #dataSrcChanged(e) {
        const input = htmlInputElement(nonNull(/** @type {Element} */ (e.target)));
        if (input.value === 'file' || input.value === 'empty' || input.value === 'paper') {
            this.dataSrc = input.value;
        } else {
            throw Error(`Unexpected data src value '${input.value}'`);
        }
    }

    /**
     * @param {Event} event
     */
    #cancelImport(event) {
        event.preventDefault(); // do not submit fake form

        this.file = null;

        htmlDialogElement(
            nonNull(dialogPolyfilledElement(nonNull(this.dialogRef.value)).dialogRef.value),
        ).close();
    }

    /**
     * @param {Event} event
     */
    async #doImport(event) {
        event.preventDefault(); // do not submit fake form

        if (this.dataSrc === 'file') {
            const file = nonNull(this.file);
            const fileEnding = `.${file.name.split('.').at(-1)}`;
            const format = nonNull(this.#supportedFormats.find((i) => i.fileEnding === fileEnding));
            const importer = new Importer(await file.arrayBuffer(), format);

            let data = importer.import();
            if (!this.replaceData) {
                const added = data.execute(QUERY_ALL);
                const existing = nonNull(this.#rdfStore.value).execute(QUERY_ALL);

                const nodes = [];
                const links = [];
                let idCounter = 0;
                for (const triple of added.links) {
                    links.push(
                        new Triple(idCounter, idCounter + 2, idCounter + 1, triple.name, idCounter),
                    );
                    nodes.push(new SubjectObject(idCounter, added.nodeAtId(triple.subject).name));
                    nodes.push(
                        new SubjectObject(idCounter + 2, added.nodeAtId(triple.object).name),
                    );
                    idCounter += 3;
                }

                for (const triple of existing.links) {
                    links.push(
                        new Triple(idCounter, idCounter + 2, idCounter + 1, triple.name, idCounter),
                    );
                    nodes.push(
                        new SubjectObject(idCounter, existing.nodeAtId(triple.subject).name),
                    );
                    nodes.push(
                        new SubjectObject(idCounter + 2, existing.nodeAtId(triple.object).name),
                    );
                    idCounter += 3;
                }

                const combined = new RdfGraphData(nodes, links);
                data = Reflect.construct(data.constructor, [combined]);
            }
            this.dispatchEvent(new RdfDataEvent(data));
        } else if (this.dataSrc === 'paper') {
            if (this.replaceData) {
                loadMockData(this, this.#rdfStore)(false);
            } else {
                loadMockData(this, this.#rdfStore)(true);
            }
        } else if (this.dataSrc === 'empty') {
            if (this.replaceData) {
                clearRdfStore(this)();
            }
            // no need for else to add empty data
        }

        // success: cleanup
        this.#cancelImport(event);
    }
}

customElements.define('dl-import', Import);
