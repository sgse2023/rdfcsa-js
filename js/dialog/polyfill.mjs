// @ts-check

import { LitElement, css, html, nothing } from 'lit';
import { createRef, ref } from 'lit/directives/ref.js';
import dialogPolyfill from 'dialog-polyfill';
import holidayCss from 'holiday.css';
import dialogCss from 'dialog-polyfill/dialog-polyfill.css';

import { nonNull } from '../lib/assertions.mjs';
import { htmlDialogElement } from '../assertions-frontend.mjs';

export class DialogPolyfilled extends LitElement {
    /**
     * @override
     */
    static styles = [holidayCss, dialogCss, css``];

    /**
     * @override
     */
    static properties = {
        dialogId: { type: String },
    };

    dialogRef = createRef();

    static dialogCounter = 0;

    constructor() {
        super();

        /**
         * @type {string | null}
         */
        this.dialogId = null;
    }

    /**
     * @override
     */
    firstUpdated() {
        // prevent error in more recent (aka not maliciously outdated)
        // firefox versions
        // (see https://github.com/GoogleChrome/dialog-polyfill/issues/221)
        if (typeof HTMLDialogElement !== 'function') {
            // make sure the dialog works across all browser versions
            dialogPolyfill.registerDialog(htmlDialogElement(nonNull(this.dialogRef.value)));
        }
    }

    /**
     * @override
     */
    render() {
        return html`
            <dialog id="${this.dialogId ?? nothing}" ${ref(this.dialogRef)}>
                <slot></slot>
            </dialog>
        `;
    }
}

customElements.define('pf-dialog', DialogPolyfilled);
