// @ts-check

import { RdfData as RdfDataEvent } from '../events/rdf-data.mjs';
import { Triple } from '../lib/rdfstore/Triple.mjs';
import { RawTriple } from '../lib/rdfstore/RawTriple.mjs';
import { SubjectObject } from '../lib/rdfstore/SubjectObject.mjs';
import { nonNull } from '../lib/assertions.mjs';
import { RdfGraphData } from '../lib/rdfstore/RdfGraphData.mjs';
import { RDFCSA } from '../lib/rdfstore/RDFCSA.mjs';

/** @typedef {import('../context.mjs').rdfStore} rdfStoreContext */
/**
 * @template {import('lit').ReactiveElement} T
 * @typedef {import('@lit-labs/context').ContextConsumer<rdfStoreContext, T>} RdfStoreContext
 */

/**
 * @template {import('lit').ReactiveElement} T
 * @param {T} t
 * @param {RdfStoreContext<T>} rdfStoreProvider
 * @returns {(appending?: boolean) => void}
 */
export function loadMockData(t, rdfStoreProvider) {
    const fn = (appending = false) => {
        const data = [
            'Inception',
            'http://example.org/filmed%20in',
            'LA',
            'LA',
            'http://example.org/city%20of',
            'USA',
            'EPage',
            'http://example.org/appears%20in',
            'Inception',
            'LDiCaprio',
            'http://example.org/appears%20in',
            'Inception',
            'JGordon',
            'http://example.org/appears%20in',
            'Inception',
            'JGordon',
            'http://example.org/born%20in',
            'USA',
            'JGordon',
            'http://example.org/lives%20in',
            'LA',
            'EPage',
            'http://example.org/born%20in',
            'Canada',
            'LDiCaprio',
            'http://example.org/born%20in',
            'USA',
            'LDiCaprio',
            'http://example.org/awarded',
            'Oscar2015',
        ];
        if (!appending) {
            const subjectObject = [];
            const triples = [];
            for (let i = 0; i < data.length; i = i + 3) {
                subjectObject.push(new SubjectObject(i, nonNull(data[i])));
                subjectObject.push(new SubjectObject(i + 2, nonNull(data[i + 2])));
                triples.push(new Triple(i, i + 2, i + 1, nonNull(data[i + 1]), i));
            }

            const rdfGraphData = new RdfGraphData(subjectObject, triples);
            t.dispatchEvent(new RdfDataEvent(new RDFCSA(rdfGraphData)));
        } else if (rdfStoreProvider.value) {
            for (let i = 0; i < data.length; i = i + 3) {
                rdfStoreProvider.value?.addTriple(
                    new RawTriple(nonNull(data[i]), nonNull(data[i + 1]), nonNull(data[i + 2])),
                );
            }
            t.dispatchEvent(new RdfDataEvent(rdfStoreProvider.value, true));
        } else {
            throw new Error('no rdf store existing');
        }
    };
    return fn;
}

/**
 * @template {import('lit').ReactiveElement} T
 * @param {T} t
 * @returns {() => void}
 */
export function clearRdfStore(t) {
    const fn = () => {
        t.dispatchEvent(new RdfDataEvent(new RDFCSA(new RdfGraphData([], []))));
    };
    return fn;
}
