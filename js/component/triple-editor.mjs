// @ts-check

import { html, css, LitElement } from 'lit';
import { createRef, ref } from 'lit/directives/ref.js';
import { ContextConsumer } from '@lit-labs/context';
import holidayCss from 'holiday.css';

import { RawTriple } from '../lib/rdfstore/RawTriple.mjs';
import { htmlTextAreaElement } from '../assertions-frontend.mjs';
import { nonNull } from '../lib/assertions.mjs';
import { RdfData as RdfDataEvent } from '../events/rdf-data.mjs';
import { rdfStore as rdfStoreContext } from '../context.mjs';
import { EditingCapactityError } from '../lib/error/EditingCapacityError.mjs';
import { QUERY_ALL } from '../lib/query/SimpleQuery.mjs';
import { RDFCSA } from '../lib/rdfstore/RDFCSA.mjs';
// todo: include feather js / style

class TripleEditor extends LitElement {
    /**
     * @override
     */
    static styles = [
        holidayCss,
        css`
            form {
                display: flex;
                width: 100%;
                justify-content: space-around;
                align-items: center;
            }
            form label {
                display: flex;
                flex-direction: column;
            }
            form textarea {
                min-height: 6em;
            }
            form .buttons {
                align-self: stretch;
                display: flex;
                flex-direction: column;
                justify-content: space-around;
                padding-top: 2rem;
                padding-left: 1rem;
            }
        `,
    ];

    #rdfStore = new ContextConsumer(this, {
        context: rdfStoreContext,
        subscribe: true,
    });

    #subjectRef = createRef();
    #predicateRef = createRef();
    #objectRef = createRef();

    /**
     * @override
     */
    static properties = {
        originalSubject: { type: String, attribute: 'subject' },
        originalPredicate: { type: String, attribute: 'predicate' },
        originalObject: { type: String, attribute: 'object' },
        editing: { type: Boolean, state: true },
        triple: { type: RawTriple, state: true },
    };

    constructor() {
        super();
        /**
         * @type {String}
         */
        this.originalSubject = '';
        /**
         * @type {String}
         */
        this.originalPredicate = '';
        /**
         * @type {String}
         */
        this.originalObject = '';
        /**
         * @type {Boolean}
         */
        this.editing = false;
    }

    /**
     * @override
     */
    render() {
        return html`<form @change=${this.#inputChanged}>
            <label
                >Subject<textarea
                    ${ref(this.#subjectRef)}
                    cols="${this.originalSubject.length}"
                    rows="1"
                    ?readonly=${!this.editing}
                    .value=${this.originalSubject}
                ></textarea>
            </label>
            <label
                >Predicate<textarea
                    ${ref(this.#predicateRef)}
                    cols="${this.originalPredicate.length}"
                    rows="1"
                    ?readonly=${!this.editing}
                    .value=${this.originalPredicate}
                ></textarea>
            </label>
            <label
                >Object<textarea
                    ${ref(this.#objectRef)}
                    cols="${this.originalObject.length}"
                    rows="1"
                    ?readonly=${!this.editing}
                    .value=${this.originalObject}
                ></textarea>
            </label>
            <div class="buttons">
                <button
                    type="submit"
                    ?readonly=${!this.editing || this.triple}
                    @click=${
                        /** @param {Event} event */ (event) => {
                            if (this.editing) {
                                return this.#saveModification(event);
                            }
                            return this.#allowModify(event);
                        }
                    }
                >
                    ${this.editing ? 'Speichern' : 'Bearbeiten'}
                </button>
                <button
                    type="reset"
                    @click=${
                        /** @param {Event} event */ (event) => {
                            if (this.editing) {
                                return this.#cancelModification(event);
                            }
                            return this.#delete(event);
                        }
                    }
                >
                    ${this.editing ? 'Abbrechen' : 'Löschen'}
                </button>
            </div>
        </form>`;
    }

    /**
     * @param {Event} event
     */
    #inputChanged(event) {
        event;

        const subject = htmlTextAreaElement(nonNull(this.#subjectRef.value)).value.trim();
        const predicate = htmlTextAreaElement(nonNull(this.#predicateRef.value)).value.trim();
        const object = htmlTextAreaElement(nonNull(this.#objectRef.value)).value.trim();

        if (subject.length > 0 && predicate.length > 0 && object.length > 0) {
            this.triple = new RawTriple(subject, predicate, object);
        } else {
            this.triple = null;
        }
    }

    /**
     * @param {Event} event
     */
    #delete(event) {
        event.preventDefault();

        const rdfStore = this.#rdfStore.value;
        if (rdfStore) {
            rdfStore.deleteTriple(
                new RawTriple(this.originalSubject, this.originalPredicate, this.originalObject),
            );
            this.dispatchEvent(new RdfDataEvent(rdfStore, true));
        }
    }

    /**
     * @param {Event} event
     */
    #allowModify(event) {
        event.preventDefault();
        this.editing = true;
    }

    /**
     * @param {Event} event
     */
    #saveModification(event) {
        event.preventDefault();
        this.editing = false;

        const oldTriple = new RawTriple(
            this.originalSubject,
            this.originalPredicate,
            this.originalObject,
        );

        let rdfStore = this.#rdfStore.value;
        if (rdfStore) {
            try {
                rdfStore.editTriple(oldTriple, this.triple);
            } catch (e) {
                if (e instanceof EditingCapactityError) {
                    rdfStore = new RDFCSA(rdfStore.execute(QUERY_ALL));
                    rdfStore.editTriple(oldTriple, this.triple);
                } else {
                    throw e;
                }
            }
            this.dispatchEvent(new RdfDataEvent(rdfStore, true));
        }
    }

    /**
     * @param {Event} event
     */
    #cancelModification(event) {
        event.preventDefault();
        this.editing = false;

        const subject = htmlTextAreaElement(nonNull(this.#subjectRef.value));
        const predicate = htmlTextAreaElement(nonNull(this.#predicateRef.value));
        const object = htmlTextAreaElement(nonNull(this.#objectRef.value));

        subject.value = this.originalSubject;
        predicate.value = this.originalPredicate;
        object.value = this.originalObject;
    }
}
customElements.define('triple-editor', TripleEditor);
