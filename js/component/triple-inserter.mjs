// @ts-check

import { html, css, LitElement } from 'lit';
import { createRef, ref } from 'lit/directives/ref.js';
import { ContextConsumer } from '@lit-labs/context';
import holidayCss from 'holiday.css';

import { RawTriple } from '../lib/rdfstore/RawTriple.mjs';
import { htmlTextAreaElement } from '../assertions-frontend.mjs';
import { nonNull } from '../lib/assertions.mjs';
import { RdfData as RdfDataEvent } from '../events/rdf-data.mjs';
import { rdfStore as rdfStoreContext } from '../context.mjs';
import { EditingCapactityError } from '../lib/error/EditingCapacityError.mjs';
import { RDFCSA } from '../lib/rdfstore/RDFCSA.mjs';
import { QUERY_ALL } from '../lib/query/SimpleQuery.mjs';
// todo: include feather js / style

class TripleInserter extends LitElement {
    /**
     * @override
     */
    static styles = [
        holidayCss,
        css`
            form {
                display: flex;
                width: 100%;
                justify-content: space-around;
                align-items: center;
            }
            form label {
                display: flex;
                flex-direction: column;
            }
            form textarea {
                min-height: 6em;
            }
            form .buttons {
                align-self: stretch;
                display: flex;
                flex-direction: column;
                justify-content: space-around;
                padding-top: 2rem;
                padding-left: 1rem;
            }
        `,
    ];

    #rdfStore = new ContextConsumer(this, {
        context: rdfStoreContext,
        subscribe: true,
    });

    #subjectRef = createRef();
    #predicateRef = createRef();
    #objectRef = createRef();

    /**
     * @override
     */
    static properties = {
        triple: { type: RawTriple, state: true },
    };

    constructor() {
        super();

        /**
         * @type {RawTriple | null}
         */
        this.triple = null;
    }

    /**
     * @override
     */
    render() {
        const valid = this.triple;

        return html`<form @change=${this.#inputChanged}>
            <label>Subject<textarea ${ref(this.#subjectRef)} rows="1"></textarea></label>
            <label>Predicate<textarea ${ref(this.#predicateRef)} rows="1"></textarea></label>
            <label>Object<textarea ${ref(this.#objectRef)} rows="1"></textarea></label>
            <div class="buttons">
                <button
                    ?disabled=${!valid}
                    type="submit"
                    @click=${/** @param {Event} event */ (event) => this.#saveTriple(event)}
                >
                    Hinzufügen
                </button>
            </div>
        </form>`;
    }

    /**
     * @param {Event} event
     */
    #inputChanged(event) {
        event;

        const subject = htmlTextAreaElement(nonNull(this.#subjectRef.value)).value.trim();
        const predicate = htmlTextAreaElement(nonNull(this.#predicateRef.value)).value.trim();
        const object = htmlTextAreaElement(nonNull(this.#objectRef.value)).value.trim();

        if (subject.length > 0 && predicate.length > 0 && object.length > 0) {
            this.triple = new RawTriple(subject, predicate, object);
        } else {
            this.triple = null;
        }
    }

    /**
     * @param {Event} event
     */
    #saveTriple(event) {
        event.preventDefault();
        this.editing = false;

        const subject = htmlTextAreaElement(nonNull(this.#subjectRef.value));
        const predicate = htmlTextAreaElement(nonNull(this.#predicateRef.value));
        const object = htmlTextAreaElement(nonNull(this.#objectRef.value));

        let rdfStore = this.#rdfStore.value;
        if (rdfStore) {
            try {
                rdfStore.addTriple(this.triple);
            } catch (e) {
                if (e instanceof EditingCapactityError) {
                    rdfStore = new RDFCSA(rdfStore.execute(QUERY_ALL));
                    rdfStore.addTriple(this.triple);
                } else {
                    throw e;
                }
            }
            this.dispatchEvent(new RdfDataEvent(rdfStore, true));

            subject.value = '';
            predicate.value = '';
            object.value = '';
        }
    }
}
customElements.define('triple-inserter', TripleInserter);
