// @ts-check

import { LitElement, css, html } from 'lit';
import holidayCss from 'holiday.css';
import { nonNull } from '../lib/assertions.mjs';
import { dialogPolyfilledElement, htmlDialogElement } from '../assertions-frontend.mjs';

export class ButtonExport extends LitElement {
    /**
     * @override
     */
    static styles = [holidayCss, css``];

    constructor() {
        super();
    }

    /**
     * @override
     */
    render() {
        return html` <button @click="${this.#openExport}">Exportieren</button> `;
    }

    #openExport() {
        htmlDialogElement(
            nonNull(
                dialogPolyfilledElement(
                    nonNull(
                        nonNull(
                            nonNull(
                                nonNull(
                                    nonNull(document.querySelector('rdfcsa-app')).shadowRoot,
                                ).querySelector('dl-export'),
                            ).shadowRoot,
                        ).querySelector('#export-dialog'),
                    ),
                ).dialogRef.value,
            ),
        ).showModal();
    }
}

customElements.define('btn-export', ButtonExport);
