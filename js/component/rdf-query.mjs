// @ts-check

import { LitElement, css, html } from 'lit';
import { createRef, ref } from 'lit/directives/ref.js';
import holidayCss from 'holiday.css';

import { RdfQuery as QueryEvent } from '../events/rdf-query.mjs';
import { TextQuery } from '../lib/query/TextQuery.mjs';
import { nonNull } from '../lib/assertions.mjs';
import { htmlInputElement } from '../assertions-frontend.mjs';
import { QUERY_ALL } from '../lib/query/SimpleQuery.mjs';

export class RdfQuery extends LitElement {
    /**
     * @override
     */
    static styles = [holidayCss, css``];

    input = createRef();

    /**
     * @override
     */
    static properties = {
        query: { type: String },
    };

    constructor() {
        super();
        this.query = '';
    }

    /**
     * @override
     */
    render() {
        return html`
            <input .value=${this.query} ${ref(this.input)} />
            <button @click="${this.#fireQuery}">Query ausführen</button>
        `;
    }

    #fireQuery() {
        const input = htmlInputElement(nonNull(this.input.value)).value.trim();

        this.dispatchEvent(new QueryEvent(input.length > 0 ? new TextQuery(input) : QUERY_ALL));
    }
}

customElements.define('rdf-query', RdfQuery);
