import type { Export } from '../conversion/Export';

// realized as a type,
// so that type checking the static properties
// & functions works
//
// Usage:
// /** @type {SettingsStatic} */
// const _typeCheck = MyImplementation;
type SettingsStatic = {
    import(fileContent: BinaryData): Settings;
};

interface Settings {
    export(): BinaryData;

    queryTripleLimit: number | undefined;

    nodeColor(id: number): string | undefined;

    nodeRenderMode: 'text' | 'icon';
}

export { Settings, SettingsStatic };
