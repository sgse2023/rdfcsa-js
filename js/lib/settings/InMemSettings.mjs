// @ts-check

/** @typedef {import('./Settings.d.ts').Settings} Settings
/** @typedef {import('./Settings.d.ts').SettingsStatic} SettingsStatic

/**
 * @implements {Settings}
 */
class InMemSettings {
    /**
     * @type {number}
     */
    queryTripleLimit = 100;

    /**
     * @param {number} id
     * @returns {string | undefined}
     */
    nodeColor(id) {
        id;
        // eslint-disable-next-line no-undefined
        return undefined;
    }

    /**
     * @type {'text' | 'icon'}
     */
    nodeRenderMode = 'text';

    /**
     *
     * @returns {BinaryData}
     */
    export() {
        throw new Error('Not supported!');
    }

    /**
     * @param {BinaryData} settings
     * @returns {InMemSettings}
     */
    static import(settings) {
        settings;
        throw new Error('Not supported!');
    }
}

/**
 * @type {SettingsStatic}
 */
const _typeCheck = InMemSettings;
_typeCheck;

export { InMemSettings };
