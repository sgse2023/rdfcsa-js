// @ts-check

/** @typedef {import('./rdfstore/ImportTriple.mjs').ImportTriple} ImportTriple */

/**
 * compare two import triples and compare in order subject, predicate, object
 * @param {ImportTriple} a
 * @param {ImportTriple} b
 */
function compareTriple(a, b) {
    if (a.subject === b.subject) {
        if (a.predicate === b.predicate) {
            if (a.object === b.object) {
                return 0;
            }
            if (a.object > b.object) {
                return 1;
            }
            return -1;
        }
        if (a.predicate > b.predicate) {
            return 1;
        }
        return -1;
    }
    if (a.subject > b.subject) {
        return 1;
    }
    return -1;
}

/**
 * Create Suffix Array from array
 * @param {ImportTriple[]} array
 * @returns {ImportTriple[]} ordered Suffix Array
 */
function createSuffixArray(array) {
    return array.sort(compareTriple);
}

export { createSuffixArray };
