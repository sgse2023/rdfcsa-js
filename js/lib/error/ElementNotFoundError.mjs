//@ts-check

class ElementNotFoundError extends Error {
    /**
     * @param {string | undefined} message
     */
    constructor(message) {
        super(message);
    }
}

export { ElementNotFoundError };
