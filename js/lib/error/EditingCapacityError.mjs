// @ts-check

class EditingCapactityError extends Error {
    constructor() {
        super('RDFStore needs to be rebuild, to continue editing');
    }
}

export { EditingCapactityError };
