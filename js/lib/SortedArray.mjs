//@ts-check
import { binarySearch } from './rdfstore/BinarySearch.mjs';
import { nonNull } from './assertions.mjs';

/**
 * @param {Uint8Array| Uint32Array | number[]} arr
 * @param {number} val
 * @param {number} _start
 * @param {number} _end
 * @returns {number}
 */
function binSearchInexact(arr, val, _start, _end) {
    let start = _start;
    let end = _end;

    if (end < 0 || end > arr.length) {
        throw new RangeError(`cannot search to ${end} (valid: 0..=${arr.length})`);
    }
    if (start < 0) {
        throw new RangeError(`cannot calculate rank starting from ${start} (valid: 0..)`);
    }
    if (start > end) {
        throw new RangeError(`start must be < end`);
    }

    if (start + 1 === end) {
        if (nonNull(arr[start]) > val) {
            return start;
        }
        return end;
    }

    while (start + 1 < end) {
        let mid = Math.floor((start + end) / 2);

        if (typeof arr[mid] === 'undefined') {
            console.error(
                `called with start ${start} and end ${end} while arr is ${arr.length} long`,
            );
        }
        const current = nonNull(arr[mid]);
        if (current === val) {
            return mid;
        } else if (current <= val) {
            start = mid;
        } else {
            end = mid;
        }
    }
    return end;
}

/**
 * Plain object representation for (de)serialisation purposes.
 *
 * @typedef {object} SortedArrayPojo
 * @prop {number[]} array
 */

class SortedArray {
    /**
     * @type {number[]}
     */
    #array;
    constructor() {
        this.#array = [];
    }

    /**
     * @returns {number} returns the index of a value inside the array
     * @param {number} value the value that should be found
     */
    find(value) {
        return binarySearch(this.#array, value);
    }

    /**
     * @returns {number} returns the index of a value inside the array
     * @param {number} value the value that should be found
     */
    findInexact(value) {
        if (this.#array.length === 0) {
            return 0;
        }
        return binSearchInexact(this.#array, value, 0, this.#array.length);
    }

    /**
     *
     * @param {number} value
     */
    insert(value) {
        let index = binSearchInexact(this.#array, value, 0, this.#array.length);
        this.#array.splice(index, 0, value);
    }

    /**
     * Convert this into a plain object for serialisation purposes.
     *
     * Note: does not do any versioning, i.e. implementation changes
     * after serialising *will* result in broken deserialisation.
     *
     * @returns {SortedArrayPojo}
     */
    toPojo() {
        return {
            array: this.#array,
        };
    }

    /**
     * Recreate from existing state in pojo.
     *
     * Note: does not do any versioning, i.e. implementation changes
     * after serialising *will* result in broken deserialisation here.
     *
     * @param {SortedArrayPojo} pojo
     * @returns {SortedArray}
     */
    static fromPojo(pojo) {
        const b = new SortedArray();
        b.#array = pojo.array;
        return b;
    }
}

export { SortedArray };
