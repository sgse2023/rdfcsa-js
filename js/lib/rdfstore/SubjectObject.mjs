// @ts-check

class SubjectObject {
    /**
     * @readonly
     * @type {number}
     */
    id;

    /**
     * @readonly
     * @type {string}
     */
    name;

    /**
     * @param {number} id
     * @param {string} name
     */
    constructor(id, name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Convert this into a plain object.
     *
     * Note: object is newly created,
     * so this method effectively acts as a `deepClone()` method.
     *
     * @returns {object}
     */
    toPojo() {
        return {
            id: this.id,
            name: this.name,
        };
    }

    /**
     * Returns the properties of the current object as json.
     *
     * Overrides toString.
     *
     * @returns {string}
     */
    toString() {
        return JSON.stringify(this);
    }
}

export { SubjectObject };
