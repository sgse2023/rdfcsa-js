// @ts-check
class ImportTriple {
    /**
     * Subject id
     * @readonly
     * @type {number}
     */
    subject;
    /**
     * Object id
     * @readonly
     * @type {number}
     */
    object;
    /**
     * Predicate id
     * @readonly
     * @type {number}
     */
    predicate;

    /**
     * @param {number} subject Subject id
     * @param {number} object Object id
     * @param {number} predicate Predicate id
     */
    constructor(subject, predicate, object) {
        this.subject = subject;
        this.object = object;
        this.predicate = predicate;
    }
}

export { ImportTriple };
