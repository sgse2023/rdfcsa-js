import type { Query } from '../query/Query';
import type { RdfGraphData } from './RdfGraphData.mjs';
import type { Export } from '../conversion/Export';
import type { RawTriple } from '../RawTriple.mjs';

// unfortunately, type checking for Import is not possbile
// with TS. Please do it manually with the method
// documented in Import.d.ts.

export interface RdfStore extends Export {
    addTriple(triple: RawTriple);
    deleteTriple(triple: RawTriple);
    editTriple(oldTriple: RawTriple, newTriple: RawTriple);
    execute(query: Query, tripleLimit?: number): RdfGraphData;
    tripleCount(): number;
}
