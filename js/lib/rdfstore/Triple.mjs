// @ts-check

class Triple {
    /**
     * Subject id, with gap
     * @readonly
     * @type {number}
     */
    subject;
    /**
     * Object id, with gap
     * @readonly
     * @type {number}
     */
    object;
    /**
     * Predicate id, with gap
     * @readonly
     * @type {number}
     */
    predicateId;
    /**
     * Name of the predicate identified by predicateId
     * @readonly
     * @type {string}
     */
    name;
    /**
     * Unique id for the graph this triple will end up in.
     * @readonly
     * @type {number}
     */
    id;

    /**
     * @param {number} subject Subject id, with gap
     * @param {number} object Object id, with gap
     * @param {number} predicateId Predicate id, with gap
     * @param {string} name Name of the predicate identified by predicateId
     * @param {number} id Unique id for the graph this triple will end up in.
     */
    constructor(subject, object, predicateId, name, id) {
        this.subject = subject;
        this.object = object;
        this.predicateId = predicateId;
        this.name = name;
        this.id = id;
    }

    /**
     * Node source id (used by force-graph).
     * @readonly
     * @type {number}
     */
    get source() {
        return this.subject;
    }

    /**
     * Node target id (used by force-graph).
     * @readonly
     * @type {number}
     */
    get target() {
        return this.object;
    }

    /**
     * Convert this into a plain object.
     *
     * Note: object is newly created,
     * so this method effectively acts as a `deepClone()` method.
     *
     * @returns {object}
     */
    toPojo() {
        return {
            subject: this.subject,
            object: this.object,
            predicateId: this.predicateId,
            name: this.name,
            id: this.id,
            source: this.source,
            target: this.target,
        };
    }

    /**
     * Returns the properties of the current object as json.
     *
     * Overrides toString.
     *
     * @returns {string}
     */
    toString() {
        return JSON.stringify(this);
    }
}

export { Triple };
