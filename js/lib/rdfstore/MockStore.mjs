// @ts-check

/** @typedef {import('./RdfStore').RdfStore} RdfStore */
/** @typedef {import('./RdfGraphData.mjs').RdfGraphData} RdfGraphData */
/** @typedef {import('./RawTriple.mjs').RawTriple} RawTriple */
/** @typedef {import('../query/Query').Query} Query */
/** @typedef {import('../conversion/Import').Import} Import */

const fileEnding = '.rdfcsa';

/**
 * @implements {RdfStore}
 */
class MockStore {
    static fileEnding = fileEnding;
    fileEnding = fileEnding;

    static mimetype = 'application/x-rdfcsa-mock';

    static formatName = 'Mock';

    static description = 'Mock. Mock Mock. Moock Moooooock Moock.';

    get mimetype() {
        return MockStore.mimetype;
    }

    get formatName() {
        return MockStore.formatName;
    }

    get description() {
        return MockStore.description;
    }

    /**
     * @returns {number}
     */
    tripleCount() {
        throw new Error('TODO');
    }

    /**
     * @param {RawTriple} triple
     */
    addTriple(triple) {
        triple;
        throw new Error('TODO');
    }
    /**
     * @param {RawTriple} triple
     */
    deleteTriple(triple) {
        triple;
        throw new Error('TODO');
    }
    /**
     * @param {RawTriple} oldTriple
     * @param {RawTriple} newTriple
     */
    editTriple(oldTriple, newTriple) {
        this.deleteTriple(oldTriple);
        this.addTriple(newTriple);
    }

    /**
     * @param {Query} query
     * @returns {RdfGraphData}
     */
    execute(query) {
        query;
        throw new Error('TODO');
    }

    /**
     * @param {RdfGraphData} triples
     * @returns {BinaryData}
     */
    export(triples) {
        triples;
        throw new Error('TODO');
    }

    /**
     * @param {BinaryData} fileContent
     * @returns {RdfGraphData | RdfStore}
     */
    static import(fileContent) {
        fileContent;
        throw new Error('TODO');
    }

    /**
     * @param {BinaryData} fileContent
     * @param {RdfStore} existing
     * @returns {boolean}
     */
    static add(fileContent, existing) {
        fileContent;
        existing;
        throw new Error('TODO');
    }
}

/** @type {Import} */
const _typeCheck = MockStore;

export { MockStore };
