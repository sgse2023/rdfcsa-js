// @ts-check

/** @typedef {import('./SubjectObject.mjs').SubjectObject} SubjectObject */
/** @typedef {import('./Triple.mjs').Triple} Triple */
/**
 * @typedef {object} GraphDataPojo
 * @property {object[]} nodes
 * @property {object[]} links
 */

import { nonNull } from '../assertions.mjs';

class RdfGraphData {
    /** @type Map<number, number> */
    #nodeIdToArrayPos = new Map();

    /**
     * @readonly
     * @type {readonly SubjectObject[]} nodes
     */
    nodes;

    /**
     * @readonly
     * @type {readonly Triple[]} links
     */
    links;

    /**
     * @param {readonly SubjectObject[]} nodes
     * @param {readonly Triple[]} links
     */
    constructor(nodes, links) {
        this.nodes = nodes;
        this.links = links;

        for (const [i, node] of this.nodes.entries()) {
            this.#nodeIdToArrayPos.set(node.id, i);
        }

        // todo: Is node_id_to_array_pos required?
    }

    /**
     * Returns the node (subject, object) with the given id or null if not found.
     *
     * @param {number} id
     * @return {SubjectObject}
     */
    nodeAtId(id) {
        return nonNull(this.nodes[nonNull(this.#nodeIdToArrayPos.get(id))]);
    }

    /**
     * Returns the link (predicate / triple) with the given id or null if not found.
     *
     * @param {number} id
     * @return {Triple | undefined}
     */
    linkAtId(id) {
        return this.links.find((link) => link.id === id);
    }

    /**
     * Convert this into a plain object.
     *
     * Note: object is newly created,
     * so this method effectively acts as a `deepClone()` method.
     *
     * @return {GraphDataPojo}
     */
    toPojo() {
        return {
            nodes: this.nodes.map((n) => n.toPojo()),
            links: this.links.map((l) => l.toPojo()),
        };
    }
}

export { RdfGraphData };
