// @ts-check

class RawTriple {
    /**
     * Subject id
     * @readonly
     * @type {string}
     */
    subject;
    /**
     * Object id
     * @readonly
     * @type {string}
     */
    object;
    /**
     * Predicate id
     * @readonly
     * @type {string}
     */
    predicate;

    /**
     * @param {string} subject Subject id
     * @param {string} object Object id
     * @param {string} predicate Predicate id
     */
    constructor(subject, predicate, object) {
        this.subject = subject;
        this.object = object;
        this.predicate = predicate;
    }
}

export { RawTriple };
