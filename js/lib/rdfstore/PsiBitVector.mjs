// @ts-check

import { nonNull } from '../assertions.mjs';
import { BitVector } from './BitVector.mjs';
import { IndexBitVector } from './IndexBitVector.mjs';

/** @typedef {import('./IndexBitVector.mjs').IndexBitVector<BitVector>} TIndexBitVector */
/** @typedef {import('./IndexBitVector.mjs').IndexBitVectorPojo} IndexBitVectorPojo */

/**
 * Calculates the index from d for usage in psi.
 *
 * @param {number} unwrappedIndex
 * @param {number} addedTriples
 * @param {number} addonCapacity
 * @param {number} origTripleCount
 */
function wrapIndex(unwrappedIndex, addedTriples, addonCapacity, origTripleCount) {
    if (unwrappedIndex < addedTriples + origTripleCount) {
        // subject
        return unwrappedIndex;
    } else if (unwrappedIndex < 2 * (addedTriples + origTripleCount)) {
        // predicate
        return unwrappedIndex - addedTriples + addonCapacity;
    }
    // object
    return unwrappedIndex - (2 * addedTriples) + (2 * addonCapacity);
}

/**
 * Calculates the index from d for usage in psi.
 *
 * @param {number} unwrappedIndex
 * @param {number} addedTriples
 * @param {number} addonCapacity
 * @param {number} origTripleCount
 */
function wrapIndexForInsert(unwrappedIndex, addedTriples, addonCapacity, origTripleCount) {
    // if (unwrappedIndex <= addedElements + origTripleCount) {
    //     // subject
    //     return unwrappedIndex;
    // } else if (unwrappedIndex <= 2 * (addedElements + origTripleCount)) {
    //     // predicate
    //     return unwrappedIndex + (addonCapacity - addedElements);
    // }
    // // object
    // return unwrappedIndex + (2 * addonCapacity - addedElements);
    if (unwrappedIndex <= addedTriples + origTripleCount) {
        // subject
        return unwrappedIndex;
    } else if (unwrappedIndex <= 2 * (addedTriples + origTripleCount)) {
        // predicate
        return unwrappedIndex - addedTriples + addonCapacity;
    }
    // object
    return unwrappedIndex - (2 * addedTriples) + (2 * addonCapacity);
}
/**
 * Calculates the index from psi for usage in d.
 *
 * @param {number} index
 * @param {number} addedTriples
 * @param {number} addonCapacity
 * @param {number} maxElementsPerType
 */
function unwrapIndex(index, addedTriples, addonCapacity, maxElementsPerType) {
    if (index < maxElementsPerType) {
        // subject
        return index;
    } else if (index < 2 * maxElementsPerType) {
        // predicate
        return index - addonCapacity + addedTriples;
    }
    // object
    return index - (2 * addonCapacity) + (2 * addedTriples);
}

/**
 * Calculates the index from psi for usage in d.
 *
 * @param {number} index
 * @param {number} addedSubjects
 * @param {number} addedPredicates
 * @param {number} addonCapacity
 * @param {number} maxElementsPerType
 */
function unwrapIndexForInsert(
    index,
    addedSubjects,
    addedPredicates,
    addonCapacity,
    maxElementsPerType,
) {
    if (index < maxElementsPerType) {
        // subject
        return index;
    } else if (index < 2 * maxElementsPerType) {
        // predicate
        return index - addonCapacity + addedSubjects;
    }
    // object
    return index - (2 * addonCapacity) + addedSubjects + addedPredicates;
}

/**
 * Plain object representation for (de)serialisation purposes.
 *
 * @typedef {object} PsiBitVectorPojo
 * @property {IndexBitVectorPojo} bitVector
 * @property {number} addonCapacity
 * @property {number} origTripleCount
 */

/**
 * Wraps an IndexBitVector for usage in contexts with addon capacity.
 *
 * That means:
 * - the indices of this bit vector are adjusted based on them representing subjects, predicates,
 * or objects
 */
class PsiBitVector {
    /**
     * @type {TIndexBitVector}
     */
    #bitVector;

    /**
     * @type {number}
     */
    #addonCapacity;

    /**
     * @type {number}
     */
    #origTripleCount;

    /**
     * @param {TIndexBitVector} bitVector
     * @param {number} addonCapacity
     * @param {number} origTripleCount
     */
    constructor(bitVector, addonCapacity, origTripleCount) {
        this.#bitVector = nonNull(bitVector);
        this.#addonCapacity = addonCapacity;
        this.#origTripleCount = origTripleCount;
    }

    get length() {
        return this.#bitVector.length;
    }

    get #maxElementsPerType() {
        return this.#addonCapacity + this.#origTripleCount;
    }

    /**
     * @param {boolean} value
     */
    append(value) {
        this.#bitVector.append(value);
    }

    /**
     * @param {number} _index
     * @param {boolean} value
     */
    insert(_index, value) {
        const index = unwrapIndexForInsert(
            _index,
            this.#addedSubjects,
            this.#addedPredicates,
            this.#addonCapacity,
            this.#maxElementsPerType,
        );
        this.#bitVector.insert(index, value);
    }

    /**
     * @param {number} _index
     * @returns {number}
     */
    rank(_index) {
        const index = unwrapIndex(
            _index,
            this.#addedTriples,
            this.#addonCapacity,
            this.#maxElementsPerType,
        );
        return this.#bitVector.rank(index);
    }

    /**
     * @param {number} rank
     * @returns {number}
     */
    select(rank) {
        const unwrappedIndex = this.#bitVector.select(rank);
        return wrapIndex(
            unwrappedIndex,
            this.#addedTriples,
            this.#addonCapacity,
            this.#origTripleCount,
        );
    }

    /**
     * @param {number} rank
     * @returns {number}
     */
    selectEndPrevious(rank) {
        const unwrappedIndex = this.#bitVector.select(rank) - 1;
        return wrapIndex(
            unwrappedIndex,
            this.#addedTriples,
            this.#addonCapacity,
            this.#origTripleCount,
        );
    }

    /**
     * @param {number} rank
     * @param {boolean} addedToDict
     * @returns {number}
     */
    selectInsertPosition(rank, addedToDict) {
        let unwrappedIndex = this.#bitVector.select(rank);
        if (!addedToDict) {
            unwrappedIndex++;
        }
        // if (type === 'S') {
        //     if (unwrappedIndex < Math.floor(this.length / 3)) {
        //         end = false;
        //         unwrappedIndex++;
        //     }
        // }
        // if (type === 'P') {
        //     if (unwrappedIndex < Math.floor(this.length / 3) * 2) {
        //         end = false;
        //         unwrappedIndex++;
        //     }
        // }
        // if (type === 'O') {
        //     if (unwrappedIndex < Math.floor(this.length / 3) * 3) {
        //         end = false;
        //         unwrappedIndex++;
        //     }
        // }

        return wrapIndexForInsert(
            unwrappedIndex,
            this.#addedTriples,
            this.#addonCapacity,
            this.#origTripleCount,
        );
    }

    /**
     * @param {number} index
     * @returns {boolean}
     */
    _at(index) {
        return /** @type {any} */ (this.#bitVector._at(index));
    }

    /**
     * @returns {number}
     */
    get #addedElements() {
        //return Math.floor(this.#bitVector.length / 3) - this.#origTripleCount;
        return this.#bitVector.length - (this.#origTripleCount * 3);
    }

    /**
     * @returns {number}
     */
    get #addedTriples() {
        //return Math.floor(this.#bitVector.length / 3) - this.#origTripleCount;
        return Math.floor(this.#addedElements / 3);
    }

    get #addedSubjects() {
        let value = this.#addedTriples;
        if (this.#addedElements % 3 !== 0) {
            value++;
        }
        return value;
    }

    get #addedPredicates() {
        let value = this.#addedTriples;
        if (this.#addedElements % 3 === 2) {
            value++;
        }
        return value;
    }

    get maximumIndex() {
        return 3 * (this.#origTripleCount + this.#addonCapacity);
    }

    /**
     * Convert this into a plain object for serialisation purposes.
     *
     * Note: does not do any versioning, i.e. implementation changes
     * after serialising *will* result in broken deserialisation.
     *
     * @return {PsiBitVectorPojo}
     */
    toPojo() {
        return {
            bitVector: this.#bitVector.toPojo(),
            addonCapacity: this.#addonCapacity,
            origTripleCount: this.#origTripleCount,
        };
    }

    /**
     * Recreate from existing state in pojo.
     *
     * Note: does not do any versioning, i.e. implementation changes
     * after serialising *will* result in broken deserialisation here.
     *
     * Note: target *must* have the exact same type as
     * the BitVector used for constructing the originally serialized
     * BitVector in pojo.
     *
     * @param {PsiBitVectorPojo} pojo
     * @returns {PsiBitVector}
     */
    static fromPojo(pojo) {
        const bitVector = /** @type {TIndexBitVector} */ (
            IndexBitVector.fromPojo(pojo.bitVector, BitVector)
        );
        const indexBitVector = new PsiBitVector(
            bitVector,
            pojo.addonCapacity,
            pojo.origTripleCount,
        );
        return indexBitVector;
    }
}

export { PsiBitVector, wrapIndex, unwrapIndex, wrapIndexForInsert };
