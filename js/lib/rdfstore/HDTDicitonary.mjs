// @ts-check

import { ElementNotFoundError } from './../error/ElementNotFoundError.mjs';
import { EditingCapactityError } from '../error/EditingCapacityError.mjs';
import { compareNaturalOrder, binarySearch } from './BinarySearch.mjs';
import { nonNull } from '../assertions.mjs';
import { RdfGraphData } from './RdfGraphData.mjs';

/**
 * Plain object representation for (de)serialisation purposes.
 *
 * @typedef {object} HDTDictionaryPojo
 * @property {number} AddonCapacity
 * @property {string []} DictionarySO
 * @property {string []} DictionaryS
 * @property {string []} DictionarySAddON
 * @property {string []} DictionaryO
 * @property {string []} DictionaryOAddON
 * @property {string []} DictionaryP
 * @property {string []} DictionaryPAddON
 * @property {number} PredicateGap
 * @property {number} ObjectGap
 * @property {Map<number, number>} ObjectSubjectMap
 */

class HDTDictionary {
    /**
     * Maximum number of Elements that can be added to one dictionary after creation
     * @type{number}
     */
    #AddonCapacity;

    /**
     * Map Of SO Strings to IDs
     * @type {string[]}
     */
    #DictionarySO;

    /**
     * Map Of S Strings to IDs
     * @type {string[]}
     */
    #DictionaryS;

    /**
     * Extension Of S Dictionary
     * @type {string[]}
     */
    #DictionarySAddON;

    /**
     * Map Of P Strings to IDs
     * @type {string[]}
     */
    #DictionaryO;

    /**
     * Extension Of O Dictionary
     * @type {string[]}
     */
    #DictionaryOAddON;

    /**
     * Map Of O Strings to IDs
     * @type {string[]}
     */
    #DictionaryP;

    /**
     * Extension Of P Dictionary
     * @type {string[]}
     */
    #DictionaryPAddON;

    /**
     * @type {number}
     */
    #PredicateGap;

    /**
     * @type {number}
     */
    #ObjectGap;

    /**
     * Mapping of Object IDs to Subject IDs that are used to create SO links so that the SO links between elements can be created
     * @type {Map<number, number>}
     */
    #ObjectSubjectMap;

    /**
     * Create Alphabetical ordered Dictionary from given RdfGraphData
     * @param {RdfGraphData} data
     * @param {number} addonCapacity maximum Number of Elements that can be added to the Dictionary after creation
     */
    constructor(data, addonCapacity) {
        /**
         * @type {Set<string>}
         */
        const SetSO = new Set();
        /**
         * @type {Set<string>}
         */
        const SetS = new Set();
        /**
         * @type {Set<string>}
         */
        const SetO = new Set();
        /**
         * @type {Set<string>}
         */
        const SetP = new Set();

        for (const triple of data.links) {
            SetP.add(triple.name);

            const subj = data.nodeAtId(triple.subject);
            if (!SetSO.has(subj.name)) {
                if (SetO.has(subj.name)) {
                    SetSO.add(subj.name);
                    SetO.delete(subj.name);
                } else {
                    SetS.add(subj.name);
                }
            }

            const obj = data.nodeAtId(triple.object);
            if (!SetSO.has(obj.name)) {
                if (SetS.has(obj.name)) {
                    SetSO.add(obj.name);
                    SetS.delete(obj.name);
                } else {
                    SetO.add(obj.name);
                }
            }
        }

        this.#DictionarySO = Array.from(SetSO).sort(compareNaturalOrder);
        this.#DictionaryS = Array.from(SetS).sort(compareNaturalOrder);
        this.#DictionaryP = Array.from(SetP).sort(compareNaturalOrder);
        this.#DictionaryO = Array.from(SetO).sort(compareNaturalOrder);
        this.#DictionarySAddON = [];
        this.#DictionaryPAddON = [];
        this.#DictionaryOAddON = [];
        this.#ObjectSubjectMap = new Map();
        this.#AddonCapacity = addonCapacity;
        this.#PredicateGap =
            this.#DictionarySO.length + this.#DictionaryS.length + this.#AddonCapacity;
        this.#ObjectGap = this.#PredicateGap + this.#DictionaryP.length + this.#AddonCapacity;
    }

    get SubjectObjectDictionary() {
        return this.#DictionarySO;
    }

    get SubjectDictionary() {
        return this.#DictionaryS;
    }

    get PredicateDictionary() {
        return this.#DictionaryP;
    }

    get ObjectDictionary() {
        return this.#DictionaryO;
    }

    get PredicateGap() {
        return this.#PredicateGap;
    }

    get ObjectGap() {
        return this.#ObjectGap;
    }

    get CurrentPredicateGap() {
        return this.#DictionarySO.length + this.#DictionaryS.length + this.#DictionarySAddON.length;
    }

    get CurrentObjectGap() {
        return this.CurrentPredicateGap + this.#DictionaryP.length + this.#DictionaryPAddON.length;
    }

    /**
    /**
     * Convert this into a plain object for serialisation purposes.
     *
     * Note: does not do any versioning, i.e. implementation changes
     * after serialising *will* result in broken deserialisation.
     *
     * @return {HDTDictionaryPojo}
     */
    toPojo() {
        return {
            AddonCapacity: this.#AddonCapacity,
            DictionarySO: this.#DictionarySO,
            DictionaryS: this.#DictionaryS,
            DictionarySAddON: this.#DictionarySAddON,
            DictionaryP: this.#DictionaryP,
            DictionaryPAddON: this.#DictionaryPAddON,
            DictionaryO: this.#DictionaryO,
            DictionaryOAddON: this.#DictionaryOAddON,
            PredicateGap: this.#PredicateGap,
            ObjectGap: this.#ObjectGap,
            ObjectSubjectMap: this.#ObjectSubjectMap,
        };
    }

    /**
     * Recreate from existing state in pojo.
     *
     * Note: does not do any versioning, i.e. implementation changes
     * after serialising *will* result in broken deserialisation.
     *
     * @param {HDTDictionaryPojo} pojo
     * @returns {HDTDictionary}
     */
    static fromPojo(pojo) {
        const dictionary = new HDTDictionary(new RdfGraphData([], []), pojo.AddonCapacity);
        dictionary.#DictionarySO = pojo.DictionarySO;
        dictionary.#DictionaryS = pojo.DictionaryS;
        dictionary.#DictionarySAddON = pojo.DictionarySAddON;
        dictionary.#DictionaryP = pojo.DictionaryP;
        dictionary.#DictionaryPAddON = pojo.DictionaryPAddON;
        dictionary.#DictionaryO = pojo.DictionaryO;
        dictionary.#DictionaryOAddON = pojo.DictionaryOAddON;
        dictionary.#PredicateGap = pojo.PredicateGap;
        dictionary.#ObjectGap = pojo.ObjectGap;
        // see https://github.com/hildjj/node-cbor/issues/37:
        // node-cbor doesn't distinguish empty map and object.
        // Result: if ObjectSubjectMap is empty during serialisation,
        // it's an object instead of Map after deserialisation.
        // This breaks things, for obvious reasons.
        // And because checking for a map with typeof would be sensible and easy,
        // it has to be done with `myVar.constructor === Map`.
        dictionary.#ObjectSubjectMap =
            pojo.ObjectSubjectMap.constructor === Map ? pojo.ObjectSubjectMap : new Map();
        return dictionary;
    }

    /**
     * Removes the SO Offset from an S or O ID
     * @param {number} id S or O ID to remove offset from
     * @returns {number} ID witout offset
     */
    #removeSOIDOffset(id) {
        return id - this.#DictionarySO.length;
    }

    /**
     * Check if the given ID is a ID of a Subject that is also a Object
     * @param {number} id ID to check
     * @param {number} soLength length of the SO Dictionary
     * @returns {boolean} True if the given ID is a SO ID
     */
    #checkIDisSOID(id, soLength) {
        if (id < soLength) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param {number} id
     * @returns Subject with the given ID
     */
    getSubject(id) {
        if (this.#checkIDisSOID(id, this.#DictionarySO.length)) {
            return this.#DictionarySO[id];
        }
        const idWithoutOffset = this.#removeSOIDOffset(id);
        if (idWithoutOffset < this.#DictionaryS.length) {
            return this.#DictionaryS[idWithoutOffset];
        }
        if (idWithoutOffset < this.#DictionaryS.length + this.#DictionarySAddON.length) {
            return this.#DictionarySAddON[idWithoutOffset - this.#DictionaryS.length];
        }
        throw new ElementNotFoundError(`Cannot find Subject with ID: "${id}" in this database`);
    }

    /**
     *
     * @param {string | null} value
     * @returns ID of the Subject
     */
    getSubjectID(value) {
        if (value === null) {
            throw new Error('Cant find index of Element with value null');
        }
        let id = binarySearch(this.#DictionaryS, value);
        if (id !== -1) {
            return id + this.#DictionarySO.length;
        }
        id = binarySearch(this.#DictionarySO, value);
        if (id !== -1) {
            return id;
        }
        for (const [i, element] of this.#DictionarySAddON.entries()) {
            if (element === value) {
                return i + this.#DictionarySO.length + this.#DictionaryS.length;
            }
        }
        throw new ElementNotFoundError(`Cannot find Subject "${value}" in this database`);
    }

    /**
     * Add Subject with the given value to the Dictionary
     * @param {string} value
     */
    addSubject(value) {
        if (this.#DictionarySAddON.length >= this.#AddonCapacity) {
            throw new EditingCapactityError();
        }
        let id = -1;
        try {
            id = this.getSubjectID(value);
        } catch (error) {
            if (error instanceof ElementNotFoundError) {
                //we just need to check if Element was found so this is probably more often the case
            } else {
                throw error;
            }
        }
        if (id !== -1) {
            return;
        }
        this.#DictionarySAddON.push(value);
        try {
            id = this.getObjectID(value);
        } catch (error) {
            if (error instanceof ElementNotFoundError) {
                //we just need to check if Element was found so this is probably more often the case
            } else {
                throw error;
            }
        }
        if (id !== -1) {
            this.#ObjectSubjectMap.set(
                id - this.CurrentObjectGap,
                this.#DictionaryS.length +
                    this.#DictionarySAddON.length +
                    this.#DictionarySO.length -
                    1 /* Calculate ID of New Subject */,
            );
        }
    }

    /**
     *
     * @param {number} id
     * @returns Predicate with the given ID
     */
    getPredicate(id) {
        if (id < this.#DictionaryP.length) {
            return this.#DictionaryP[id];
        }
        if (id < this.#DictionaryP.length + this.#DictionaryPAddON.length) {
            return this.#DictionaryPAddON[id - this.#DictionaryP.length];
        }
        throw new ElementNotFoundError(`Cannot find Predicate with ID: "${id}" in this database`);
    }

    /**
     *
     * @param {number} id
     * @returns Predicate with the given ID
     */
    getPredicateWithGap(id) {
        return this.getPredicate(id - this.CurrentPredicateGap);
    }

    /**
     *
     * @param {string | null} value
     * @returns ID of the Predicate
     */
    getPredicateID(value) {
        if (value === null) {
            throw new Error('Cant find index of Element with value null');
        }
        let idWithoutGap = binarySearch(this.#DictionaryP, value);
        if (idWithoutGap >= 0) {
            return idWithoutGap + this.CurrentPredicateGap;
        }
        for (const [i, element] of this.#DictionaryPAddON.entries()) {
            if (element === value) {
                return i + this.#DictionaryP.length + this.CurrentPredicateGap;
            }
        }
        throw new ElementNotFoundError(`Cannot find Predicate "${value}" in this database`);
    }

    /**
     * Add Predicate with the given Value to Dictionary
     * @param {string} value
     */
    addPredicate(value) {
        if (this.#DictionaryPAddON.length >= this.#AddonCapacity) {
            throw new EditingCapactityError();
        }
        let id = -1;
        try {
            id = this.getPredicateID(value);
        } catch (error) {
            if (error instanceof ElementNotFoundError) {
                //we just need to check if Element was found so this is probably more often the case
            } else {
                throw error;
            }
        }
        if (id !== -1) {
            return;
        }
        this.#DictionaryPAddON.push(value);
    }

    /**
     *
     * @param {number} id
     * @returns Object with the given ID
     */
    getObject(id) {
        if (this.#checkIDisSOID(id, this.#DictionarySO.length)) {
            return this.#DictionarySO[id];
        }
        const idWithoutOffset = this.#removeSOIDOffset(id);
        if (idWithoutOffset < this.#DictionaryO.length) {
            return this.#DictionaryO[idWithoutOffset];
        }
        if (idWithoutOffset < this.#DictionaryO.length + this.#DictionaryOAddON.length) {
            return this.#DictionaryOAddON[idWithoutOffset - this.#DictionaryO.length];
        }
        throw new ElementNotFoundError(`Cannot find Object with ID: "${id}" in this database`);
    }

    /**
     *
     * @param {number} id the id with gap
     * @returns Object with the given ID
     */
    getObjectWithGap(id) {
        return this.getObject(id - this.CurrentObjectGap);
    }

    /**
     *
     * @param {string | null} value
     * @returns ID of the Object
     */
    getObjectID(value) {
        if (value === null) {
            throw new Error('Cant find index of Element with value null');
        }

        let id = binarySearch(this.#DictionaryO, value);
        if (id !== -1) {
            return id + this.CurrentObjectGap + this.#DictionarySO.length;
        }
        let idWithoutGap = binarySearch(this.#DictionarySO, value);
        if (idWithoutGap !== -1) {
            return idWithoutGap + this.CurrentObjectGap;
        }
        for (const [i, element] of this.#DictionaryOAddON.entries()) {
            if (element === value) {
                return (
                    i + this.#DictionarySO.length + this.#DictionaryO.length + this.CurrentObjectGap
                );
            }
        }
        throw new ElementNotFoundError(`Cannot find Object "${value}" in this database`);
    }

    /**
     * checks if the id is an SO ID and returns the Subject ID it is a SO Object
     * @param {number} origID
     * @returns ID of the Object
     */
    getObjectNodeIDforGraphCreation(origID) {
        if (origID - this.CurrentObjectGap < this.#DictionarySO.length) {
            return origID - this.CurrentObjectGap;
        }
        if (this.#ObjectSubjectMap.has(origID - this.CurrentObjectGap)) {
            let dummy = nonNull(this.#ObjectSubjectMap.get(origID - this.CurrentObjectGap));
            return dummy;
        }
        return origID;
    }

    /**
     * Add given value as new Object to the Object dictionary
     * @param {string} value
     */
    addObject(value) {
        if (this.#DictionaryOAddON.length >= this.#AddonCapacity) {
            throw new EditingCapactityError();
        }
        let id = -1;
        try {
            id = this.getObjectID(value);
        } catch (error) {
            if (error instanceof ElementNotFoundError) {
                //we just need to check if Element was found so this is probably more often the case
            } else {
                throw error;
            }
        }
        if (id !== -1) {
            return;
        }
        this.#DictionaryOAddON.push(value);
        try {
            id = this.getSubjectID(value);
        } catch (error) {
            if (error instanceof ElementNotFoundError) {
                //we just need to check if Element was found so this is probably more often the case
            } else {
                throw error;
            }
        }
        if (id !== -1) {
            this.#ObjectSubjectMap.set(
                this.#DictionaryOAddON.length +
                    this.#DictionaryO.length +
                    this.#DictionarySO.length -
                    1 /* Calculate ID of New Object */,
                id,
            );
        }
    }
}

export { HDTDictionary };
