// realized as a type,
// so that type checking the static properties
// & functions works
//
// Usage:
// /** @type {BitVectorStatic} */
// const _typeCheck = MyImplementation;

type BitVectorStatic<B extends BitVector> = {
    /**
     * Recreate a BitVector instance from its serialised representation.
     *
     * Only use this with data
     * gotten from the same concrete implementation of this interface.
     *
     * @returns {BitVector}
     */
    fromPojo(pojo: object);
};

/**
 * Interface for a bitmap B[1,n] as discussed in the RDFCSA paper.
 *
 * Invalid indices are reported by exceptions.
 */
export interface BitVector {
    readonly length: number;

    append(value: boolean);
    insert(index: number, value: boolean);

    /**
     * Number of 1's at index.
     */
    rank(index: number): number;

    /**
     * If `rank` is not (or never if rank==0 && B[0]) reached,
     * this.length + 1 is returned.
     */
    select(rank: number): number;

    /**
     * For testing only.
     *
     * (Needed to verify the BitVector has been correctly
     * assembled in the RDFCSA constructor)
     */
    _at(i: number): boolean | undefined;

    /**
     * Convert this into a plain object for serialisation purposes.
     *
     * @return {object}
     */
    toPojo();
}
