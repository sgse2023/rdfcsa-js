// @ts-check

import { nonNull } from '../assertions.mjs';

/** @typedef {import('./BitVector').BitVector} IBitVector */
/**
 * @template {IBitVector} B
 * @typedef {import('./BitVector').BitVectorStatic<B>} BitVectorStatic
 * */

/**
 * Plain object representation for (de)serialisation purposes.
 *
 * @typedef {object} IndexBitVectorPojo
 * @property {object} bitVector
 */

/**
 * Wraps an IBitVector for usage in 0based index contexts.
 *
 * That means:
 * - the values of this bit vector are 0 based: they start at 0 and go to n-1
 * - the 1 counts are offset by -1, i.e. the bitmap `110` has rank(1)==0
 *
 * @template {IBitVector} B
 */
class IndexBitVector {
    /**
     * @type {B}
     */
    #bitVector;

    /**
     * @param {B} bitVector
     */
    constructor(bitVector) {
        this.#bitVector = nonNull(bitVector);
    }

    get length() {
        return this.#bitVector.length;
    }

    /**
     * @param {boolean} value
     */
    append(value) {
        this.#bitVector.append(value);
    }

    /**
     * @param {number} index
     * @param {boolean} value
     */
    insert(index, value) {
        this.#bitVector.insert(index + 1, value);
    }

    /**
     * @param {number} index
     * @returns {number}
     */
    rank(index) {
        return this.#bitVector.rank(index + 1) - 1;
    }

    /**
     * @param {number} rank
     * @returns {number}
     */
    select(rank) {
        return this.#bitVector.select(rank + 1) - 1;
    }

    /**
     * @param {number} index
     * @returns {boolean}
     */
    _at(index) {
        return /** @type {any} */ (this.#bitVector._at(index + 1));
    }

    /**
     * Convert this into a plain object for serialisation purposes.
     *
     * Note: does not do any versioning, i.e. implementation changes
     * after serialising *will* result in broken deserialisation.
     *
     * @return {IndexBitVectorPojo}
     */
    toPojo() {
        return {
            bitVector: this.#bitVector.toPojo(),
        };
    }

    /**
     * Recreate from existing state in pojo.
     *
     * Note: does not do any versioning, i.e. implementation changes
     * after serialising *will* result in broken deserialisation here.
     *
     * Note: target *must* have the exact same type as
     * the BitVector used for constructing the originally serialized
     * BitVector in pojo.
     *
     * @template {IBitVector} B
     * @template {BitVectorStatic<B>} S
     * @param {IndexBitVectorPojo} pojo
     * @param {S} target
     * @returns {IndexBitVector<B>}
     */
    static fromPojo(pojo, target) {
        const bitVector = target.fromPojo(pojo.bitVector);
        const indexBitVector = new IndexBitVector(bitVector);
        return indexBitVector;
    }
}

export { IndexBitVector };
