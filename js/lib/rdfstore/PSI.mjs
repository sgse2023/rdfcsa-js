//@ts-check
import { SortedArray } from './../SortedArray.mjs';
import { nonNull } from '../assertions.mjs';

/** @typedef {import('./../SortedArray.mjs').SortedArrayPojo} SortedArrayPojo */

/**
 * Plain object representation for (de)serialisation purposes.
 *
 * @typedef {object} PsiPojo
 * @prop {number[]} S
 * @prop {number[]} P
 * @prop {number[]} O
 * @prop {SortedArrayPojo} Soffset
 * @prop {SortedArrayPojo} Poffset
 * @prop {SortedArrayPojo} Ooffset
 * @prop {number} origTripleCount
 * @prop {Map<number,number>} addedSubjTranslations
 * @prop {Map<number,number>} addedPredTranslations
 * @prop {Map<number,number>} addedObjTranslations
 * @prop {number} addonCapacity
 */

class PSI {
    /**
     * @type {number[]}
     */
    #S = [];
    /**
     * @type {number[]}
     */
    #P = [];
    /**
     * @type {number[]}
     */
    #O = [];

    /**
     * @type {SortedArray}
     */
    #Soffset;
    /**
     * @type {SortedArray}
     */
    #Poffset;
    /**
     * @type {SortedArray}
     */
    #Ooffset;

    /**
     * @type {number}
     */
    #origTripleCount;

    /**
     * @type {Map<number,number>}
     */
    #addedSubjTranslations;

    /**
     * @type {Map<number,number>}
     */
    #addedPredTranslations;

    /**
     * @type {Map<number,number>}
     */
    #addedObjTranslations;

    /**
     * @type {number}
     */
    #addonCapacity;

    /**
     *
     * @param {readonly number[]} a
     * @param  {number} addonCapacity
     */
    constructor(a, addonCapacity) {
        this.#origTripleCount = Math.floor(a.length / 3);
        this.#Soffset = new SortedArray();
        this.#Poffset = new SortedArray();
        this.#Ooffset = new SortedArray();
        this.#addonCapacity = addonCapacity;
        this.#addedSubjTranslations = new Map();
        this.#addedPredTranslations = new Map();
        this.#addedObjTranslations = new Map();

        /**
         * @type {Map<number,number>}
         */
        let tmpAValueToIndex = new Map();

        for (let i = 0; i < a.length; i++) {
            tmpAValueToIndex.set(nonNull(a[i]), i);
        }

        // get PSI values for subjects
        for (const val of a.slice(0, a.length / 3)) {
            this.#S.push(nonNull(tmpAValueToIndex.get(val + 1)) + addonCapacity);
        }

        // get PSI values for predicates
        for (const val of a.slice(a.length / 3, (a.length / 3) * 2)) {
            this.#P.push(nonNull(tmpAValueToIndex.get(val + 1)) + (addonCapacity * 2));
        }

        // get PSI values for objects; except the last element
        for (const val of a.slice((a.length / 3) * 2, a.length - 1)) {
            // -2 instead of + 1 since we want the index of the first element of the triple (s); and this is faster than adjjusting PSI afterwards
            this.#O.push(nonNull(tmpAValueToIndex.get(val - 2)));
        }

        // get PSI for the last Element of A
        this.#O.push((a.length / 3) - 1);
    }

    /**
     *
     * @param {number} index
     */
    get(index) {
        let indexWithOffset = index;
        if (indexWithOffset < this.#S.length) {
            // check of S[index] is the actual value of a placeholder for the dict
            let rawValue = nonNull(this.#S[indexWithOffset]);
            if (
                this.#origTripleCount <= rawValue &&
                rawValue < this.#origTripleCount + this.#addonCapacity
            ) {
                // rawValue has been inserted
                return this.#addedSubjTranslations.get(rawValue);
            }
            // rawValue has not been inserted after psi creation
            let offset = this.#Poffset.findInexact(rawValue);
            return rawValue + offset;
        }
        indexWithOffset -= this.#addonCapacity + this.#origTripleCount;
        if (indexWithOffset < this.#P.length) {
            // check of P[index] is the actual value of a placeholder for the dict
            let rawValue = nonNull(this.#P[indexWithOffset]);
            if (
                this.#origTripleCount <= rawValue &&
                rawValue < this.#origTripleCount + this.#addonCapacity
            ) {
                // rawValue has been inserted
                return this.#addedPredTranslations.get(rawValue);
            }
            // rawValue has not been inserted after psi creation
            let offset = this.#Ooffset.findInexact(rawValue);
            return rawValue + offset;
        }
        indexWithOffset -= this.#addonCapacity + this.#origTripleCount;
        if (indexWithOffset < this.#O.length) {
            // check of O[index] is the actual value of a placeholder for the dict
            let rawValue = nonNull(this.#O[indexWithOffset]);
            if (
                this.#origTripleCount <= rawValue &&
                rawValue < this.#origTripleCount + this.#addonCapacity
            ) {
                // rawValue has been inserted
                return this.#addedObjTranslations.get(rawValue);
            }
            // rawValue has not been inserted after psi creation
            let offset = this.#Soffset.findInexact(rawValue);
            return rawValue + offset;
        }
        throw new Error(`Index ${index} out of bound in PSI`);
    }

    /**
     *
     * @param {number} index
     * @param {number} value
     */
    insert(index, value) {
        let indexWithOffset = index;
        if (indexWithOffset <= this.#S.length) {
            this.#S.splice(indexWithOffset, 0, this.#S.length);
            this.#addedSubjTranslations.set(this.#S.length - 1, value);
            this.#Soffset.insert(index);
            // fix subsequent elements in addedTranslation due to insert
            for (const [placeholderIndex, actualIndex] of this.#addedObjTranslations.entries()) {
                if (actualIndex > index) {
                    this.#addedObjTranslations.set(placeholderIndex, actualIndex + 1);
                }
            }
            return;
        }
        indexWithOffset -= this.#origTripleCount + this.#addonCapacity;
        if (indexWithOffset <= this.#P.length) {
            this.#P.splice(indexWithOffset, 0, this.#P.length);
            this.#addedPredTranslations.set(this.#P.length - 1, value);
            this.#Poffset.insert(index);
            for (const [placeholderIndex, actualIndex] of this.#addedSubjTranslations.entries()) {
                if (actualIndex > index) {
                    this.#addedSubjTranslations.set(placeholderIndex, actualIndex + 1);
                }
            }
            return;
        }
        indexWithOffset -= this.#origTripleCount + this.#addonCapacity;
        if (indexWithOffset <= this.#O.length) {
            this.#O.splice(indexWithOffset, 0, this.#O.length);
            this.#addedObjTranslations.set(this.#O.length - 1, value);
            this.#Ooffset.insert(index);
            for (const [placeholderIndex, actualIndex] of this.#addedPredTranslations.entries()) {
                if (actualIndex > index) {
                    this.#addedPredTranslations.set(placeholderIndex, actualIndex + 1);
                }
            }
            return;
        }

        throw new Error(`Index ${index} out of bound in PSI`);
    }

    get maximumIndex() {
        return 3 * (this.#origTripleCount + this.#addonCapacity);
    }

    get length() {
        return this.#O.length + this.#S.length + this.#P.length;
    }

    /**
     * Convert this into a plain object for serialisation purposes.
     *
     * Note: does not do any versioning, i.e. implementation changes
     * after serialising *will* result in broken deserialisation.
     *
     * @return {PsiPojo}
     */
    toPojo() {
        return {
            S: this.#S,
            P: this.#P,
            O: this.#O,
            Soffset: this.#Soffset.toPojo(),
            Poffset: this.#Poffset.toPojo(),
            Ooffset: this.#Ooffset.toPojo(),
            origTripleCount: this.#origTripleCount,
            addedSubjTranslations: this.#addedSubjTranslations,
            addedPredTranslations: this.#addedPredTranslations,
            addedObjTranslations: this.#addedObjTranslations,
            addonCapacity: this.#addonCapacity,
        };
    }

    /**
     * Recreate from existing state in pojo.
     *
     * Note: does not do any versioning, i.e. implementation changes
     * after serialising *will* result in broken deserialisation here.
     *
     * Note: target *must* have the exact same type as
     * the BitVector used for constructing the originally serialized
     * BitVector in pojo.
     *
     * @param {PsiPojo} pojo
     * @returns {PSI}
     */
    static fromPojo(pojo) {
        const psi = new PSI([], 0);
        psi.#S = pojo.S;
        psi.#P = pojo.P;
        psi.#O = pojo.O;
        psi.#Soffset = SortedArray.fromPojo(pojo.Soffset);
        psi.#Poffset = SortedArray.fromPojo(pojo.Poffset);
        psi.#Ooffset = SortedArray.fromPojo(pojo.Ooffset);
        psi.#origTripleCount = pojo.origTripleCount;
        // see https://github.com/hildjj/node-cbor/issues/37:
        // node-cbor doesn't distinguish empty map and object.
        // Result: if ObjectSubjectMap is empty during serialisation,
        // it's an object instead of Map after deserialisation.
        // This breaks things, for obvious reasons.
        // And because checking for a map with typeof would be sensible and easy,
        // it has to be done with `myVar.constructor === Map`.
        psi.#addedSubjTranslations =
            pojo.addedSubjTranslations.constructor === Map ? pojo.addedSubjTranslations : new Map();
        psi.#addedPredTranslations =
            pojo.addedPredTranslations.constructor === Map ? pojo.addedPredTranslations : new Map();
        psi.#addedObjTranslations =
            pojo.addedObjTranslations.constructor === Map ? pojo.addedObjTranslations : new Map();
        psi.#addonCapacity = pojo.addonCapacity;
        return psi;
    }
}

export { PSI };
