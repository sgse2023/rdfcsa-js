// @ts-check

import { nonNull } from '../assertions.mjs';

/** @typedef {import('./BitVector').BitVector} IBitVector */
/**
 * @typedef {import('./BitVector').BitVectorStatic<BitVector>} BitVectorStatic
 * */

/**
 * Plain object representation for (de)serialisation purposes.
 *
 * @typedef {object} BitVectorPojo
 * @prop {number} length
 * @prop {Uint8Array} values
 * @prop {Uint8Array} blocks
 * @prop {number} blocksLength
 * @prop {Uint32Array} superBlocks
 * @prop {number} superBlocksLength
 */

/**
 * @param {number} current length
 * @returns {number} new length
 */
function growLength(current) {
    if (current < 100) {
        return current + 20;
    }
    if (current < 1_000) {
        return current * 2;
    }
    if (current < 1_000_000) {
        return current * 10;
    }

    return current + 1_000_000;
}

/**
 * @param {Uint8Array| Uint32Array | number[]} arr
 * @param {number} val
 * @param {number} _start
 * @param {number} _end
 * @returns {number}
 */
function binSearchInexact(arr, val, _start, _end) {
    let start = _start;
    let end = _end;

    if (end < 0 || end > arr.length) {
        throw new RangeError(`cannot search to ${end} (valid: 0..=${arr.length})`);
    }
    if (start < 0) {
        throw new RangeError(`cannot calculate rank starting from ${start} (valid: 0..)`);
    }
    if (start > end) {
        throw new RangeError(`start must be < end`);
    }

    while (start + 1 < end) {
        let mid = Math.floor((start + end) / 2);

        if (typeof arr[mid] === 'undefined') {
            console.error(
                `called with start ${start} and end ${end} while arr is ${arr.length} long`,
            );
        }
        const current = nonNull(arr[mid]);
        if (current === val) {
            // If the bitvector contains many consecutive 0's,
            // neighboring blocks have the same value.
            // This is a problem if we search for that exact value,
            // because this method is supposed to return the earliest occurrence
            // of the index.
            // If the same value is within the search range,
            // a naive version returns the last index here,
            // thus violating the assumption of finding the earliest occurrence
            // of `val`.
            // Prevent that from happening,
            // by decreasing mid until it is at the occurrence in the search range.
            while (mid - 1 >= start && arr[mid - 1] === arr[mid]) {
                mid--;
            }
            return mid;
        } else if (current <= val) {
            start = mid;
        } else {
            end = mid;
        }
    }
    return end;
}

/**
 * @implements {IBitVector}
 */
class BitVector {
    /**
     * @type {number}
     */
    #length = 0;
    /**
     * @type {Uint8Array}
     */
    #values;

    /**
     * @type {Uint8Array}
     */
    #blocks;
    /**
     * stores number of blocks
     * that have a value in #blocks.
     * @type {number}
     */
    #blocksLength;

    /**
     * @type {Uint32Array}
     */
    #superblocks;
    /**
     * stores number of superblocks
     * that have a value in #superblocks.
     * @type {number}
     */
    #superblocksLength;

    /**
     * @returns {number}
     */
    get length() {
        return this.#length;
    }

    /**
     * @param {number} expectedSize preallocate internal storage
     */
    constructor(expectedSize = 0) {
        if (expectedSize < 0) {
            throw new RangeError(`can't have fewer than 0 elements`);
        }
        this.#values = new Uint8Array(expectedSize + 1);
        this.#blocks = new Uint8Array(Math.ceil(this.#values.length / 32));
        this.#superblocks = new Uint32Array(Math.ceil(this.#values.length / 256));
        this.#blocksLength = 1;
        this.#superblocksLength = 1;
    }

    /**
     * @param {boolean} value
     */
    append(value) {
        // +1: .length is last element (1-based)
        this.insert(this.length + 1, value);
    }

    #increaseLength() {
        this.#length++;

        // Math.max: special case length 0 at beginning
        const endIndex = Math.max(1, Math.ceil(this.length / 8));

        // is more space needed in #values?
        if (endIndex >= this.#values.length) {
            const valuesNew = new Uint8Array(growLength(this.#values.length));
            for (const [i, v] of this.#values.entries()) {
                valuesNew[i] = v;
            }
            this.#values = valuesNew;
        }
    }

    /**
     * @param {number} index
     * @param {boolean} value
     */
    insert(index, value) {
        // this.length + 1 is append (since this.length is last element)
        if (index < 1 || index > this.length + 2) {
            throw new RangeError(`cannot insert at ${index} (valid: 1..=${this.length})`);
        }

        if (index < this.length + 1) {
            this.#increaseLength();

            const valIndex = Math.ceil(index / 8);

            // 1. insert bit into correct byte

            // part of the uint8 before the inserted value
            let valBefore = nonNull(this.#values[valIndex]);
            // remember the last bit, which is shifted out of val
            // (and needs to be inserted into the next byte)
            let prevLastBit = valBefore & 128;
            // part of the uint8 after and including the inserted value
            let valWithAfter = valBefore;
            // shift values to make space for value
            valWithAfter <<= 1;
            // insert value
            if (value) {
                valWithAfter |= 1 << ((index - 1) % 8);
            } else {
                valWithAfter &= ~(1 << ((index - 1) % 8));
            }
            // combine original values before insertion index, inserted value
            // and bits after inserted value
            const mask = (2 ** ((index - 1) % 8)) - 1;
            valWithAfter &= ~mask;
            valBefore &= mask;
            this.#values[valIndex] = valWithAfter | valBefore;

            // 2. adjust & move all following bytes

            for (let i = valIndex + 1; i <= Math.ceil(this.length / 8); i++) {
                let val = nonNull(this.#values[i]);
                const lastBit = val & 128;

                val <<= 1;
                if (prevLastBit) {
                    val |= 1;
                } else {
                    val &= ~1;
                }
                this.#values[i] = val;

                prevLastBit = lastBit;
            }
        } else {
            this.#increaseLength();

            // Math.max: special case length 0 at beginning
            const valIndex = Math.max(1, Math.ceil(this.length / 8));

            // insert new value. no need to differentiate between
            // 'new' slot or higher bit in existing slot,
            // because 'new' slot already exists due to manually
            // managing the array size
            let val = nonNull(this.#values[valIndex]);
            if (value) {
                val |= 1 << ((index - 1) % 8);
            }
            this.#values[valIndex] = val;
        }

        // create block
        if (this.length % 32 === 0) {
            const newBlockRank = this.#rank(Math.max(1, this.length - 32 + 1), this.length);
            const blockIndex = Math.floor(this.length / 32);
            const previousBlockRank = nonNull(this.#blocks[blockIndex - 1]);

            if (blockIndex >= this.#blocks.length) {
                const blocksNew = new Uint8Array(growLength(this.#blocks.length));
                for (const [i, v] of this.#blocks.entries()) {
                    blocksNew[i] = v;
                }
                this.#blocks = blocksNew;
            }

            this.#blocks[blockIndex] = this.length % 256 ? newBlockRank + previousBlockRank : 0;
            this.#blocksLength++;
        }

        // create superblock
        if (this.length % 256 === 0) {
            const newSuperblockRank = this.#rank(Math.max(1, this.length - 256 + 1), this.length);
            const superblockIndex = Math.floor(this.length / 256);
            const previousSuperblockRank = nonNull(this.#superblocks[superblockIndex - 1]);

            if (superblockIndex >= this.#superblocks.length) {
                const superblocksNew = new Uint32Array(growLength(this.#superblocks.length));
                for (const [i, v] of this.#superblocks.entries()) {
                    superblocksNew[i] = v;
                }
                this.#superblocks = superblocksNew;
            }

            this.#superblocks[superblockIndex] = newSuperblockRank + previousSuperblockRank;
            this.#superblocksLength++;
        }

        // update blocks and superblocks after insert:
        // (since we already inserted, we no longer check for length + 1)
        if (index < this.length) {
            const nextBlockStart = index - ((index - 1) % 32) + 32;
            const blockIndex = Math.floor((nextBlockStart - 1) / 32);

            // since block values are cumulative: carry over adjusment
            // to following blocks
            let blockAdjustment = 0;

            // ignore every 8th block, where the cumulative value is reset
            if (blockIndex < this.#blocksLength && this.length !== 32 && blockIndex % 8 !== 0) {
                if (value && !this._at(nextBlockStart)) {
                    this.#blocks[blockIndex] = nonNull(this.#blocks[blockIndex]) + 1;
                    blockAdjustment += 1;
                } else if (!value && this._at(nextBlockStart)) {
                    this.#blocks[blockIndex] = nonNull(this.#blocks[blockIndex]) - 1;
                    blockAdjustment -= 1;
                }
            }

            for (let i = blockIndex + 1; i < this.#blocksLength; i += 1) {
                const movedInBit = ((i - 1) * 32) + 1;
                const movedOutBit = (i * 32) + 1;

                // cumulative block value is reset every 8 blocks
                if (i % 8 === 0) {
                    continue;
                }
                if ((i - 1) % 8 === 0) {
                    blockAdjustment = 0;
                }

                // adjustment needs to alway be applied (the previous block(s) changes influence
                // this block's value in any case)
                this.#blocks[i] = nonNull(this.#blocks[i]) + blockAdjustment;

                // if the length % 32 is 0 there is no bit after the last block,
                // which was then just created and does not need to be updated;
                if (i + 1 === this.#blocksLength && this.length % 32 === 0) {
                    continue;
                }

                if (this._at(movedInBit) && !this._at(movedOutBit)) {
                    this.#blocks[i] = nonNull(this.#blocks[i]) + 1;
                    blockAdjustment += 1;
                } else if (!this._at(movedInBit) && this._at(movedOutBit)) {
                    this.#blocks[i] = nonNull(this.#blocks[i]) - 1;
                    blockAdjustment -= 1;
                }
            }

            // Debugging: comment in blocks health checks
            //             for (let i = 1; i < this.#blocksLength; i++) {
            //                 if (i % 8 == 0) {
            //                     continue;
            //                 }
            //                 const blockVal = this.#blocks[i];
            //                 const rank = this.#rank(Math.max(1, ((i - ((i - 1) % 8)) * 32) - 31), i * 32);
            //                 if (rank != blockVal) {
            //                     console.error(
            //                         `value ${value}, length ${this.length}, block ${i}: expected ${rank} but is ${blockVal}`,
            //                     );
            //                     console.error(`blocklength ${this.#blocksLength}, blocks`, this.#blocks);
            //                     console.error(`values`, this.#values);
            //                     throw new Error('BUG: insertion calculated incorrect block value')
            //                 }
            //             }

            const nextSuperblockStart = index - ((index - 1) % 256) + 256;
            const superblockIndex = Math.floor((nextSuperblockStart - 1) / 256);

            // since superblock values are cumulative: carry over adjusment
            // to following blocks
            let superblockAdjustment = 0;

            if (superblockIndex < this.#superblocksLength && this.length !== 256) {
                if (value && !this._at(nextSuperblockStart)) {
                    this.#superblocks[superblockIndex] =
                        nonNull(this.#superblocks[superblockIndex]) + 1;
                    superblockAdjustment += 1;
                } else if (!value && this._at(nextSuperblockStart)) {
                    this.#superblocks[superblockIndex] =
                        nonNull(this.#superblocks[superblockIndex]) - 1;
                    superblockAdjustment -= 1;
                }
            }

            for (let i = superblockIndex + 1; i < this.#superblocksLength; i += 1) {
                const movedInBit = ((i - 1) * 256) + 1;
                const movedOutBit = (i * 256) + 1;

                // adjustment needs to alway be applied (the previous block(s) changes influence
                // this block's value in any case)
                this.#superblocks[i] = nonNull(this.#superblocks[i]) + superblockAdjustment;

                // if the length % 256 is 0 there is no bit after the last block,
                // which was then just created and does not need to be updated;
                if (i + 1 === this.#superblocksLength && this.length % 256 === 0) {
                    continue;
                }

                if (this._at(movedInBit) && !this._at(movedOutBit)) {
                    this.#superblocks[i] = nonNull(this.#superblocks[i]) + 1;
                    superblockAdjustment += 1;
                } else if (!this._at(movedInBit) && this._at(movedOutBit)) {
                    this.#superblocks[i] = nonNull(this.#superblocks[i]) - 1;
                    superblockAdjustment -= 1;
                }
            }

            // Debugging: comment in superblock health check
            // for (let i = 1; i < this.#superblocksLength; i++) {
            //     const superblockVal = this.#superblocks[i];
            //     const rank = this.#rank(1, i * 256);
            //     if (rank != superblockVal) {
            //         console.error(
            //             `length ${this.length}: expected ${rank} but is ${superblockVal}`,
            //         );
            //         console.error(`superblocks`, this.#superblocks);
            //     }
            // }
        }
    }

    /**
     * @param {number} start
     * @param {number} index
     * @returns {number}
     */
    #rank(start, index) {
        if (index < 1 || index > this.length) {
            throw new RangeError(`cannot calculate rank at ${index} (valid: 1..=${this.length})`);
        }
        if (start < 1) {
            throw new RangeError(`cannot calculate rank starting from ${start} (valid: 1..)`);
        }
        if (start > index) {
            throw new RangeError(`start must be < index`);
        }

        const startIndex = Math.ceil(start / 8);
        const valIndex = Math.ceil(index / 8);

        let rank = 0;
        for (const [i_, v_] of this.#values.slice(startIndex, valIndex + 1).entries()) {
            let val = v_;
            const i = i_ + 1; // 0based -> 1based

            if (i === 1) {
                val >>>= (start - 1) % 8;
            }

            // ignore higher bits (i.e. bigger indices) if index
            // is not at the end of a slot
            if (i + startIndex - 1 === valIndex) {
                // prettier-ignore
                const lookup = [
                    0b11111111,
                    0b00000001,
                    0b00000011,
                    0b00000111,
                    0b00001111,
                    0b00011111,
                    0b00111111,
                    0b01111111,
                ]
                val &= nonNull(lookup[index % 8]);
            }

            while (val) {
                rank += val & 1;
                val >>>= 1;
            }
        }

        return rank;
    }

    /**
     * @param {number} index
     * @returns {number}
     */
    rank(index) {
        const superblockIndex = Math.floor((index - 1) / 256);
        const superblockVal = nonNull(this.#superblocks[superblockIndex]);

        const blockIndex = Math.floor((index - 1) / 32);
        const blockVal = nonNull(this.#blocks[blockIndex]);

        const rankStart = Math.max(1, (blockIndex * 32) + 1);
        const rankEnd = rankStart + ((index - 1) % 32);
        const rank = this.#rank(rankStart, rankEnd);

        return superblockVal + blockVal + rank;
    }

    /**
     * @param {number} rank
     * @returns {number}
     */
    select(rank) {
        if (rank < 0) {
            throw new RangeError(`cannot search for rank ${rank} (must be >= 0)`);
        }

        if (rank === 0) {
            // look at first value to determine if rank 1 exists,
            // but only if that first value exists
            if (this.length) {
                return nonNull(this.#values[1]) & 1 ? -1 : 1;
            }
            return -1;
        }

        const superblockIndex = binSearchInexact(
            this.#superblocks,
            rank,
            0,
            this.#superblocksLength,
        );
        const blockRank = rank - nonNull(this.#superblocks[superblockIndex - 1]);

        const blockStartIndex = (superblockIndex - 1) * 8;
        const blockEndIndex = blockStartIndex + 8;
        let blockIndex = binSearchInexact(
            this.#blocks,
            blockRank,
            blockStartIndex,
            Math.min(this.#blocksLength, blockEndIndex),
        );
        let slotRank = blockRank - nonNull(this.#blocks[blockIndex - 1]);

        const slotStartIndex = Math.max((((blockIndex - 1) * 32) / 8) + 1, 1);
        const slotEndIndex = Math.floor(slotStartIndex + (32 / 8));

        let counter = 0;
        for (const [i, value] of this.#values.slice(slotStartIndex, slotEndIndex + 1).entries()) {
            let val = value;

            for (let j = 1; j <= 8; j++) {
                if (val & 1) {
                    counter++;
                    if (counter === slotRank) {
                        const superblockOffset = (superblockIndex - 1) * 256;
                        const blockOffset = ((blockIndex - 1) * 32) - superblockOffset;
                        const slotOffset = blockOffset + superblockOffset;
                        return (i * 8) + j + slotOffset;
                    }
                }
                val >>>= 1;
            }
        }

        return this.length + 1;
    }

    /**
     * @param {number} index
     * @returns {boolean}
     */
    _at(index) {
        if (index < 1 || index > this.length) {
            throw new RangeError(`cannot return value at ${index} (valid: 1..=${this.length})`);
        }

        const valIndex = Math.ceil(index / 8);
        const val = nonNull(this.#values[valIndex]);
        // eslint-disable-next-line no-unneeded-ternary
        return val & (1 << ((index - 1) % 8)) ? true : false;
    }

    /**
     * Convert this into a plain object for serialisation purposes.
     *
     * Note: does not do any versioning, i.e. implementation changes
     * after serialising *will* result in broken deserialisation.
     *
     * @returns {BitVectorPojo}
     */
    toPojo() {
        return {
            length: this.length,
            values: this.#values,
            blocks: this.#blocks,
            blocksLength: this.#blocksLength,
            superBlocks: this.#superblocks,
            superBlocksLength: this.#superblocksLength,
        };
    }

    /**
     * Recreate from existing state in pojo.
     *
     * Note: does not do any versioning, i.e. implementation changes
     * after serialising *will* result in broken deserialisation here.
     *
     * @param {BitVectorPojo} pojo
     * @returns {BitVector}
     */
    static fromPojo(pojo) {
        const b = new BitVector();
        b.#length = pojo.length;
        b.#values = pojo.values;
        b.#blocks = pojo.blocks;
        b.#blocksLength = pojo.blocksLength;
        b.#superblocks = pojo.superBlocks;
        b.#superblocksLength = pojo.superBlocksLength;
        return b;
    }
}

/**
 * @type {BitVectorStatic}
 */
const _typeCheck = BitVector;
_typeCheck;

export { BitVector };
