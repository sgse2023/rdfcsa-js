// @ts-check

/** https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort
 * @param {any} a
 * @param {any} b
 * @returns {number}
 */
function compareNaturalOrder(a, b) {
    if (a < b) {
        return -1;
    }
    if (a > b) {
        return 1;
    }
    // a must be equal to b
    return 0;
}

/**
 *  Do a Binary Search in an ordered string array and return id of element
 * @param {any[]} array
 * @param {any} element
 * @returns {number} id of element or -1 if not in list
 */
function binarySearch(array, element) {
    /**
     * @type {number}
     */
    let id = -1;

    let low = 0;
    let high = array.length - 1;

    if (high === -1) {
        return -1;
    }

    while (low <= high) {
        const mid = Math.floor((low + high) / 2);
        const order = compareNaturalOrder(array[mid], element);
        if (order === 0) {
            return mid;
        } else if (order === 1) {
            high = mid - 1;
        } else {
            low = mid + 1;
        }
    }
    return id;
}

export { compareNaturalOrder, binarySearch };
