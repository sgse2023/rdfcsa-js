// @ts-check
import { createSuffixArray } from '../SuffixArray.mjs';
import { nonNull } from '../assertions.mjs';
import { Triple } from '../rdfstore/Triple.mjs';
import { ImportTriple } from '../rdfstore/ImportTriple.mjs';
import { RdfGraphData } from './RdfGraphData.mjs';
import { SubjectObject } from './SubjectObject.mjs';
import { IndexBitVector } from './IndexBitVector.mjs';
import { PsiBitVector } from './PsiBitVector.mjs';
import { BitVector } from './BitVector.mjs';
import { QueryTripleElement } from '../query/QueryTripleElement.mjs';
import { TriplePattern } from './../query/TriplePattern.mjs';
import { HDTDictionary } from './HDTDicitonary.mjs';
import { compareNaturalOrder } from './BinarySearch.mjs';
import { SortedArray } from './../SortedArray.mjs';
import { decode as cborDecode } from 'cbor-x/decode';
import { Encoder } from 'cbor-x/encode';
import { PSI } from './PSI.mjs';
import { ElementNotFoundError } from '../error/ElementNotFoundError.mjs';
import { QUERY_ALL } from '../query/SimpleQuery.mjs';
import { RawTriple } from './RawTriple.mjs';

/** @typedef {import('./RdfStore').RdfStore} RdfStore */
/** @typedef {import('../conversion/Import').Import} Import */
/** @typedef {import('./HDTDicitonary.mjs').HDTDictionaryPojo} HDTDictionaryPojo */
/** @typedef {import('./PsiBitVector.mjs').PsiBitVectorPojo} PsiBitVectorPojo */
/** @typedef {import('./../SortedArray.mjs').SortedArrayPojo} SortedArrayPojo */
/** @typedef {import('./PSI.mjs').PsiPojo} PsiPojo */

/**
 * Plain object representation for (de)serialisation purposes.
 *
 * @typedef {object} RDFCSAPojo
 * @property {PsiBitVectorPojo} D
 * @property {HDTDictionaryPojo} Dictionary
 * @property {SortedArrayPojo} deletedTriples
 * @property {PsiPojo} PSI
 *
 * #CurrentQueryNodes and #currentID omitted,
 * since they are only of temporary nature
 * (used during query execution)
 */

/**
 * Maps the Dictionary to the Array
 * @param {RdfGraphData} data
 * @param {HDTDictionary} dictionary SO Dictionary
 * @returns {ImportTriple[]} Triples with corresponding IDs of the data Array
 */
function createIDArrayFromRdfGraphData(data, dictionary) {
    /**
     * @type {ImportTriple[]}
     */
    let idArray = [];
    for (const triple of data.links) {
        const subjName = data.nodeAtId(triple.subject).name;
        const objName = data.nodeAtId(triple.object).name;
        idArray.push(
            new ImportTriple(
                dictionary.getSubjectID(subjName),
                dictionary.getPredicateID(triple.name) - dictionary.PredicateGap,
                dictionary.getObjectID(objName) - dictionary.ObjectGap,
            ),
        );
    }
    return idArray;
}

/**
 * Modify the P and O indices so that they do not overlap with with respectively S and S & P
 * @param {ImportTriple[]} array
 * @param {number} predicateGap the Gap for Predicates
 * @param {number} objectGap the Gap for Objects
 */
function addGapToArrayIndices(array, predicateGap, objectGap) {
    /**
     * @type {number[]}
     */
    let gapArray = [];

    for (const value of array) {
        gapArray.push(value.subject);
        gapArray.push(predicateGap + value.predicate);
        gapArray.push(objectGap + value.object);
    }

    return gapArray;
}

/**
 * Create the Array A and create Array D alongside it
 * @param {number[]} array suffix Array to use
 * @returns {[number[], IndexBitVector<BitVector>]} A, D
 */
function createAAndD(array) {
    /**
     * @type {number[]}
     */
    let tmpA = [];

    /**
     * @type {IndexBitVector<BitVector>}
     */
    let tmpD = new IndexBitVector(new BitVector(array.length));

    /**
     * @type {([number, number])[]}
     */
    let tmpPredicates = [];
    /**
     * @type {([number, number])[]}
     */
    let tmpObjects = [];

    let lastNumber = -1;
    for (const [i, value] of array.entries()) {
        if (i % 3 === 0) {
            tmpA.push(i); //Subjects are already in Order
            if (lastNumber !== value) {
                tmpD.append(true);
                lastNumber = value;
            } else {
                tmpD.append(false);
            }
        }
        if (i % 3 === 1) {
            tmpPredicates.push([value, i]);
        }
        if (i % 3 === 2) {
            tmpObjects.push([value, i]);
        }
    }

    /**
     * @type {(a: [number, number], b: [number, number]) => (number)}
     */
    const compareByValue = ([a, _tIa], [b, _tIb]) => compareNaturalOrder(a, b);

    tmpPredicates = tmpPredicates.sort(compareByValue);

    lastNumber = -1;
    for (const [value, tIndex] of tmpPredicates) {
        if (lastNumber === value) {
            tmpD.append(false);
        } else {
            tmpD.append(true);
        }
        tmpA.push(tIndex);
        lastNumber = value;
    }

    tmpObjects = tmpObjects.sort(compareByValue);

    lastNumber = -1;
    for (const [value, tIndex] of tmpObjects) {
        if (lastNumber === value) {
            tmpD.append(false);
        } else {
            tmpD.append(true);
        }

        tmpA.push(tIndex);
        lastNumber = value;
    }

    return [tmpA, tmpD];
}

/**
 * @extends {RdfStore}
 */
class RDFCSA {
    /**
     * @type {PsiBitVector}
     */
    #D;

    /**
     * @type {HDTDictionary}
     */
    #dictionary;

    /**
     * @type {SortedArray}
     */
    #deletedTriples;

    /**
     * For testing only.
     *
     * Needed so that tests can check that D is correctly assembled.
     *
     * @param {number} i
     * @returns {number}
     */
    indexD(i) {
        return this.#D._at(i) ? 1 : 0;
    }

    /**
     * For testing only.
     *
     * @param {number} i
     * @returns {number}
     */
    getDRank1(i) {
        return this.#D.rank(i);
    }

    /**
     * For testing only.
     *
     * @param {number} rank
     * @returns {number}
     */
    getDSelect1(rank) {
        return this.#D.select(rank);
    }

    /**
     * @type {PSI}
     */
    #PSI;

    /**
     * List of Nodes for the current Query
     * @type {SubjectObject[]}
     */
    #CurrentQueryNodes;

    tripleCount() {
        return Math.floor(this.#PSI.length / 3);
    }

    /**
     * List of Nodes for the current Query
     * @type {number}
     */
    #currentID;

    /**
     *
     * @param {number} index
     * @returns the value of Psi at that index
     */
    getPSIIndex(index) {
        return this.#PSI.get(index);
    }

    /**
     *
     * @param {import('./../query/TextQuery.mjs').TextQuery} query
     * @param {number | undefined} tripleLimit
     * @returns {import('./../rdfstore/RdfGraphData.mjs').RdfGraphData}
     */
    // eslint-disable-next-line no-undefined
    execute(query, tripleLimit = undefined) {
        this.resetQueryAttributes();
        /**
         * @type {Triple[]}
         */
        let triples = [];
        if (query.getTriplePatterns().length === 0) {
            throw new Error('Cant exexute query without triple pattern');
        }
        if (query.getTriplePatterns().length === 1) {
            triples = this.executeSingleTriplePattern(
                nonNull(query.getTriplePatterns()[0]),
                tripleLimit,
            );
        } else {
            return this.executeMergeJoin(query, tripleLimit);
        }
        // Map usage: filter out duplicate variables
        // (i.e. the same variable occurs in multiple patterns,
        // but the return value should contain it only once)
        let nodes = Array.from(new Map(this.#CurrentQueryNodes.map((so) => [so.id, so])).values());
        return new RdfGraphData(nodes, triples);
    }

    /**
     * @param {RawTriple} triple
     */
    addTriple(triple) {
        let dictionaryInsertions = 0;
        // insert in hdt dict
        /**
         * @type {[number | undefined, number | undefined, number | undefined]}
         */
        // eslint-disable-next-line no-undefined
        let [subjId, predId, objId] = [undefined, undefined, undefined];
        let [subjAdded, predAdded, objAdded] = [false, false, false];
        try {
            subjId = this.#dictionary.getSubjectID(triple.subject);
        } catch (e) {
            if (e instanceof ElementNotFoundError) {
                dictionaryInsertions++;
                this.#dictionary.addSubject(triple.subject);
                subjId = this.#dictionary.getSubjectID(triple.subject);
                subjAdded = true;
            } else {
                throw e;
            }
        }
        try {
            predId = this.#dictionary.getPredicateID(triple.predicate);
        } catch (e) {
            if (e instanceof ElementNotFoundError) {
                dictionaryInsertions++;
                this.#dictionary.addPredicate(triple.predicate);
                predId = this.#dictionary.getPredicateID(triple.predicate);
                predAdded = true;
            } else {
                throw e;
            }
        }
        try {
            objId = this.#dictionary.getObjectID(triple.object);
        } catch (e) {
            if (e instanceof ElementNotFoundError) {
                this.#dictionary.addObject(triple.object);
                objId = this.#dictionary.getObjectID(triple.object);
                objAdded = true;
            } else {
                throw e;
            }
        }

        // calc pos where to insert into d and psi
        // todo: warning! this is not 100% the correct position
        // if (multiple) elements with the same sub / pred / obj already exists, the insert pos is
        // always statically the 2nd. this might break some optimizations (but these are not
        // currently implemented anyways
        let subjInsertId = this.#D.selectInsertPosition(subjId, subjAdded);
        let predInsertId = this.#D.selectInsertPosition(predId - (subjAdded ? 1 : 0), predAdded);
        let objInsertId = this.#D.selectInsertPosition(objId - dictionaryInsertions, objAdded);

        // insert triple into d
        this.#D.insert(subjInsertId, subjAdded);
        this.#D.insert(predInsertId, predAdded);
        this.#D.insert(objInsertId, objAdded);

        // insert triple into psi
        this.#PSI.insert(subjInsertId, predInsertId);
        this.#PSI.insert(predInsertId, objInsertId);
        this.#PSI.insert(objInsertId, subjInsertId);
    }

    /**
     * @param {RawTriple} triple
     */
    deleteTriple(triple) {
        //throw new Error('TODO');
        let l = this.#D.select(this.#dictionary.getSubjectID(triple.subject));
        let r = this.#D.select(this.#dictionary.getSubjectID(triple.subject) + 1) - 1;
        let l2 = this.#D.select(this.#dictionary.getPredicateID(triple.predicate));
        let r2 = this.#D.select(this.#dictionary.getPredicateID(triple.predicate) + 1) - 1;
        let l3 = this.#D.select(this.#dictionary.getObjectID(triple.object));
        let r3 = this.#D.select(this.#dictionary.getObjectID(triple.object) + 1) - 1;
        for (let i = l; i <= r; i++) {
            if (l2 <= nonNull(this.#PSI.get(i)) && nonNull(this.#PSI.get(i)) <= r2) {
                if (
                    l3 <= nonNull(this.#PSI.get(nonNull(this.#PSI.get(i)))) &&
                    nonNull(this.#PSI.get(nonNull(this.#PSI.get(i)))) <= r3
                ) {
                    // add PSI[subject] to deleted triples
                    this.#deletedTriples.insert(nonNull(this.#PSI.get(i)));
                    return;
                }
            }
        }
    }

    /**
     * @param {RawTriple} oldTriple
     * @param {RawTriple} newTriple
     */
    editTriple(oldTriple, newTriple) {
        this.deleteTriple(oldTriple);
        this.addTriple(newTriple);
    }

    /**
     * resets all atributes used for a query
     */
    resetQueryAttributes() {
        this.#CurrentQueryNodes = [];
        this.#currentID = 0;
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Region Queries
    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @param {import('./../query/TextQuery.mjs').TextQuery} query
     * @param {number | undefined} tripleLimit
     * @returns {import('./../rdfstore/RdfGraphData.mjs').RdfGraphData}
     */
    executeLeftChainingJoin(query, tripleLimit) {
        if (query.getTriplePatterns().length !== 2) {
            throw new Error('left-chaining-joins only work with 2 triple pattern');
        }
        if (query.getVariables().length !== 1) {
            throw new Error('left-chaining-joins only work with 1 variable');
        }
        this.resetQueryAttributes();
        return this.executeChainingJoinIndependentOrder(
            nonNull(query.getTriplePatterns()[0]),
            nonNull(query.getTriplePatterns()[1]),
            tripleLimit,
        );
    }

    /**
     *
     * @param {import('./../query/TextQuery.mjs').TextQuery} query
     * @param {number | undefined} tripleLimit
     * @returns {import('./../rdfstore/RdfGraphData.mjs').RdfGraphData}
     */
    executeRightChainingJoin(query, tripleLimit) {
        if (query.getTriplePatterns().length !== 2) {
            throw new Error('right-chaining-joins only work with 2 triple pattern');
        }
        if (query.getVariables().length !== 1) {
            throw new Error('right-chaining-joins only work with 1 variable');
        }
        this.resetQueryAttributes();
        return this.executeChainingJoinIndependentOrder(
            nonNull(query.getTriplePatterns()[1]),
            nonNull(query.getTriplePatterns()[0]),
            tripleLimit,
        );
    }

    /**
     * The Paper mentions that left-chaining and right-chaining works in the same way just with the order swapped.
     * So this method performs the chaining method with the first and second triple pattern and will be called by each method with the corresponding order of triple patterns
     * @param {import('./../query/TriplePattern.mjs').TriplePattern} firstTriplePattern
     * @param {import('./../query/TriplePattern.mjs').TriplePattern} secondTriplePattern
     * @param {number | undefined} tripleLimit
     * @returns {import('./../rdfstore/RdfGraphData.mjs').RdfGraphData}
     */
    executeChainingJoinIndependentOrder(firstTriplePattern, secondTriplePattern, tripleLimit) {
        /**
         * @type {Set<Number>}
         */
        let possibleVarialebValues = new Set();
        /**
         * @type {Triple[]}
         */
        let firstTriples = [];
        /**
         * @type {Triple[]}
         */
        let filteredTriples = [];
        /**
         * @type {SubjectObject[]}
         */
        let filteredNodes = [];

        if (firstTriplePattern.subject.isVariable) {
            firstTriples = this.executeSingleTriplePattern(firstTriplePattern, tripleLimit);
            for (const triple of firstTriples) {
                possibleVarialebValues.add(triple.subject);
            }
        } else if (firstTriplePattern.predicate.isVariable) {
            firstTriples = this.executeSingleTriplePattern(firstTriplePattern, tripleLimit);
            for (const triple of firstTriples) {
                possibleVarialebValues.add(triple.predicateId);
            }
        } else if (firstTriplePattern.object.isVariable) {
            firstTriples = this.executeSingleTriplePattern(firstTriplePattern, tripleLimit);
            for (const triple of firstTriples) {
                possibleVarialebValues.add(triple.object);
            }
        }
        let firstNodes = Array.from(
            new Map(this.#CurrentQueryNodes.map((so) => [so.id, so])).values(),
        );

        //The triples are collected from individual queries. Therefore, the triple ids overlap across queries. In order to solve this, the ids are reassigned. This variable is used to ensure the uniqueness of the new ids
        let newTripleID = 0;
        for (const value of possibleVarialebValues) {
            let secondSubject = secondTriplePattern.subject;
            let secondPredicate = secondTriplePattern.predicate;
            let secondObject = secondTriplePattern.object;
            if (secondTriplePattern.subject.isVariable) {
                secondSubject = new QueryTripleElement(
                    nonNull(this.#dictionary.getSubject(value)),
                    false,
                );
            } else if (secondTriplePattern.predicate.isVariable) {
                secondPredicate = new QueryTripleElement(
                    nonNull(this.#dictionary.getPredicate(value)),
                    false,
                );
            } else if (secondTriplePattern.object.isVariable) {
                secondObject = new QueryTripleElement(
                    nonNull(this.#dictionary.getObject(value)),
                    false,
                );
            }
            let secondTriples = this.executeSingleTriplePattern(
                new TriplePattern(secondSubject, secondPredicate, secondObject),
                tripleLimit,
            );
            if (secondTriples.length > 0) {
                for (const triple of secondTriples) {
                    filteredTriples.push(
                        new Triple(
                            triple.subject,
                            triple.object,
                            triple.predicateId,
                            triple.name,
                            newTripleID++,
                        ),
                    );
                }
                filteredNodes = filteredNodes.concat(
                    Array.from(new Map(this.#CurrentQueryNodes.map((so) => [so.id, so])).values()),
                );
            } else {
                possibleVarialebValues.delete(value);
            }
        }
        if (firstTriplePattern.subject.isVariable) {
            for (const triple of firstTriples) {
                if (possibleVarialebValues.has(triple.subject)) {
                    filteredTriples.push(
                        new Triple(
                            triple.subject,
                            triple.object,
                            triple.predicateId,
                            triple.name,
                            newTripleID++,
                        ),
                    );
                    filteredNodes.push(
                        nonNull(firstNodes.find((node) => node.id === triple.subject)),
                    );
                    filteredNodes.push(
                        nonNull(firstNodes.find((node) => node.id === triple.object)),
                    );
                }
            }
        } else if (firstTriplePattern.predicate.isVariable) {
            for (const triple of firstTriples) {
                if (possibleVarialebValues.has(triple.predicateId)) {
                    filteredTriples.push(
                        new Triple(
                            triple.subject,
                            triple.object,
                            triple.predicateId,
                            triple.name,
                            newTripleID++,
                        ),
                    );
                    filteredNodes.push(
                        nonNull(firstNodes.find((node) => node.id === triple.subject)),
                    );
                    filteredNodes.push(
                        nonNull(firstNodes.find((node) => node.id === triple.object)),
                    );
                }
            }
        } else if (firstTriplePattern.object.isVariable) {
            for (const triple of firstTriples) {
                if (possibleVarialebValues.has(triple.object)) {
                    filteredTriples.push(
                        new Triple(
                            triple.subject,
                            triple.object,
                            triple.predicateId,
                            triple.name,
                            newTripleID++,
                        ),
                    );
                    filteredNodes.push(
                        nonNull(firstNodes.find((node) => node.id === triple.subject)),
                    );
                    filteredNodes.push(
                        nonNull(firstNodes.find((node) => node.id === triple.object)),
                    );
                }
            }
        }

        let nodes = Array.from(new Map(filteredNodes.map((so) => [so.id, so])).values());
        let links = Array.from(new Map(filteredTriples.map((l) => [l.id, l])).values());
        return new RdfGraphData(nodes, links);
    }

    /**
     *
     * @param {import('./../query/TextQuery.mjs').TextQuery} query
     * @param {number | undefined} tripleLimit
     * @returns {import('./../rdfstore/RdfGraphData.mjs').RdfGraphData}
     */
    executeMergeJoin(query, tripleLimit) {
        this.resetQueryAttributes();
        /**
         * @type {Map<string,Map<number,number>>}
         */
        let variableOccurenceMap = new Map();
        let expectedVariableOccurences = new Map();
        query.getVariables().forEach((variable) => {
            variableOccurenceMap.set(nonNull(variable.value), new Map());
            expectedVariableOccurences.set(nonNull(variable.value), 0);
        });
        /**
         * @type {RdfGraphData[]}
         */
        let triplePatternResults = [];
        // go through each triple pattern
        query.getTriplePatterns().forEach((triplePattern) => {
            /**
             * @type {Map<string,Set<number>>}
             */
            let possibleValuesMap = new Map();
            query.getVariables().forEach((variable) => {
                possibleValuesMap.set(nonNull(variable.value), new Set());
            });
            // set all variables to unbound
            if (triplePattern.subject.isVariable) {
                expectedVariableOccurences.set(
                    triplePattern.subject.value,
                    expectedVariableOccurences.get(triplePattern.subject.value) + 1,
                );
            }
            if (triplePattern.predicate.isVariable) {
                expectedVariableOccurences.set(
                    triplePattern.predicate.value,
                    expectedVariableOccurences.get(triplePattern.predicate.value) + 1,
                );
            }
            if (triplePattern.object.isVariable) {
                expectedVariableOccurences.set(
                    triplePattern.object.value,
                    expectedVariableOccurences.get(triplePattern.object.value) + 1,
                );
            }
            // execute triple pattern
            let triples = this.executeSingleTriplePattern(nonNull(triplePattern), tripleLimit);
            // get possible values for each variable
            triples.forEach((triple) => {
                if (triplePattern.subject.isVariable) {
                    possibleValuesMap
                        .get(nonNull(triplePattern.subject.value))
                        ?.add(triple.subject);
                }
                if (triplePattern.predicate.isVariable) {
                    possibleValuesMap
                        .get(nonNull(triplePattern.predicate.value))
                        ?.add(triple.predicateId);
                }
                if (triplePattern.object.isVariable) {
                    possibleValuesMap.get(nonNull(triplePattern.object.value))?.add(triple.object);
                }
            });
            // for each variable for each value adjust the counter
            for (let [variableName, possibleValues] of possibleValuesMap) {
                possibleValues.forEach((value) => {
                    if (variableOccurenceMap.get(variableName)?.has(value)) {
                        let currentValue = variableOccurenceMap.get(variableName)?.get(value);
                        variableOccurenceMap
                            .get(variableName)
                            ?.set(value, nonNull(currentValue) + 1);
                    } else {
                        variableOccurenceMap.get(variableName)?.set(value, 1);
                    }
                });
            }
            // get the final rdfgraphdata and reset the query attributes
            let nodes = Array.from(
                new Map(this.#CurrentQueryNodes.map((so) => [so.id, so])).values(),
            );
            triplePatternResults.push(new RdfGraphData(nodes, triples));
            this.resetQueryAttributes();
        });
        //let numberofNeededOccurences = query.getTriplePatterns().length;
        /**
         * @type {SubjectObject []}
         */
        let filteredNodes = [];
        /**
         * @type {Triple []}
         */
        let filteredLinks = [];
        // filter the triples and nodes, so that if the element is a variable,
        // the triple and belonging nodes are only added if they are in the result of every triple pattern
        let tripleID = 0;
        loop1: for (let i = 0; i < triplePatternResults.length; i++) {
            for (const triple of nonNull(triplePatternResults[i]).links.values()) {
                let pushTriple = false;
                let currentTriplePattern = query.getTriplePatterns()[i];

                //  check subject
                if (currentTriplePattern?.subject.isVariable) {
                    // check if the triple is on the result of every triple pattern
                    if (
                        variableOccurenceMap
                            .get(nonNull(currentTriplePattern.subject.value))
                            ?.get(triple.subject) ===
                        expectedVariableOccurences.get(currentTriplePattern.subject.value)
                    ) {
                        // get the SubjectObjects for the subject and object of the triple
                        let subjectNode = triplePatternResults[i]?.nodes.find(
                            (node) => node.id === triple.subject,
                        );
                        let objectNode = triplePatternResults[i]?.nodes.find(
                            (node) => node.id === triple.object,
                        );
                        // add the link and nodes to the filtered lists
                        filteredNodes.push(nonNull(subjectNode));
                        filteredNodes.push(nonNull(objectNode));
                        pushTriple = true;
                    }
                }
                //  check predicte
                if (currentTriplePattern?.predicate.isVariable) {
                    // check if the triple is on the result of every triple pattern
                    if (
                        variableOccurenceMap
                            .get(nonNull(currentTriplePattern.predicate.value))
                            ?.get(triple.predicateId) ===
                        expectedVariableOccurences.get(currentTriplePattern.subject.value)
                    ) {
                        // get the SubjectObjects for the subject and object of the triple
                        let subjectNode = triplePatternResults[i]?.nodes.find(
                            (node) => node.id === triple.subject,
                        );
                        let objectNode = triplePatternResults[i]?.nodes.find(
                            (node) => node.id === triple.object,
                        );
                        // add the link and nodes to the filtered lists
                        filteredNodes.push(nonNull(subjectNode));
                        filteredNodes.push(nonNull(objectNode));
                        pushTriple = true;
                    }
                }
                //  check object
                if (currentTriplePattern?.object.isVariable) {
                    // check if the triple is on the result of every triple pattern
                    if (
                        variableOccurenceMap
                            .get(nonNull(currentTriplePattern.object.value))
                            ?.get(triple.object) ===
                        expectedVariableOccurences.get(currentTriplePattern.object.value)
                    ) {
                        // get the SubjectObjects for the subject and object of the triple
                        let subjectNode = triplePatternResults[i]?.nodes.find(
                            (node) => node.id === triple.subject,
                        );
                        let objectNode = triplePatternResults[i]?.nodes.find(
                            (node) => node.id === triple.object,
                        );
                        // add the link and nodes to the filtered lists
                        filteredNodes.push(nonNull(subjectNode));
                        filteredNodes.push(nonNull(objectNode));
                        pushTriple = true;
                    }
                }

                if (pushTriple) {
                    const newTriple = new Triple(
                        triple.subject,
                        triple.object,
                        triple.predicateId,
                        triple.name,
                        tripleID++,
                    );
                    filteredLinks.push(newTriple);
                }

                if (tripleLimit && filteredLinks.length >= tripleLimit) {
                    break loop1;
                }
            }
        }
        let nodes = Array.from(new Map(filteredNodes.map((so) => [so.id, so])).values());
        let links = Array.from(new Map(filteredLinks.map((l) => [l.id, l])).values());
        return new RdfGraphData(nodes, links);
    }

    /**
     *
     * @param {import('./../query/TriplePattern.mjs').TriplePattern} triplePattern
     * @param {number | undefined} tripleLimit
     * @returns {import('./../rdfstore/Triple.mjs').Triple[]}
     */
    executeSingleTriplePattern(triplePattern, tripleLimit) {
        /**
         * @type {Triple[]}
         */
        let triples = [];
        let numberOfBoundElements = this.getNumberofBoundElements(triplePattern);
        if (numberOfBoundElements === 0) {
            triples = this.retreiveFullGraph(tripleLimit);
        }
        if (numberOfBoundElements === 1) {
            triples = this.executeTriplePattern(triplePattern, false, tripleLimit);
        }
        if (numberOfBoundElements === 2) {
            triples = this.executeTriplePattern(triplePattern, true, tripleLimit);
        }
        if (numberOfBoundElements === 3) {
            triples = this.retreiveSingleTriple(triplePattern);
        }

        return triples;
    }

    /**
     *
     * @param {import('./../query/TriplePattern.mjs').TriplePattern} triplePattern
     * @returns {import('./../rdfstore/Triple.mjs').Triple[]}
     */
    retreiveSingleTriple(triplePattern) {
        let l = this.#D.select(this.#dictionary.getSubjectID(triplePattern.subject.value));
        let r = this.#D.selectEndPrevious(
            this.#dictionary.getSubjectID(triplePattern.subject.value) + 1,
        );
        let l2 = this.#D.select(this.#dictionary.getPredicateID(triplePattern.predicate.value));
        let r2 = this.#D.selectEndPrevious(
            this.#dictionary.getPredicateID(triplePattern.predicate.value) + 1,
        );
        let l3 = this.#D.select(this.#dictionary.getObjectID(triplePattern.object.value));
        let r3 = this.#D.selectEndPrevious(
            this.#dictionary.getObjectID(triplePattern.object.value) + 1,
        );
        for (let i = l; i <= r; i++) {
            if (this.#deletedTriples.find(nonNull(this.#PSI.get(i))) >= 0) {
                continue;
            }
            if (l2 <= nonNull(this.#PSI.get(i)) && nonNull(this.#PSI.get(i)) <= r2) {
                if (
                    l3 <= nonNull(this.#PSI.get(nonNull(this.#PSI.get(i)))) &&
                    nonNull(this.#PSI.get(nonNull(this.#PSI.get(i)))) <= r3
                ) {
                    const subjectId = this.#dictionary.getSubjectID(triplePattern.subject.value);
                    const objectId = this.#dictionary.getObjectID(triplePattern.object.value);

                    const subject = new SubjectObject(
                        subjectId,
                        nonNull(this.#dictionary.getSubject(subjectId)),
                    );
                    const object = new SubjectObject(
                        objectId,
                        nonNull(this.#dictionary.getObjectWithGap(objectId)),
                    );

                    this.#CurrentQueryNodes.push(subject);
                    this.#CurrentQueryNodes.push(object);

                    return [
                        new Triple(
                            subjectId,
                            objectId,
                            this.#dictionary.getPredicateID(triplePattern.predicate.value),
                            nonNull(triplePattern.predicate.value),
                            0,
                        ),
                    ];
                }
            }
        }
        return [];
    }

    /**
     * @param {number | undefined} tripleLimit
     * @returns {import('./../rdfstore/Triple.mjs').Triple[] | []}
     */
    retreiveFullGraph(tripleLimit) {
        /**
         * @type {Triple[]}
         */
        let triples = [];
        for (let i = 0; i < this.#D.length / 3; i++) {
            let triple = this.retreiveTriple(i, 'S');
            triple ? triples.push(triple) : null;

            if (tripleLimit && triples.length >= tripleLimit) {
                return triples;
            }
        }
        return triples;
    }

    /**
     *
     * @param {import('./../query/TriplePattern.mjs').TriplePattern} triplePattern the triple pattern of the query
     * @param {boolean} twoBoundElements true if the triple pattern has two bound elements
     * @param {number | undefined} tripleLimit
     * @returns {import('./../rdfstore/Triple.mjs').Triple[]}
     */
    executeTriplePattern(triplePattern, twoBoundElements, tripleLimit) {
        let suffix = this.getSuffixArray(triplePattern);
        /**
         * @type {Triple[]}
         */
        let triples = [];
        let l = this.#D.select(nonNull(suffix[0]));
        let r = this.#D.selectEndPrevious(nonNull(suffix[0]) + 1);
        // if there is only one bound Element, set [l,r] for the second Element to the whole range of PSI/D so that the check is always true
        let l2 = 0;
        let r2 = this.#PSI.maximumIndex;
        if (twoBoundElements) {
            l2 = this.#D.select(nonNull(suffix[1]));
            r2 = this.#D.selectEndPrevious(nonNull(suffix[1]) + 1);
        }
        for (let i = l; i <= r; i++) {
            if (l2 <= nonNull(this.#PSI.get(i)) && nonNull(this.#PSI.get(i)) <= r2) {
                if (triplePattern.subject.isBound && !triplePattern.object.isBound) {
                    let triple = this.retreiveTriple(i, 'S');
                    triple ? triples.push(triple) : null;
                } else if (triplePattern.predicate.isBound && !triplePattern.subject.isBound) {
                    let triple = this.retreiveTriple(i, 'P');
                    triple ? triples.push(triple) : null;
                } else if (triplePattern.object.isBound && !triplePattern.predicate.isBound) {
                    let triple = this.retreiveTriple(i, 'O');
                    triple ? triples.push(triple) : null;
                }
            }
            if (tripleLimit && triples.length >= tripleLimit) {
                return triples;
            }
        }
        return triples;
    }
    /**
     *
     * @param {number} i the index of the bound Element
     * @param {"S"|"P"|"O"} firstBound the first bound element of the triple pattern
     */
    retreiveTriple(i, firstBound) {
        if (firstBound === 'S') {
            if (this.#deletedTriples.find(nonNull(this.#PSI.get(i))) >= 0) {
                return;
            }
        } else if (firstBound === 'P') {
            if (
                this.#deletedTriples.find(
                    nonNull(this.#PSI.get(nonNull(this.#PSI.get(nonNull(this.#PSI.get(i)))))),
                ) >= 0
            ) {
                return;
            }
        } else if (firstBound === 'O') {
            if (this.#deletedTriples.find(nonNull(this.#PSI.get(nonNull(this.#PSI.get(i))))) >= 0) {
                return;
            }
        }
        let triple = [];
        // get current Element
        triple.push(this.#D.rank(i));
        // get second Element
        triple.push(this.#D.rank(nonNull(this.#PSI.get(i))));
        // get last Element
        triple.push(this.#D.rank(nonNull(this.#PSI.get(nonNull(this.#PSI.get(i))))));

        if (firstBound === 'S') {
            this.#CurrentQueryNodes.push(
                new SubjectObject(
                    nonNull(triple[0]),
                    nonNull(this.#dictionary.getSubject(nonNull(triple[0]))),
                ),
            );
            this.#CurrentQueryNodes.push(
                new SubjectObject(
                    this.#dictionary.getObjectNodeIDforGraphCreation(nonNull(triple[2])),
                    nonNull(this.#dictionary.getObjectWithGap(nonNull(triple[2]))),
                ),
            );
            return new Triple(
                nonNull(triple[0]),
                this.#dictionary.getObjectNodeIDforGraphCreation(nonNull(triple[2])),
                nonNull(triple[1]),
                nonNull(this.#dictionary.getPredicateWithGap(nonNull(triple[1]))),
                this.#currentID++,
            );
        } else if (firstBound === 'P') {
            this.#CurrentQueryNodes.push(
                new SubjectObject(
                    nonNull(triple[2]),
                    nonNull(this.#dictionary.getSubject(nonNull(triple[2]))),
                ),
            );
            this.#CurrentQueryNodes.push(
                new SubjectObject(
                    this.#dictionary.getObjectNodeIDforGraphCreation(nonNull(triple[1])),
                    nonNull(this.#dictionary.getObjectWithGap(nonNull(triple[1]))),
                ),
            );
            return new Triple(
                nonNull(triple[2]),
                this.#dictionary.getObjectNodeIDforGraphCreation(nonNull(triple[1])),
                nonNull(triple[0]),
                nonNull(this.#dictionary.getPredicateWithGap(nonNull(triple[0]))),
                this.#currentID++,
            );
        } else if (firstBound === 'O') {
            this.#CurrentQueryNodes.push(
                new SubjectObject(
                    nonNull(triple[1]),
                    nonNull(this.#dictionary.getSubject(nonNull(triple[1]))),
                ),
            );

            this.#CurrentQueryNodes.push(
                new SubjectObject(
                    this.#dictionary.getObjectNodeIDforGraphCreation(nonNull(triple[0])),
                    nonNull(this.#dictionary.getObjectWithGap(nonNull(triple[0]))),
                ),
            );
            return new Triple(
                nonNull(triple[1]),
                this.#dictionary.getObjectNodeIDforGraphCreation(nonNull(triple[0])),
                nonNull(triple[2]),
                nonNull(this.#dictionary.getPredicateWithGap(nonNull(triple[2]))),
                this.#currentID++,
            );
        }
        throw new Error('Unexpected value for first bound Element');
    }

    /**
     *
     * @param {import('./../query/TriplePattern.mjs').TriplePattern} triplePattern
     */
    getNumberofBoundElements(triplePattern) {
        let numberofBoundElements = 0;
        if (triplePattern.subject.isBound) {
            numberofBoundElements++;
        }
        if (triplePattern.predicate.isBound) {
            numberofBoundElements++;
        }
        if (triplePattern.object.isBound) {
            numberofBoundElements++;
        }
        return numberofBoundElements;
    }
    /**
     *
     * @param {import('./../query/TriplePattern.mjs').TriplePattern} triplePattern
     */
    getSuffixArray(triplePattern) {
        let suffix = [];
        if (triplePattern.subject.isBound) {
            suffix.push(this.#dictionary.getSubjectID(triplePattern.subject.value));
        }
        if (triplePattern.predicate.isBound) {
            suffix.push(this.#dictionary.getPredicateID(triplePattern.predicate.value));
        }
        if (triplePattern.object.isBound) {
            if (triplePattern.subject.isBound && !triplePattern.predicate.isBound) {
                suffix.unshift(this.#dictionary.getObjectID(triplePattern.object.value));
            } else {
                suffix.push(this.#dictionary.getObjectID(triplePattern.object.value));
            }
        }
        return suffix;
    }

    /**
     * Create Dicitonary and Data Structures from the
     * @param {RdfGraphData} data
     * @param {number} addonCapacity
     */
    constructor(data, addonCapacity = 100) {
        //create Dictionary
        this.#dictionary = new HDTDictionary(data, addonCapacity);

        //Map dictionary to Array
        let idArray = createIDArrayFromRdfGraphData(data, this.#dictionary);

        //create Suffix Array from IDs = Tid
        let Tid = createSuffixArray(idArray);

        //create Gap Array
        let T = addGapToArrayIndices(
            Tid,
            this.#dictionary.PredicateGap,
            this.#dictionary.ObjectGap,
        );

        const [A, D] = createAAndD(T);

        this.#D = new PsiBitVector(D, addonCapacity, A.length / 3);

        //let PSIorig = createPSIorig(AandD[0]);

        //this.#PSI = createPSI(PSIorig);
        this.#PSI = new PSI(A, addonCapacity);
        this.#CurrentQueryNodes = [];
        this.#currentID = 0;
        this.#deletedTriples = new SortedArray();
    }

    /**
     * @type {string}
     */
    static get fileEnding() {
        return '.rdfcsa';
    }

    static get mimetype() {
        return 'application/x-rdfcsa';
    }

    static get formatName() {
        return 'RDFCSA';
    }

    static get description() {
        return 'On-Disk representation of the RDFCSA datastructure';
    }

    get fileEnding() {
        return RDFCSA.fileEnding;
    }

    get mimetype() {
        return RDFCSA.mimetype;
    }

    get formatName() {
        return RDFCSA.formatName;
    }

    get description() {
        return RDFCSA.description;
    }

    get dictionary() {
        return this.#dictionary;
    }

    /**
     * Convert this into a plain object for serialisation purposes.
     *
     * Note: does not do any versioning, i.e. implementation changes
     * after serialising *will* result in broken deserialisation.
     *
     * @return {RDFCSAPojo}
     */
    toPojo() {
        return {
            D: this.#D.toPojo(),
            Dictionary: this.#dictionary.toPojo(),
            deletedTriples: this.#deletedTriples.toPojo(),
            PSI: this.#PSI.toPojo(),
        };
    }

    /**
     * Recreate from existing state in pojo.
     *
     * Note: does not do any versioning, i.e. implementation changes
     * after serialising *will* result in broken deserialisation here.
     *
     * @param {RDFCSAPojo} pojo
     * @return {RDFCSA}
     */
    static fromPojo(pojo) {
        const rdfcsa = new RDFCSA(new RdfGraphData([], []));
        rdfcsa.#D = PsiBitVector.fromPojo(pojo.D);
        rdfcsa.#dictionary = HDTDictionary.fromPojo(pojo.Dictionary);
        rdfcsa.#deletedTriples = SortedArray.fromPojo(pojo.deletedTriples);
        rdfcsa.#PSI = PSI.fromPojo(pojo.PSI);
        return rdfcsa;
    }

    /**
     *
     * @param {BinaryData} _fileContent
     * @returns {RdfGraphData | RdfStore}
     */
    static import(_fileContent) {
        const fileContent =
            _fileContent instanceof ArrayBuffer
                ? new Uint8Array(_fileContent)
                : new Uint8Array(
                      _fileContent.buffer,
                      _fileContent.byteOffset,
                      _fileContent.byteLength,
                  );

        return RDFCSA.fromPojo(cborDecode(fileContent));
    }

    /**
     * Not implemented
     * @param {BinaryData} _fileContent
     * @param {RdfStore} existing
     * @returns {boolean}
     */
    static add(_fileContent, existing) {
        const fileContent =
            _fileContent instanceof ArrayBuffer
                ? new Uint8Array(_fileContent)
                : new Uint8Array(
                      _fileContent.buffer,
                      _fileContent.byteOffset,
                      _fileContent.byteLength,
                  );

        const newData = RDFCSA.fromPojo(cborDecode(fileContent));

        const triples = newData.execute(QUERY_ALL);

        for (const value of triples.links) {
            existing.addTriple(
                new RawTriple(
                    triples.nodeAtId(value.subject).name,
                    value.name,
                    triples.nodeAtId(value.object).name,
                ),
            );
        }

        return true;
    }

    /**
     *
     * @param {RdfGraphData} triples
     *  @returns {BinaryData}
     */
    export(triples) {
        const rdfStore = new RDFCSA(triples);
        const rdfPojo = rdfStore.toPojo();
        const encoder = new Encoder({ structuredClone: true });
        return encoder.encode(rdfPojo);
    }
}

/**
 * @type {Import}
 */
const _typecheck = RDFCSA;
_typecheck;

export { RDFCSA, createIDArrayFromRdfGraphData, addGapToArrayIndices, createAAndD };
