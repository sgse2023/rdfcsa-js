// @ts-check

/**
 * Important: This must not use any code that only
 * works in a browser environment (or import any such code).
 * Use ../assertions-frontend.mjs for that.
 */

/**
 * @template {any} T
 * @param {T | undefined | null} e
 * @returns {T}
 */
function nonNull(e) {
    if (e === null || typeof e === 'undefined') {
        throw new Error(`Element is ${e} but is expected to be non-null!`);
    }
    return e;
}

export { nonNull };
