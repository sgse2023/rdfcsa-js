// realized as a type,
// so that type checking the static properties
// & functions works
//
// Usage:
// /** @type {Import} */
// const _typeCheck = MyImplementation;

import type { RdfGraphData } from '../rdfstore/RdfGraphData.mjs';
import type { RdfStore } from '../rdfstore/RdfStore';

export type Import = {
    /**
     * File ending of the format implemented.
     */
    fileEnding: string;
    /**
     * Mimetype of the format implemented.
     */
    mimetype: string;
    /**
     * Human-Readable name of the format implemented.
     */
    formatName: string;
    /**
     * Human-Readable description of the format implemented.
     */
    description: string;
    import(fileContent: BinaryData): RdfGraphData | RdfStore;
    add(fileContent: BinaryData, existing: RdfStore): boolean;
};
