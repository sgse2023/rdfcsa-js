// @ts-check

import N3 from 'n3';
import { validateIri, IriValidationStrategy } from 'validate-iri';

import { RdfGraphData } from '../rdfstore/RdfGraphData.mjs';
import { SubjectObject } from '../rdfstore/SubjectObject.mjs';
import { Triple } from '../rdfstore/Triple.mjs';
import { RawTriple } from '../rdfstore/RawTriple.mjs';

/** @typedef {import('n3').NamedNode} NamedNode */
/** @typedef {import('n3').BlankNode} BlankNode */
/** @typedef {import('n3').Literal} Literal */

/** @typedef {import('./Import').Import} Import */
/** @typedef {import('../rdfstore/RdfStore').RdfStore} RdfStore */
/** @typedef {import('./Export').Export} Export */

/**
 * @extends {Export}
 */
class TurtleFormat {
    /**
     * @type {string}
     */
    static get fileEnding() {
        return '.ttl';
    }

    static get mimetype() {
        return 'text/turtle';
    }

    static get formatName() {
        return 'Turtle';
    }

    static get description() {
        return 'Terse RDF Triple Language; syntax and file format for RDF data';
    }

    get fileEnding() {
        return TurtleFormat.fileEnding;
    }

    get mimetype() {
        return TurtleFormat.mimetype;
    }

    get formatName() {
        return TurtleFormat.formatName;
    }

    get description() {
        return TurtleFormat.description;
    }

    /**
     *
     * @param {BinaryData} fileContent
     * @returns {RdfGraphData | RdfStore}
     */
    static import(fileContent) {
        const parser = new N3.Parser({ format: 'Turtle', blankNodePrefix: '' });

        const subjectObject = [];
        const triples = [];

        const textDecoder = new TextDecoder();

        const quads = parser.parse(textDecoder.decode(fileContent));

        for (const [i, value] of quads.entries()) {
            subjectObject.push(new SubjectObject(i, value.subject.value));
            subjectObject.push(new SubjectObject(i + (quads.length * 2), value.object.value));
            triples.push(
                new Triple(i, i + (quads.length * 2), i + quads.length, value.predicate.value, i),
            );
        }

        return new RdfGraphData(subjectObject, triples);
    }

    /**
     * Not implemented
     * @param {BinaryData} fileContent
     * @param {RdfStore} existing
     * @returns {boolean}
     */
    static add(fileContent, existing) {
        const parser = new N3.Parser({ format: 'Turtle', blankNodePrefix: '' });

        const textDecoder = new TextDecoder();

        const quads = parser.parse(textDecoder.decode(fileContent));

        for (const value of quads) {
            existing.addTriple(
                new RawTriple(value.subject.value, value.predicate.value, value.object.value),
            );
        }
        return true;
    }

    /**
     *
     * @param {RdfGraphData} triples
     * @returns {BinaryData}
     */
    export(triples) {
        const { DataFactory } = N3;
        const { quad, namedNode, blankNode, literal } = DataFactory;
        const writer = new N3.Writer({ format: 'Turtle' });

        for (const value of triples.links) {
            // get Subject
            /**
             * @type {string}
             */
            let subject = '';

            /**
             * @type {string}
             */
            let object = '';
            for (const potentialSubject of triples.nodes) {
                if (potentialSubject.id === value.subject) {
                    subject = potentialSubject.name;
                }
            }

            // get Predicate
            const predicate = value.name;
            // get Object
            for (const potentialObject of triples.nodes) {
                if (potentialObject.id === value.object) {
                    object = potentialObject.name;
                }
            }

            if (validateIri(predicate, IriValidationStrategy.Pragmatic)) {
                console.warn(
                    `Predicate '${predicate}' is not a valid IRI. Exported data will likely not be importable!`,
                );
            }

            /**
             * @type {NamedNode | BlankNode}
             */
            let subjectNode = namedNode(subject);
            if (validateIri(subject, IriValidationStrategy.Pragmatic)) {
                // validateIri returns '$thing' if validation ***fails***
                // so this is the *non-iri* case
                subjectNode = blankNode(subject);
            }

            /**
             * @type {NamedNode | Literal}
             */
            let objectNode = namedNode(object);
            if (validateIri(object, IriValidationStrategy.Pragmatic)) {
                // validateIri returns '$thing' if validation ***fails***
                // so this is the *non-iri* case
                objectNode = literal(object);
            }

            writer.addQuad(quad(subjectNode, namedNode(predicate), objectNode));
        }

        let resultString = '';

        writer.end(
            (
                /**
                 * @type {Error}
                 */
                error,
                /**
                 * @type {string}
                 */
                result,
            ) => {
                if (result.length === 0) {
                    throw error;
                }
                resultString = result;
            },
        );

        const textEncoder = new TextEncoder();

        return textEncoder.encode(resultString);
    }
}

/** @type {Import} */
const _typeCheck = TurtleFormat;

export { TurtleFormat };
