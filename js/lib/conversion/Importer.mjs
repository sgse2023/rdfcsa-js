// @ts-check

import { RDFCSA } from '../rdfstore/RDFCSA.mjs';
import { RdfGraphData } from '../rdfstore/RdfGraphData.mjs';

/** @typedef {import('../rdfstore/RdfStore').RdfStore} RdfStore */
/** @typedef {import('./Import').Import} Import */

class Importer {
    /**
     * @type {BinaryData}
     */
    #fileContent;

    /**
     * @type {Import}
     */
    #format;

    /**
     * @param {BinaryData} fileContent
     * @param {Import} format
     */
    constructor(fileContent, format) {
        this.#fileContent = fileContent;
        this.#format = format;
    }

    /**
     * @returns {RdfStore}
     */
    import() {
        const importedData = this.#format.import(this.#fileContent);

        if (importedData instanceof RdfGraphData) {
            return new RDFCSA(importedData);
        }
        return importedData;
    }

    /**
     * @param {RdfStore} existing
     * @returns {boolean}
     */
    add(existing) {
        existing;
        throw new Error('TODO');
    }
}

export { Importer };
