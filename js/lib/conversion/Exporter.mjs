// @ts-check

/** @typedef {import('./Export').Export} Export */
/** @typedef {import('../rdfstore/RdfGraphData.mjs').RdfGraphData} RdfGraphData */

class Exporter {
    /**
     * @type {RdfGraphData}
     */
    #data;

    /**
     * @type {Export}
     */
    #format;
    /**
     * @param {RdfGraphData} data
     * @param {Export} format
     */
    constructor(data, format) {
        this.#data = data;
        this.#format = format;
    }

    /**
     * @returns {BinaryData}
     */
    export() {
        return this.#format.export(this.#data);
    }
}

export { Exporter };
