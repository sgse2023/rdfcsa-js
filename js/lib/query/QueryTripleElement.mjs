// @ts-check

class QueryTripleElement {
    /**
     * @readonly
     * @type {string | null}
     */
    value;
    /**
     * @readonly
     * @type {boolean}
     */
    isVariable;
    /**
     * @readonly
     * @type {boolean}
     */
    isBound;

    /**
     * @param {string | null} value
     * @param {boolean} isVariable
     */
    constructor(value, isVariable) {
        if (value === null && isVariable) {
            throw new Error('variable must have a value');
        }
        this.value = value;
        this.isVariable = isVariable;
        this.isBound = this.value !== null && !this.isVariable;
    }
}

export { QueryTripleElement };
