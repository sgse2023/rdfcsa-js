import type { TriplePattern } from './TriplePattern.mjs';
import type { QueryTripleElement } from './QueryTripleElement.mjs';

export interface Query {
    getTriplePatterns(): readonly TriplePattern[];
    getVariables(): readonly QueryTripleElement[];
}
