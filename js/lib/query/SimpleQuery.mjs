// @ts-check

import { QueryTripleElement } from './QueryTripleElement.mjs';
import { TriplePattern } from './TriplePattern.mjs';

/** @typedef {import('./Query').Query} Query */

/**
 * @implements {Query}
 */
class SimpleQuery {
    /**
     * @type {readonly TriplePattern[]}
     */
    #patterns;

    /**
     * @param {TriplePattern[]} patterns
     */
    constructor(patterns) {
        this.#patterns = patterns;
    }

    /**
     * @returns {readonly TriplePattern[]}
     */
    getTriplePatterns() {
        return this.#patterns;
    }

    /**
     * @returns {readonly QueryTripleElement[]}
     */
    getVariables() {
        const varsFromPattern = /** @param {TriplePattern} pattern */ (pattern) =>
            [pattern.subject, pattern.predicate, pattern.object].filter((elem) => elem.isVariable);
        const varsToValueElemPairs = /**
            @param {QueryTripleElement} variable
            @returns {[string | null, QueryTripleElement]}
            */ (variable) => [variable.value, variable];

        const duplicateValueVarPairs = this.#patterns.flatMap((elem) =>
            varsFromPattern(elem).map(varsToValueElemPairs),
        );

        // Map usage: filter out duplicate variables
        // (i.e. the same variable occurs in multiple patterns,
        // but the return value should contain it only once)
        return Array.from(new Map(duplicateValueVarPairs).values());
    }
}

const QUERY_ALL = new SimpleQuery([
    new TriplePattern(
        new QueryTripleElement(null, false),
        new QueryTripleElement(null, false),
        new QueryTripleElement(null, false),
    ),
]);

export { SimpleQuery, QUERY_ALL };
