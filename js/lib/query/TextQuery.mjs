// @ts-check

import P from 'parsimmon';

import { QueryTripleElement } from './QueryTripleElement.mjs';
import { TriplePattern } from './TriplePattern.mjs';
import { SimpleQuery } from './SimpleQuery.mjs';

/**
 * The @ts-ignore comments are needed in here,
 * because the typings for parsimmon are broken.
 * (i.e., they cause errors when running `npm run check`,
 * but not in the ide; that's why they are @ts-ignore and not @ts-expect-error)
 */

/** @typedef {import('./Query').Query} Query */

function parseUnbound() {
    // @ts-ignore
    return P.string('?').result(new QueryTripleElement(null, false));
}

function parseBound() {
    return P.regexp(
        /[a-zA-Z0-9!*';:@&=+$/?#^%\-<>.{}|\u0080-\uFFFF ]+/u,
        // @ts-ignore
    ).map(/** @param {string} text */ (text) => new QueryTripleElement(text.trim(), false));
}

function parseVariable() {
    return (
        P.regexp(/[a-zA-Z]+/)
            // @ts-ignore
            .wrap(P.string('['), P.string(']'))
            .map(/** @param {string} name */ (name) => new QueryTripleElement(name, true))
    );
}

function parseTripleElement() {
    return P.alt(parseUnbound(), parseBound(), parseVariable());
}

function parseTriplePattern() {
    return P.seqMap(
        // @ts-ignore
        parseTripleElement().skip(P.regexp(/, ?/)),
        // @ts-ignore
        parseTripleElement().skip(P.regexp(/, ?/)),
        parseTripleElement(),
        /**
         * @param {QueryTripleElement} subject
         * @param {QueryTripleElement} predicate
         * @param {QueryTripleElement} object
         */
        (subject, predicate, object) => new TriplePattern(subject, predicate, object),
    ).wrap(P.string('('), P.string(')'));
}

/**
 * @param {string} input
 * @returns {TriplePattern[]}
 */
function parseQuery(input) {
    const parser = P.sepBy1(parseTriplePattern(), P.regexp(/ [Xx] /));
    return parser.tryParse(input);
}

/**
 * @param {string} input
 * @returns {QueryTripleElement}
 */
function parseTripleElementPublic(input) {
    // I'm usually not a fan of exception based error handling,
    // but it's so much easier right now
    // @ts-ignore
    return parseTripleElement().tryParse(input);
}

/**
 * @implements {Query}
 */
class TextQuery extends SimpleQuery {
    /**
     * @param {string} query
     */
    constructor(query) {
        super(parseQuery(query));
    }
}

export { TextQuery, parseTripleElementPublic as parseTripleElement };
