// @ts-check

/** @typedef {import('./QueryTripleElement.mjs').QueryTripleElement} QueryTripleElement */

class TriplePattern {
    /**
     * @readonly
     * @type {QueryTripleElement}
     */
    subject;
    /**
     * @readonly
     * @type {QueryTripleElement}
     */
    predicate;
    /**
     * @readonly
     * @type {QueryTripleElement}
     */
    object;

    /**
     * @param {QueryTripleElement} subject
     * @param {QueryTripleElement} predicate
     * @param {QueryTripleElement} object
     */
    constructor(subject, predicate, object) {
        this.subject = subject;
        this.predicate = predicate;
        this.object = object;
    }
}

export { TriplePattern };
