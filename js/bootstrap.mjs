// @ts-check

console.log('RDFCSA-JS is loading now...');

// allow to load css files for the whole page,
// regardless of their inclusion in other js files.
// I.e., by putting them here it is ensured they're always bundled.
// If they are supposed to be used
// (see ../build/config.mjs:scriptLoading.globalCssFileNames)
// but are not bundled, a warning message will be printed
// during `npm run build`.
import 'holiday.css';
import './custom.css';
import 'dialog-polyfill/dialog-polyfill.css';

// this file must import *all* modules
// that register custom web components!
// (otherwise the browser won't know about their existance)
// they follow below:
import './view/app.mjs';
