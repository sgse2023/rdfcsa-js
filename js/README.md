# RDFCSA-JS Implementation

The implementation consists of roughly 3 parts,
split across aptly named directories:

-   [component](component/): individual Web Components that are composed to form individual pages
-   [view](view/): the Web Components that display the individual pages (.e.g an "About" page, the 2D visualization, etc)
-   [lib](lib/): visualization/UI independent implementation, e.g. data structures
