// @ts-check

/**
 * Assertions that only work in a browser environment
 * or import code that only works there.
 */

import { DialogPolyfilled } from './dialog/polyfill.mjs';

/**
 * @template {Element} T
 * @param {T} e
 * @returns {HTMLElement}
 */
function htmlElement(e) {
    if (!(e instanceof HTMLElement)) {
        throw new Error(`Element is not an HTMLElement but a ${typeof e}!`);
    }
    return e;
}

let firstWarning = 0;

/**
 * @template {Element} T
 * @param {T} e
 * @returns {HTMLDialogElement}
 */
function htmlDialogElement(e) {
    if (typeof HTMLDialogElement !== 'function') {
        if (firstWarning === 0) {
            firstWarning++;
            console.warn('no native HTMLDialogElement support; skipping assertion');
        }
        const d = /** @type {unknown} */ (e);
        return /** @type {HTMLDialogElement} */ (d);
    }
    if (!(e instanceof HTMLDialogElement)) {
        throw new Error(`Element is not an HTMLDialogElement but a ${typeof e}!`);
    }
    return e;
}

/**
 * @template {Element} T
 * @param {T} e
 * @returns {DialogPolyfilled}
 */
function dialogPolyfilledElement(e) {
    if (!(e instanceof DialogPolyfilled)) {
        throw new Error(`Element is not an DialogPolyfilled but a ${typeof e}!`);
    }
    return e;
}

/**
 * @template {Element} T
 * @param {T} e
 * @returns {HTMLInputElement}
 */
function htmlInputElement(e) {
    if (!(e instanceof HTMLInputElement)) {
        throw new Error(`Element is not an HTMLInputElement but a ${typeof e}!`);
    }
    return e;
}

/**
 * @template {Element} T
 * @param {T} e
 * @returns {HTMLTextAreaElement}
 */
function htmlTextAreaElement(e) {
    if (!(e instanceof HTMLTextAreaElement)) {
        throw new Error(`Element is not an HTMLTextAreaElement but a ${typeof e}!`);
    }
    return e;
}

/**
 * @template {Element} T
 * @param {T} e
 * @returns {HTMLAnchorElement}
 */
function htmlAnchorElement(e) {
    if (!(e instanceof HTMLAnchorElement)) {
        throw new Error(`Element is not an HTMLAnchorElement but a ${typeof e}!`);
    }
    return e;
}

export {
    htmlElement,
    htmlDialogElement,
    dialogPolyfilledElement,
    htmlInputElement,
    htmlAnchorElement,
    htmlTextAreaElement,
};
