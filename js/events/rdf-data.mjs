// @ts-check

/** @typedef {import('../lib/rdfstore/RdfStore').RdfStore} RdfStore */
/** @typedef {import('../lib/rdfstore/RdfGraphData.mjs').RdfGraphData} RdfGraphData */

/**
 * @param {RdfStore} data
 */
export class RdfData extends Event {
    static type = 'rdf-data';

    /**
     * @param {RdfStore} data
     * @param {boolean} update true if this event signals an update to the existing store
     */
    constructor(data, update = false) {
        super(RdfData.type, {
            bubbles: true,
            composed: true,
        });
        this.data = {
            store: data,
            isUpdate: update,
        };
    }
}
