// @ts-check

/** @typedef {import('../lib/settings/Settings').Settings} Settings */

export class RdfSettings extends Event {
    static type = 'rdf-settings';

    /**
     * @param {Settings} settings
     * @param {boolean} update true if this event signals an update to the existing settings
     * instance
     */
    constructor(settings, update) {
        super(RdfSettings.type, {
            bubbles: true,
            composed: true,
        });
        this.data = {
            settings: settings,
            isUpdate: update,
        };
    }
}
