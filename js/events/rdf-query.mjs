// @ts-check

/** @typedef {import('../lib/query/Query').Query} Query */

/**
 * @param {Query} query
 */
export class RdfQuery extends Event {
    static type = 'rdf-query';

    /**
     * @param {Query} query
     */
    constructor(query) {
        super(RdfQuery.type, {
            bubbles: true,
            composed: true,
        });
        this.query = query;
    }
}
