// @ts-check

import { html, css, LitElement } from 'lit';
import holidayCss from 'holiday.css';

import './visu/3d.mjs';
import './visu/2d.mjs';
import './visu/text.mjs';

export class Visu extends LitElement {
    /**
     * @override
     */
    static styles = [holidayCss, css``];

    /**
     * @override
     */
    static properties = {
        parentRoute: {
            type: Object,
            attribute: false,
        },
    };

    constructor() {
        super();
        this.parentRoute = {};
    }

    /**
     * @override
     */
    render() {
        return html`
            <domx-route-not-found
                element="rdfcsa-not-found"
                append-to="parent"
            ></domx-route-not-found>
            <domx-route
                .parentRoute="${this.parentRoute}"
                pattern="/3d"
                element="visu-3d"
                append-to="parent"
            ></domx-route>
            <domx-route
                .parentRoute="${this.parentRoute}"
                pattern="/2d"
                element="visu-2d"
                append-to="parent"
            ></domx-route>
            <domx-route
                .parentRoute="${this.parentRoute}"
                pattern="/text"
                element="visu-text"
                append-to="parent"
            ></domx-route>
        `;
    }
}

customElements.define('rdfcsa-visu', Visu);
