// @ts-check

import { html, css, LitElement } from 'lit';
import { createRef, ref } from 'lit/directives/ref.js';
import { ContextConsumer } from '@lit-labs/context';
import holidayCss from 'holiday.css';

import { RdfSettings as SettingsEvent } from '../events/rdf-settings.mjs';
import { settings as settingsContext, rdfStore as storeContext } from '../context.mjs';
import { htmlInputElement } from '../assertions-frontend.mjs';
import { nonNull } from '../lib/assertions.mjs';

export class Settings extends LitElement {
    /**
     * @override
     */
    static styles = [holidayCss, css``];

    inputMaxTriple = createRef();
    checkboxRenderModeText = createRef();
    checkboxRenderModeIcon = createRef();

    #settings = new ContextConsumer(this, {
        context: settingsContext,
    });

    #rdfStore = new ContextConsumer(this, {
        context: storeContext,
    });

    constructor() {
        super();
    }

    /**
     * @override
     */
    render() {
        const settings = this.#settings.value;
        const rdfStore = this.#rdfStore.value;
        const tripleCount = rdfStore?.tripleCount() ?? 1000;
        return html`
            <article>
                <h1>Einstellungen</h1>
                <label
                    >Max. Anzahl angezeigter Triple
                    <input
                        ${ref(this.inputMaxTriple)}
                        .valueAsNumber="${settings?.queryTripleLimit ?? tripleCount}"
                        type="range"
                        min="0"
                        max="${tripleCount * 2}"
                        step="${Math.max(1, Math.floor(tripleCount / 10))}"
                    />
                </label>
                <fieldset>
                    <legend>Rendering von Knoten</legend>
                    <label
                        >Text
                        <input
                            ${ref(this.checkboxRenderModeText)}
                            .checked="${settings?.nodeRenderMode === 'text'}"
                            type="radio"
                            name="renderMode"
                        />
                    </label>
                    <label
                        >Icon
                        <input
                            ${ref(this.checkboxRenderModeIcon)}
                            .checked="${settings?.nodeRenderMode === 'icon'}"
                            type="radio"
                            name="renderMode"
                        />
                    </label>
                </fieldset>
                <button @click="${this.#applySettings}">Speichern</button>
            </article>
        `;
    }

    #applySettings() {
        const inputTripleLimit = htmlInputElement(nonNull(this.inputMaxTriple.value)).valueAsNumber;
        const checkboxRenderModeText = htmlInputElement(
            nonNull(this.checkboxRenderModeText.value),
        ).checked;
        const checkboxRenderModeIcon = htmlInputElement(
            nonNull(this.checkboxRenderModeIcon.value),
        ).checked;

        const settings = this.#settings.value;
        if (settings) {
            settings.queryTripleLimit = inputTripleLimit;
            if (checkboxRenderModeText) {
                settings.nodeRenderMode = 'text';
            } else if (checkboxRenderModeIcon) {
                settings.nodeRenderMode = 'icon';
            } else {
                console.error('BUG: neither render mode text nor render mode icon is selected');
            }
            this.dispatchEvent(new SettingsEvent(settings, true));
        }
    }
}

customElements.define('rdfcsa-settings', Settings);
