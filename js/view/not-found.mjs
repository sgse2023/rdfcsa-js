// @ts-check

import { html, css, LitElement } from 'lit';
import holidayCss from 'holiday.css';

export class NotFound extends LitElement {
    /**
     * @override
     */
    static styles = [holidayCss, css``];

    /**
     * @override
     */
    static properties = {
        pathname: {},
    };

    constructor() {
        super();
        this.pathname = '';
    }

    // FIXME href zum Start muss page root sein, statt "/"
    /**
     * @override
     */
    render() {
        return html`
            <article>
                <h1>Nicht Gefunden</h1>
                <p>
                    Leider existiert die Seite ${this.pathname} nicht. Zurück
                    <a href="/">zum Start</a>
                </p>
            </article>
        `;
    }
}

customElements.define('rdfcsa-not-found', NotFound);
