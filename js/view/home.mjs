// @ts-check

import { html, css, LitElement } from 'lit';
import holidayCss from 'holiday.css';

export class Home extends LitElement {
    /**
     * @override
     */
    static styles = [holidayCss, css``];

    constructor() {
        super();
    }

    /**
     * @override
     */
    render() {
        return html`
            <h1>Willkommen!</h1>
            <article>
                Die Seite RDFCSA-JS bietet eine Implementierung der RDFCSA Datenstruktur, welche
                komplett im Browser ausgeführt wird. Es können Daten importiert, exportiert und
                sogar editiert werden. Jedoch wird mit steigender Anzahl an Änderungen die
                Datenstruktur langsamer (ein Export und erneuter Import sorgt für gewohnte
                Geschwindigkeit nach dem Editieren). Die Daten können als 2D- und 3D-Graph
                dargestellt werden. Zudem kann in einer Textansicht editiert werden.
            </article>
        `;
    }
}

customElements.define('rdfcsa-home', Home);
