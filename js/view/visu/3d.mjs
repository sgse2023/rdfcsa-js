// @ts-check

import { html, css, LitElement } from 'lit';
import { createRef, ref } from 'lit/directives/ref.js';
import { ContextConsumer } from '@lit-labs/context';
import holidayCss from 'holiday.css';
import ForceGraph3D from '3d-force-graph';
/**
 * @template {import('@lit-labs/context').Context<unknown, unknown>} T
 * @typedef {import('@lit-labs/context').ContextType<T>} ContextType<T>
 */
/** @typedef {import('three-forcegraph').NodeObject} NodeObject */

import { nonNull } from '../../lib/assertions.mjs';
import { htmlElement } from '../../assertions-frontend.mjs';
import { rdfGraphData as graphDataContext, settings as settingsContext } from '../../context.mjs';

/**
 * @typedef {import('../../lib/settings/Settings.js').Settings} Settings
 */

/**
 * @typedef {object} GraphNode
 * @prop {number} id
 * @prop {string} name
 * @prop {string} color
 */

import '../../component/rdf-query.mjs';
import '../../component/button-import.mjs';
import '../../component/button-export.mjs';
import SpriteText from 'three-spritetext';

export class Visu3D extends LitElement {
    /**
     * @override
     */
    static styles = [
        holidayCss,
        css`
            article {
                display: flex;
                justify-content: center;
                flex-direction: column;
                align-items: center;
            }
            article div {
                max-width: 80vw;
                max-height: 50vh;
            }
            .scene-tooltip {
                /*
                The tooltip arbitrarily changes width/height, causing, e.g., a scrollbar to randomly
                appear and disappear while moving across the graph with the mouse.
                We do not need the tooltip, but force-graph does not have an API to hide it.
                Therefor forcefully remove it from the users' sight.
                 */
                display: none;
            }
        `,
    ];

    graphRef = createRef();

    #graphData = new ContextConsumer(this, {
        context: graphDataContext,
        subscribe: true,
        // () =>: make sure this inside #updateGraphData is the class instance
        callback: (v) => this.#updateGraphData(v),
    });
    #settings = new ContextConsumer(this, {
        context: settingsContext,
        subscribe: true,
        // note: this could be handled without a callback in updated(),
        // problem: this is not a property in lit's sense,
        // and as such there is no notification it has changed
        // -> it would need to be set on each update to this component
        // () =>: make sure this inside #updateGraphData is the class instance
        callback: (v) => this.#updateGraphSettings(v),
    });

    /**
     * @override
     */
    static properties = {
        graph3d: { type: ForceGraph3D, state: true },
    };

    constructor() {
        super();
        this.graph3d = ForceGraph3D();
    }

    /**
     * @param {ContextType<graphDataContext>} newGraphData
     */
    #updateGraphData(newGraphData) {
        this.graph3d?.graphData(newGraphData.toPojo());
    }

    /**
     * @param {Settings?} newSettings
     */
    #updateGraphSettings(newSettings) {
        if (this.graph3d && newSettings) {
            if (newSettings.nodeRenderMode === 'icon') {
                // @ts-expect-error Reset property, type signature is missing this possiblity.
                this.graph3d.nodeThreeObject(null);
            } else {
                this.graph3d.nodeThreeObject(
                    /**
                    @param {NodeObject} _node
                */
                    (_node) => {
                        const node = /** @type {GraphNode & NodeObject} */ (_node);

                        const sprite = new SpriteText(node.name);
                        sprite.material.depthWrite = false; // make sprite background transparent
                        sprite.color = node.color;
                        sprite.textHeight = 8;
                        return sprite;
                    },
                );
            }
        }
    }

    /**
     * @override
     */
    firstUpdated() {
        const graphDiv = htmlElement(nonNull(this.graphRef.value));
        this.graph3d = ForceGraph3D()(graphDiv);
        this.graph3d.backgroundColor('#FFFFFF');
        this.graph3d.linkAutoColorBy(function () {
            return '#000000';
        });
        this.graph3d.nodeAutoColorBy(function () {
            return '#0000FF';
        });
        this.graph3d.width(graphDiv.offsetWidth);
        this.graph3d.height(graphDiv.offsetHeight);
        if (this.#graphData.value) {
            this.graph3d.graphData(this.#graphData.value.toPojo());
        }

        this.#updateGraphSettings(this.#settings.value ?? null);
    }

    /**
     * @override
     */
    render() {
        return html`
            <article>
                <h1>3D Visu</h1>
                <btn-import></btn-import>
                <btn-export></btn-export>
                <rdf-query></rdf-query>
                <div ${ref(this.graphRef)}></div>
            </article>
        `;
    }
}

customElements.define('visu-3d', Visu3D);
