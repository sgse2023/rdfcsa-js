// @ts-check

import { html, css, LitElement } from 'lit';
import { virtualize } from '@lit-labs/virtualizer/virtualize.js';
import { ContextConsumer } from '@lit-labs/context';
import holidayCss from 'holiday.css';
/**
 * @template {import('@lit-labs/context').Context<unknown, unknown>} T
 * @typedef {import('@lit-labs/context').ContextType<T>} ContextType<T>
 */

import { rdfGraphData as graphDataContext } from '../../context.mjs';

import '../../component/rdf-query.mjs';
import '../../component/button-import.mjs';
import '../../component/button-export.mjs';
import '../../component/triple-editor.mjs';
import '../../component/triple-inserter.mjs';

export class VisuText extends LitElement {
    /**
     * @override
     */
    static styles = [
        holidayCss,
        css`
            article {
                display: flex;
                justify-content: center;
                flex-direction: column;
                align-items: center;
            }
            triple-editor {
                width: 100%;
            }
        `,
    ];

    #graphData = new ContextConsumer(this, {
        context: graphDataContext,
        subscribe: true,
    });

    constructor() {
        super();
    }

    /**
     * @override
     */
    render() {
        const graphData = this.#graphData.value;
        const items = graphData
            ? html`
                  ${virtualize({
                      items: /** @type {import('../../lib/rdfstore/Triple.mjs').Triple[]} */ (
                          graphData.links
                      ),
                      renderItem:
                          /** @param {import('../../lib/rdfstore/Triple.mjs').Triple} predicate */ (
                              predicate,
                          ) => {
                              let subject = graphData.nodeAtId(predicate.subject);
                              let object = graphData.nodeAtId(predicate.object);
                              if (typeof subject === 'undefined') {
                                  console.error(
                                      "subject id of predicate '" + predicate + "' is invalid",
                                  );
                                  return html``;
                              }
                              if (typeof object === 'undefined') {
                                  console.error(
                                      "object id of predicate '" + predicate + "' is invalid",
                                  );
                                  return html``;
                              }
                              return html`<triple-editor
                                  subject=${subject.name}
                                  predicate=${predicate.name}
                                  object=${object.name}
                              ></triple-editor>`;
                          },
                  })}
              `
            : html``;

        return html`
            <article>
                <h1>Text Visu</h1>
                <btn-import></btn-import>
                <btn-export></btn-export>
                <rdf-query></rdf-query>
                <triple-inserter></triple-inserter>
                <section>${items}</section>
            </article>
        `;
    }
}

customElements.define('visu-text', VisuText);
