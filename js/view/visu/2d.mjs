// @ts-check

import { html, css, LitElement } from 'lit';
import { createRef, ref } from 'lit/directives/ref.js';
import { ContextConsumer } from '@lit-labs/context';
import holidayCss from 'holiday.css';
import ForceGraph from 'force-graph';
/**
 * @template {import('@lit-labs/context').Context<unknown, unknown>} T
 * @typedef {import('@lit-labs/context').ContextType<T>} ContextType<T>
 */

import { nonNull } from '../../lib/assertions.mjs';
import { htmlElement } from '../../assertions-frontend.mjs';
import { rdfGraphData as graphDataContext, settings as settingsContext } from '../../context.mjs';

/**
 * @typedef {import('../../lib/settings/Settings.js').Settings} Settings
 */

/**
 * @typedef {object} GraphNode
 * @prop {number} id
 * @prop {string} name
 * @prop {[number, number]} __bckgDimensions
 * @prop {number} x
 * @prop {number} y
 * @prop {string} color
 */

import '../../component/rdf-query.mjs';
import '../../component/button-import.mjs';
import '../../component/button-export.mjs';

export class Visu2D extends LitElement {
    /**
     * @override
     */
    static styles = [
        holidayCss,
        css`
            article {
                display: flex;
                justify-content: center;
                flex-direction: column;
                align-items: center;
            }
            article div {
                max-width: 80vw;
                max-height: 50vh;
            }
        `,
    ];

    graphRef = createRef();

    #graphData = new ContextConsumer(this, {
        context: graphDataContext,
        subscribe: true,
        // note: this could be handled without a callback in updated(),
        // problem: this is not a property in lit's sense,
        // and as such there is no notification it has changed
        // -> it would need to be set on each update to this component
        // () =>: make sure this inside #updateGraphData is the class instance
        callback: (v) => this.#updateGraphData(v),
    });
    #settings = new ContextConsumer(this, {
        context: settingsContext,
        subscribe: true,
        // note: this could be handled without a callback in updated(),
        // problem: this is not a property in lit's sense,
        // and as such there is no notification it has changed
        // -> it would need to be set on each update to this component
        // () =>: make sure this inside #updateGraphData is the class instance
        callback: (v) => this.#updateGraphSettings(v),
    });

    /**
     * @override
     */
    static properties = {
        graph2d: { type: ForceGraph, state: true },
    };

    constructor() {
        super();
        /**
         * @type {any|null}
         */
        this.graph2d = null;
    }

    /**
     * @param {ContextType<graphDataContext>} newGraphData
     */
    #updateGraphData(newGraphData) {
        this.graph2d?.graphData(newGraphData.toPojo());
    }

    /**
     * @param {Settings?} newSettings
     */
    #updateGraphSettings(newSettings) {
        if (this.graph2d && newSettings) {
            if (newSettings.nodeRenderMode === 'icon') {
                this.graph2d.nodeAutoColorBy(null);
                this.graph2d.nodeCanvasObject(null);
                this.graph2d.nodePointerAreaPaint(null);
            } else {
                this.graph2d.nodeAutoColorBy('group');
                this.graph2d.nodeCanvasObject(
                    /**
                     * @param {GraphNode} node
                     * @param {CanvasRenderingContext2D} ctx
                     * @param {number} globalScale
                     */
                    (node, ctx, globalScale) => {
                        const label = node.name;
                        const fontSize = 12 / globalScale;
                        ctx.font = `${fontSize}px Sans-Serif`;
                        const textWidth = ctx.measureText(label).width;
                        const bckgDimensions = /** @type {[number, number]} */ (
                            [textWidth, fontSize].map((n) => n + (fontSize * 0.2))
                        ); // some padding

                        ctx.fillStyle = 'rgba(255, 255, 255, 0.8)';
                        ctx.fillRect(
                            node.x - (bckgDimensions[0] / 2),
                            node.y - (bckgDimensions[1] / 2),
                            ...bckgDimensions,
                        );

                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';
                        ctx.fillStyle = node.color;
                        ctx.fillText(label, node.x, node.y);

                        node.__bckgDimensions = bckgDimensions; // to re-use in nodePointerAreaPaint
                    },
                );
                this.graph2d.nodePointerAreaPaint(
                    /**
                     * @param {GraphNode} node
                     * @param {string} color
                     * @param {CanvasRenderingContext2D} ctx
                     */
                    (node, color, ctx) => {
                        ctx.fillStyle = color;
                        const bckgDimensions = node.__bckgDimensions;
                        bckgDimensions &&
                            ctx.fillRect(
                                node.x - (bckgDimensions[0] / 2),
                                node.y - (bckgDimensions[1] / 2),
                                ...bckgDimensions,
                            );
                    },
                );
            }
        }
    }

    /**
     * @override
     */
    firstUpdated() {
        const graphDiv = htmlElement(nonNull(this.graphRef.value));
        this.graph2d = ForceGraph()(graphDiv);
        this.graph2d.height(graphDiv.offsetHeight);
        this.graph2d.width(graphDiv.offsetWidth);
        this.graph2d.graphData(this.#graphData.value?.toPojo());

        this.#updateGraphSettings(this.#settings.value ?? null);
    }

    /**
     * @override
     */
    render() {
        return html`
            <article>
                <h1>2D Visu</h1>
                <rdf-query></rdf-query>
                <btn-import></btn-import>
                <btn-export></btn-export>
                <div ${ref(this.graphRef)}></div>
            </article>
        `;
    }
}

customElements.define('visu-2d', Visu2D);
