// @ts-check

import { html, css, LitElement } from 'lit';
import { ContextProvider } from '@lit-labs/context';
import holidayCss from 'holiday.css';
import '@domx/router/domx-route-not-found';
import '@domx/router/domx-route';
import { Router } from '@domx/router';

import { RdfQuery as QueryEvent } from '../events/rdf-query.mjs';
import { RdfData as RdfDataEvent } from '../events/rdf-data.mjs';
import { RdfSettings as SettingsEvent } from '../events/rdf-settings.mjs';
import { RdfGraphData } from '../lib/rdfstore/RdfGraphData.mjs';
import { RDFCSA } from '../lib/rdfstore/RDFCSA.mjs';
import {
    rdfStore as rdfStoreContext,
    rdfGraphData as graphDataContext,
    settings as settingsContext,
} from '../context.mjs';
import { SubjectObject } from '../lib/rdfstore/SubjectObject.mjs';
import { nonNull } from '../lib/assertions.mjs';
import { Triple } from '../lib/rdfstore/Triple.mjs';
import { RawTriple } from '../lib/rdfstore/RawTriple.mjs';
import { InMemSettings } from '../lib/settings/InMemSettings.mjs';
import { QUERY_ALL } from '../lib/query/SimpleQuery.mjs';
import { loadMockData, clearRdfStore } from '../component/StoreMockData.mjs';

/** @typedef {import('../lib/rdfstore/RdfStore').RdfStore} RdfStore */
/** @typedef {import('../lib/query/Query').Query} Query */

import './home.mjs';
import './visu.mjs';
import './settings.mjs';
import './imprint.mjs';
import './not-found.mjs';
import '../dialog/import.mjs';
import '../dialog/export.mjs';

export class App extends LitElement {
    /**
     * @override
     */
    static styles = [
        holidayCss,
        css`
            header {
                display: flex;
                align-items: center;
                justify-content: center;
                column-gap: 10px;
            }
            nav {
                display: flex;
                align-items: center;
                justify-content: center;
            }
            header h1 {
                flex-shrink: 0;
            }
            main {
                flex-grow: 1;
            }
            footer {
                margin-top: 1rem;
                display: flex;
                align-items: center;
                justify-content: space-between;
            }
        `,
    ];

    /**
     * @override
     */
    static properties = {
        _routePath: { type: String, state: true },
    };

    #rdfStoreProvider = new ContextProvider(this, {
        context: rdfStoreContext,
    });
    #graphDataProvider = new ContextProvider(this, {
        context: graphDataContext,
        initialValue: new RdfGraphData([], []),
    });
    #settingsProvider = new ContextProvider(this, {
        context: settingsContext,
        initialValue: new InMemSettings(),
    });

    constructor() {
        super();
        this._routePath = window.location.pathname;
        Router.root = PAGE_ROOT;

        this.addEventListener(QueryEvent.type, this.#handleQuery);
        this.addEventListener(RdfDataEvent.type, this.#handleRdfData);
        this.addEventListener(SettingsEvent.type, this.#handleSettings);

        // Update the graph data when a new store is loaded
        // () =>: make sure this inside #updateGraphData points to this class instance
        this.#rdfStoreProvider.addCallback((v) => this.#updateGraphData(v), true);

        // @ts-expect-error yes typescript, global.loadMockData has type 'any'.
        // It's not like I'm defining it right here.
        window.loadMockData = loadMockData(this, this.#rdfStoreProvider);

        // @ts-expect-error
        window.clearRdfStore = clearRdfStore(this);

        // @ts-expect-error
        window.loadTripleArray = (data, appending = false) => {
            if (!appending) {
                const subjectObject = [];
                const triples = [];
                for (let i = 0; i < data.length; i = i + 3) {
                    subjectObject.push(new SubjectObject(i, nonNull(data[i])));
                    subjectObject.push(new SubjectObject(i + 2, nonNull(data[i + 2])));
                    triples.push(new Triple(i, i + 2, i + 1, nonNull(data[i + 1]), i));
                }

                const rdfGraphData = new RdfGraphData(subjectObject, triples);
                this.#rdfStoreProvider.setValue(new RDFCSA(rdfGraphData));
            } else {
                for (let i = 0; i < data.length; i = i + 3) {
                    this.#rdfStoreProvider.value.addTriple(
                        new RawTriple(nonNull(data[i]), nonNull(data[i + 1]), nonNull(data[i + 2])),
                    );
                }
            }
        };
    }

    /**
     * @param {RdfStore} newRdfStore
     */
    #updateGraphData(newRdfStore) {
        if (newRdfStore) {
            const tripleLimit = this.#settingsProvider.value.queryTripleLimit;
            this.#graphDataProvider.setValue(newRdfStore.execute(QUERY_ALL, tripleLimit));
        }
    }

    /**
     * @param {string} path
     * @param {any} content
     */
    #subPage(path, content) {
        return html`
            <li>
                <a
                    href="${PAGE_ROOT}${path}"
                    aria-current="${path === this._routePath ? 'page' : 'false'}"
                >
                    ${content}
                </a>
            </li>
        `;
    }

    /**
     * @override
     */
    render() {
        return html`
            <!--
            domx-route-not-found queries the topmost children of the component
            for routes. Therefore the routes *must not* be nested into,
            e.g. the lit workaround div.
            -->
            <domx-route-not-found
                element="rdfcsa-not-found"
                append-to="#site-content"
            ></domx-route-not-found>
            <domx-route pattern="/" element="rdfcsa-home" append-to="#site-content"></domx-route>
            <domx-route
                pattern="/visu/*routeTail"
                element="rdfcsa-visu"
                append-to="#site-content"
            ></domx-route>
            <domx-route
                pattern="/settings"
                element="rdfcsa-settings"
                append-to="#site-content"
            ></domx-route>
            <domx-route
                pattern="/imprint"
                element="rdfcsa-imprint"
                append-to="#site-content"
            ></domx-route>
            <!-- bug in holiday.css and web components: burger svg is displayed as if the site is
            in dark mode while light mode is forced site wide resulting in a white svg in front of
            a white background. workaround to use light colorschema for navbar hambuger via div -->
            <div id="content-wrapper" class="holiday-css-light" style="display: contents;">
                <header>
                    <h1>RDFCSA-JS</h1>
                    <p>
                        Speicher/Zeit-effizienter RDF-Store basierend auf
                        <i>circular suffix sorting</i>
                    </p>
                </header>
                <nav>
                    <ul>
                        ${this.#subPage('/', 'Start')}
                        <li>
                            <span>Visualisierung</span>
                            <ul>
                                ${this.#subPage('/visu/3d', '3D')}
                                ${this.#subPage('/visu/2d', '2D')}
                                ${this.#subPage('/visu/text', 'Text')}
                            </ul>
                        </li>
                        ${this.#subPage('/settings', 'Einstellungen')}
                        <li>
                            <a target="_blank" href="https://arxiv.org/abs/2009.10045">
                                Paper (externer Link)
                            </a>
                        </li>
                        ${this.#subPage('/imprint', 'Impressum')}
                    </ul>
                </nav>
                <main id="site-content"></main>
                <footer>
                    <section>
                        <p>
                            <a href="https://git.disroot.org/sgse2023/rdfcsa-js/" target="_blank"
                                >Quellcode</a
                            >
                        </p>
                    </section>
                    <section>
                        <p>
                            Umgesetzt von
                            <a href="mailto://malte.donzelmann@hsbi.de">Malte Donzelmann</a>,
                            <a href="mailto://jonas_patrick.voellmecke@hsbi.de"
                                >Jonas Patrick Völlmecke</a
                            >, <a href="mailto://christian.tirre@hsbi.de">Christian Tirre</a> und
                            <a href="mailto://florian_filip.warzecha@hsbi.de">Florian Warzecha</a>.
                        </p>
                    </section>
                </footer>
            </div>
            <dl-import></dl-import>
            <dl-export></dl-export>
        `;
    }

    /**
     * @param {Event} event
     */
    #handleLocationChange = (event) => {
        event;
        this._routePath = window.location.pathname.replace(PAGE_ROOT, '');
    };

    /**
     * @override
     */
    connectedCallback() {
        super.connectedCallback();
        window.addEventListener('location-changed', this.#handleLocationChange);
    }

    /**
     * @override
     */
    disconnectedCallback() {
        window.removeEventListener('location-changed', this.#handleLocationChange);
        super.disconnectedCallback();
    }

    /**
     * @param {Event} e
     */
    #handleQuery(e) {
        if (!(e instanceof QueryEvent)) {
            throw new TypeError(`Unexpected event type: '${typeof e}'`);
        }

        console.log('Received query event', e);
        const tripleLimit = this.#settingsProvider.value.queryTripleLimit;
        this.#graphDataProvider.setValue(
            this.#rdfStoreProvider.value.execute(e.query, tripleLimit),
        );
    }

    /**
     * @param {Event} e
     */
    #handleRdfData(e) {
        if (!(e instanceof RdfDataEvent)) {
            throw new TypeError(`Unexpected event type: '${typeof e}'`);
        }

        console.log('Received rdf data event', e);
        this.#rdfStoreProvider.setValue(e.data.store, e.data.isUpdate);
    }

    /**
     * @param {Event} e
     */
    #handleSettings(e) {
        if (!(e instanceof SettingsEvent)) {
            throw new TypeError(`Unexpected event type: '${typeof e}'`);
        }

        this.#settingsProvider.setValue(e.data.settings, e.data.isUpdate);
    }
}

customElements.define('rdfcsa-app', App);
