// @ts-check

import { html, css, LitElement } from 'lit';
import holidayCss from 'holiday.css';

export class Imprint extends LitElement {
    /**
     * @override
     */
    static styles = [holidayCss, css``];

    constructor() {
        super();
    }

    /**
     * @override
     */
    render() {
        return html`
            <h1>Impressum</h1>
            <article>
                <h2>Über</h2>
                <p>
                    Umsetzung der RDFCSA Datenstruktur, einem RDF-Store, aus
                    <a href="https://arxiv.org/abs/2009.10045" target="_blank"
                        >dem Paper Space/time-efficient RDF stores based on circular suffix
                        sorting</a
                    >; umgesetzt im Sommersemester 2023 an der
                    <a href="https://hsbi.de" target="_blank">Hochschule Bielefeld</a> im Modul
                    <i>Spezielle Gebiete zum Software Engineering</i>.
                </p>
            </article>
        `;
    }
}

customElements.define('rdfcsa-imprint', Imprint);
