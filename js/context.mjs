// @ts-check

import { createContext } from '@lit-labs/context';

/** @typedef {import('./lib/rdfstore/RdfStore').RdfStore} RdfStore */
/** @typedef {import('./lib/rdfstore/RdfGraphData.mjs').RdfGraphData} RdfGraphData */
/** @typedef {import('./lib/settings/Settings').Settings} Settings */
/**
 * @template T
 * @template U
 * @typedef {import('@lit-labs/context').Context<T, U>} Context<T, U>
 */

/**
 * @type {Context<Symbol, RdfStore>}
 */
export const rdfStore = createContext(Symbol('rdfstore'));

/**
 * @type {Context<Symbol, RdfGraphData>}
 */
export const rdfGraphData = createContext(Symbol('graphdata'));

/**
 * @type {Context<Symbol, Settings>}
 */
export const settings = createContext(Symbol('settings'));
