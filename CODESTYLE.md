# Codestyle

Although the project contains a formatter, linter & typechecking configuration,
some things cannot easily be solved that way.
For this reason, some additonal rules to be followed are documented
in this document.

## Ignorieren von Lints/Formatting/Checks

Note that, unless it's obvious why (see the next section for a case where it is;
although better assume it isn't),
you should also leave a comment _why_ ignoring the tool
is the right thing.

And, most importantly: if you have the impression,
that some lint, formatting rule or typeschecking option
is hindering your productivity: open an issue!

### eslint

See [the rule reference](https://eslint.org/docs/latest/rules/) for details about specific rules.

```
// eslint-disable-next-line no-alert
alert('foo');

// oder

alert('foo'); // eslint-disable-line no-alert
```

(vgl. [eslint docs](https://eslint.org/docs/latest/use/configure/rules#using-configuration-comments-1))

### prettier

Use `// prettier-ignore` to ignore the next node in the abstract syntax tree (see [prettier
docs](https://prettier.io/docs/en/ignore.html)).

### typescript

Use `// @ts-expect-error` to disable checking for the following line.
Use this only when you can add a good explanation for using it before
the annotation.
In case the typechecker is unhappy with you (e.g. index operations, see below),
better use an explicit check (see [js/lib/assertions.mjs](./js/lib/assertions.mjs) for examples),
to ensure your expectation is validated at runtime.

## Index-Operations and Type-Checking

Especially in 'classic' (`for (const i = 0; ...)`) `for`-Loops,
you will notice typerrors on (almost) every index access
(`arr[i]`).
Although annoying, the typechecker is totally correct here.
Usually, you will want to use the `nonNull` function from
[js/lib/assertions.mjs](./js/lib/assertions.mjs) on the result of the index operation.

Therefore:

1. If possible, use better suited `for`-loop variants (see below)
2. Otherwise: Use `nonNull`, to ensure your expectation of the index being valid
   is validated at runtime.

## `for`-Loops

Wherever possible, use `const ... of ` style `for`-loops.
Respective documentation and examples:

-   [Iteration without index](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of)
-   [Iteration with Index](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/entries)

## Order of Import-Statements

Import-statements are sorted in three blocks,
that follow the order shown hereafter:

1. Imports from the standard library (therefore not relevant for any frontend-only code)
2. Imports from (3rd-party) libraries
3. Imports from our own source code

Inside each blocks, the statements should be sorted alphabetically.
(And yes, I would assume that's the job of a linter/formatter, but I didn't write
them, so...)
