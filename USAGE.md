# Deploying RDFCSA-JS

The latest stable version is available at
https://sgse2023:sgse2023@rdfcsa-js.liketechnik.name<wbr>.
Additionally a docker container is provided in the [package
registry](https://git.disroot.org/sgse2023/rdfcsa-js/packages).
Of course, you always have the option to build the static page yourself,
and serve it with whatever webserver you prefer (e. g. nginx, apache, etc).

## Docker

The docker image uses nginx to serve the static files RDFCSA-JS is comprised
of as an SPA (Single Page Application).
It runs on port 8000.

## Building

There are two options to build RDFCSA-JS:

1. Building with a manual install [npm](#user-content-npm) and required dependencies.
2. Building with [nix](https://nixos.org).

No matter which variant you choose,
after you successfully built the static files required to host
RDFCSA-JS, you need to configure a webserver to serve the files
as an SPA (Single Page Application).
For nginx, take a look at [the configuration used in the docker
container](nix/packages/docker.nix).
We can't provide any further instructions for other server at this time,
sorry.

### npm

After you have installed `npm`, follow these steps:

1. Install the necessary dependencies: `npm install`
2. Build the dependencies into assets for the website: `npm run build`
3. Remove any files except for `dist/`, `js/` and `index.html`. [^1]

[^1]:
    In case the website doesn't work correctly, this instruction probably got
    outdated. Please cross check with the files copied by
    [the nix build](nix/packages/rdfcsa-js.nix).

### Nix

If you have flakes enabled on your system, simply run `nix build`.
The files to put into the root of your webserver are contained
in `result`.
If you don't have flakes, `nix-build` accomplishes the same.
