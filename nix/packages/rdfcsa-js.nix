{
  nix-filter,
  buildNpmPackage,
  nodejs,
  pageRoot ? "/",
  lib,
}: let
  packageJson = builtins.fromJSON (builtins.readFile ../../package.json);
in
  assert lib.assertMsg (lib.hasSuffix "/" pageRoot) "pageRoot must end with '/'";
  buildNpmPackage {
    pname = packageJson.name;
    version = packageJson.version;

    src = nix-filter {
      root = ../../.;
      include = [
        "index.html"
        "js"
        "package.json"
        "package-lock.json"
        "build"
      ];
      exclude = [
        (nix-filter.matchExt "ts")
      ];
    };

    PAGE_ROOT = pageRoot;

    # $ prefetch-npm-deps package-lock.json
    npmDepsHash = "sha256-FzJ6wovVUxHmIIhgLsB7N9++Cw7bzjpXcYFiYHny09w=";

    nativeBuildInputs = [
    ];

    buildInputs = [
    ];

    postPatch = ''
      patchShebangs .
    '';

    dontNpmInstall = true;
    installPhase = ''
      mkdir $out
      cp -r index.html dist $out
    '';
  }
