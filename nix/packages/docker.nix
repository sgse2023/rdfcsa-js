{
  dockerTools,
  rdfcsa-js,
  writeText,
  nginx,
  mailcap,
}: let
  port = "80";
  nginxConf = writeText "nginx.conf" ''
    user nobody nobody;
    daemon off;
    error_log /dev/stdout info;
    pid /dev/null;
    events {}

    http {
      access_log /dev/stdout;

      include ${mailcap}/etc/nginx/mime.types;
      # mailcap has a list that is too large for the default size
      types_hash_max_size 4096;
      # this one is missing...
      types {
        application/javascript mjs;
      }

      server {
        listen ${port};

        root ${rdfcsa-js};

        # static files
        location ~*
        "^/([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?(?<p10>[^/]*/)?(?!index\.html)(?<p11>[^/]*)\.(?<p12>.*)$" {
          try_files
            /$p11.$p12
            /$p10/$p11.$p12
            /$9/$p10/$p11.$p12
            /$8/$9/$p10/$p11.$p12
            /$7/$8/$9/$p10/$p11.$p12
            /$6/$7/$8/$9/$p10/$p11.$p12
            /$5/$6/$7/$8/$9/$p10/$p11.$p12
            /$4/$5/$6/$7/$8/$9/$p10/$p11.$p12
            /$3/$4/$5/$6/$7/$8/$9/$p10/$p11.$p12
            /$2/$3/$4/$5/$6/$7/$8/$9/$p10/$p11.$p12
            /$1/$2/$3/$4/$5/$6/$7/$8/$9/$p10/$p11.$p12
          =404;
        }

        # error handler for url matching above: if the path is too long,
        # the regex fails, and without this handler would simply server index.html
        location ~* "^/?.*/(?!index\.html)[^/]*\.[^/]*$" {
          return 500;
        }

        # server the index.html file
        location / {
          index index.html;
          try_files $uri $uri/index.html /index.html;
        }
      }
    }
  '';
in
  dockerTools.buildLayeredImage {
    name = "rdfcsa-js";

    # the real tag will be set by the publish step
    # (which has access to the value it's supposed to have)
    tag = null;

    contents = [
      dockerTools.fakeNss
      nginx
    ];

    extraCommands = ''
      # nginx still tries to read this directory even if error_log
      # directive specifies another file :/
      mkdir -p var/log/nginx
      mkdir -p var/cache/nginx
      mkdir -p tmp/
    '';

    config = {
      Cmd = ["nginx" "-c" nginxConf];
      Env = [
      ];
      ExposedPorts = {
        "${port}/tcp" = {};
      };
    };
  }
