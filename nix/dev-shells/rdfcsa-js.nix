{
  mkShell,
  rdfcsa-js,
  reuse,
  prefetch-npm-deps,
  miniserve,
  playwright-driver,
  nodePackages,
}:
mkShell {
  nativeBuildInputs =
    rdfcsa-js.nativeBuildInputs
    ++ [
      reuse
      prefetch-npm-deps

      miniserve
      nodePackages.typescript-language-server
    ];

  buildInputs =
    rdfcsa-js.buildInputs
    ++ [
    ];

  PLAYWRIGHT_SKIP_BROWSER_DOWNLOAD = "1";
  PLAYWRIGHT_BROWSERS_PATH = "${playwright-driver.browsers}";
}
