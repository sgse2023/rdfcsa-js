{
  nix-filter,
  mkShell,
  rdfcsa-js,
  commitlint,
  git,
  playwright-driver,
  skopeo,
  miniserve,
  rsync,
  openssh,
  writeShellScriptBin,
  coreutils,
}: let
  deps-prepared = rdfcsa-js.overrideAttrs (oldAttrs: {
    src = nix-filter {
      root = ../../.;
      include = [
        "package.json"
        "package-lock.json"
        "build"
      ];
    };

    # building doesn't buy as anything here,
    # as that would mean rebuilding the CI env on every code change.
    # the build results are not used by the CI steps anyway
    dontNpmBuild = true;

    # important: the target folder for the node_modules
    # *must* be called node_modules too
    # (otherwise npm does npm stuff)
    installPhase = ''
      mkdir $out
      mv node_modules $out/node_modules
    '';
  });
  rsync-deploy = writeShellScriptBin "rsync-deploy" ''
    # not sure if woodpecker is able to hide the env variable
    # if bash prints it. Better hide it manually
    set -e +x -o pipefail

    echo "$SGSE_DEPLOY_KEY" > /tmp/id_ed25519

    set -xe -o pipefail

    chmod 600 /tmp/id_ed25519
    exec ${rsync}/bin/rsync \
      --rsh="${openssh}/bin/ssh -i /tmp/id_ed25519 -o KnownHostsCommand=\"${coreutils}/bin/cat $SSH_KNOWN_HOSTS_FILE\"" \
      --verbose --delete --recursive --checksum \
      "$1" "$2"
  '';
in
  mkShell {
    nativeBuildInputs =
      rdfcsa-js.nativeBuildInputs
      ++ [
        skopeo
        commitlint
        miniserve
        rsync-deploy
        git # needed for commitlint
      ];

    buildInputs =
      rdfcsa-js.buildInputs
      ++ [
      ];

    PLAYWRIGHT_SKIP_BROWSER_DOWNLOAD = "1";
    PLAYWRIGHT_BROWSERS_PATH = "${playwright-driver.browsers}";

    shellHook = ''
      function linkDeps() {
        if [[ -d $1 && ! -L $1 ]]
        then
          echo "error: unable to link '$1': folder already exists"
        else
          if [[ -L $1 ]]
          then
            echo "warn: '$1' symlink outdated: replacing"
            unlink $1
          fi
          ln --verbose -s ${deps-prepared}/$1 $1
        fi
      }

      linkDeps node_modules
    '';
  }
