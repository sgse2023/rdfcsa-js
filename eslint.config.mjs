import path from 'path';
import { fileURLToPath } from 'url';

import globals from 'globals';
import js from '@eslint/js';
import rulesdir from 'eslint-plugin-rulesdir';
import prettier from 'eslint-config-prettier';
import lit from 'eslint-plugin-lit';
import wc from 'eslint-plugin-wc';
import lit_a11y from 'eslint-plugin-lit-a11y';
import ava from 'eslint-plugin-ava';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
rulesdir.RULES_DIR = path.join(__dirname, './build/eslint_rules');

export default [
    // global setup
    {
        ignores: ['dist/*'],
    },
    // configurations from eslint + plugins
    js.configs.recommended,
    prettier,
    {
        plugins: {
            lit: lit,
        },
        rules: lit.configs.recommended.rules,
    },
    {
        plugins: {
            wc: wc,
        },
        rules: wc.configs['best-practice'].rules,
    },
    {
        plugins: {
            'lit-a11y': lit_a11y,
        },
        rules: lit_a11y.configs.recommended.rules,
    },
    {
        plugins: {
            ava: ava,
        },
        rules: ava.configs.recommended.rules,
    },
    // global config
    {
        rules: {
            'no-unused-vars': [
                'error',
                {
                    vars: 'all',
                    varsIgnorePattern: '^_',
                    args: 'all',
                    argsIgnorePattern: '^_',
                    caughtErrors: 'all',
                    caughtErrorsIgnorePattern: '^_',
                    destructuredArrayIgnorePattern: '^_',
                },
            ],
            'array-callback-return': ['error', { checkForEach: true }],
            'no-duplicate-imports': ['error', { includeExports: true }],
            'grouped-accessor-pairs': ['error', 'getBeforeSet'],
            'no-implicit-coercion': ['error', { disallowTemplateShorthand: true }],
            'no-return-assign': ['error', 'always'],
            camelcase: ['error', { allow: ['lit_a11y'] }],
            'no-await-in-loop': 'error',
            'no-constant-binary-expression': 'error',
            'no-constructor-return': 'error',
            'no-promise-executor-return': 'error',
            'no-self-compare': 'error',
            'no-template-curly-in-string': 'error',
            'no-unmodified-loop-condition': 'error',
            'no-unreachable-loop': 'error',
            'no-unused-private-class-members': 'error',
            'no-use-before-define': 'error',
            'require-atomic-updates': 'error',
            'arrow-body-style': 'error',
            'block-scoped-var': 'error',
            curly: 'error',
            'default-case': 'error',
            'default-case-last': 'error',
            'default-param-last': 'error',
            eqeqeq: 'error',
            'guard-for-in': 'error',
            'no-array-constructor': 'error',
            'no-confusing-arrow': 'error',
            'no-else-return': 'error',
            'no-eq-null': 'error',
            'no-implied-eval': 'error',
            'no-eval': 'error',
            'no-invalid-this': 'error',
            'no-iterator': 'error',
            'no-lonely-if': 'error',
            'no-loop-func': 'error',
            'no-mixed-operators': 'error',
            'no-multi-assign': 'error',
            'no-multi-str': 'error',
            'no-nested-ternary': 'error',
            'no-unneeded-ternary': 'error',
            'no-new': 'error',
            'no-new-func': 'error',
            'no-new-object': 'error',
            'no-new-wrappers': 'error',
            'no-param-reassign': 'error',
            'no-throw-literal': 'error',
            'no-undef-init': 'error',
            'no-undefined': 'error',
            'no-useless-computed-key': 'error',
            'no-useless-concat': 'error',
            'no-useless-return': 'error',
            'no-var': 'error',
        },
        linterOptions: {
            noInlineConfig: false,
            reportUnusedDisableDirectives: true,
        },
    },
    // config for our source code
    {
        files: ['js/**/*.mjs'],
        languageOptions: {
            ecmaVersion: 2022,
            sourceType: 'module',
            globals: {
                ...globals.browser,
                PAGE_ROOT: 'readonly',
            },
        },
    },
    // make sure that source code + tests are typechecked
    {
        files: ['js/**/*.mjs', 'test/**/*.mjs', 'build/**/*.mjs'],
        plugins: {
            rulesdir: rulesdir,
        },
        rules: {
            'rulesdir/is-typechecked': 'error',
        },
    },
    // configuration for non-source code files (e.g. build scripts, config files)
    {
        files: ['**/*.cjs'],
        ignores: ['js/*'],
        languageOptions: {
            sourceType: 'module',
            globals: {
                ...globals.node,
            },
        },
    },
    {
        files: ['**/*.mjs', '**/*.js'],
        ignores: ['js/*'],
        languageOptions: {
            globals: {
                ...globals.nodeBuiltin,
            },
        },
    },
];
