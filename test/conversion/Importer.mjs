// @ts-check
import test from 'ava';
import { readFile } from 'node:fs/promises';
import { Importer } from '../../js/lib/conversion/Importer.mjs';
import { NTriplesFormat } from '../../js/lib/conversion/NTriplesFormat.mjs';
import { nonNull } from '../../js/lib/assertions.mjs';
import { TextQuery } from '../../js/lib/query/TextQuery.mjs';

const FullGraphQuery = new TextQuery('(?,?,?)');

// execute test serial,
// n3 seemed to be flakey when run with multiple threads...
test.serial('import', async (t) => {
    /**
     * @type {BinaryData | null}
     */
    let contents = null;
    try {
        const filePath = new URL('./test.nt', import.meta.url);
        contents = await readFile(filePath);
    } catch (
        /**
         * @type {*}
         */
        err
    ) {
        console.error('Failed to read Sample Data from test.nt', err.message);
    }

    const importer = new Importer(nonNull(contents), NTriplesFormat);

    const rdfCsa = importer.import();

    t.snapshot(rdfCsa.execute(FullGraphQuery));
});
