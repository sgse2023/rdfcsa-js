// @ts-check
import test from 'ava';
import { readFile } from 'node:fs/promises';
import { NTriplesFormat } from '../../js/lib/conversion/NTriplesFormat.mjs';
import { nonNull } from '../../js/lib/assertions.mjs';
import { RdfGraphData } from '../../js/lib/rdfstore/RdfGraphData.mjs';
import { RDFCSA } from '../../js/lib/rdfstore/RDFCSA.mjs';
import { convertStringArrayToRDFGraphData } from '../lib/rdfcsa-conversion.mjs';
import { QUERY_ALL } from '../../js/lib/query/SimpleQuery.mjs';

/**
 * @type {readonly string[]}
 */
const data = [
    'Inception',
    'filmed in',
    'L.A.',
    'L.A.',
    'city of',
    'USA',
    'E.Page',
    'appears in',
    'Inception',
    'L.DiCaprio',
    'appears in',
    'Inception',
    'J.Gordon',
    'appears in',
    'Inception',
    'J.Gordon',
    'born in',
    'USA',
    'J.Gordon',
    'lives in',
    'L.A.',
    'E.Page',
    'born in',
    'Canada',
    'L.DiCaprio',
    'born in',
    'USA',
    'L.DiCaprio',
    'awarded',
    'Oscar2015',
];

// execute test serial,
// n3 seemed to be flakey when run with multiple threads...
test.serial('import NTriplesExample', async (t) => {
    /**
     * @type {BinaryData | null}
     */
    let contents = null;
    try {
        const filePath = new URL('./test.nt', import.meta.url);
        contents = await readFile(filePath);
    } catch (
        /**
         * @type {*}
         */
        err
    ) {
        console.error('Failed to read Sample Data from test.nt', err.message);
    }

    t.snapshot(NTriplesFormat.import(nonNull(contents)));
});

// execute test serial,
// n3 seemed to be flakey when run with multiple threads...
test.serial('export NTriplesExample', async (t) => {
    /**
     * @type {BinaryData | null}
     */
    let contents = null;
    try {
        const filePath = new URL('./test.nt', import.meta.url);
        contents = await readFile(filePath);
    } catch (
        /**
         * @type {*}
         */
        err
    ) {
        console.error('Failed to read Sample Data from test.nt', err.message);
    }

    const nTriplesFormat = new NTriplesFormat();

    const data = NTriplesFormat.import(nonNull(contents));

    if (data instanceof RdfGraphData) {
        t.snapshot(nTriplesFormat.export(data));
    } else {
        t.fail();
    }
});

test.serial('add new File to existing Dataset', async (t) => {
    let contents = null;

    try {
        const filePath = new URL('./NTADD.nt', import.meta.url);
        contents = await readFile(filePath);
    } catch (
        /**
         * @type {*}
         */
        err
    ) {
        console.error('Failed to read Sample Data from test.nt', err.message);
    }

    const rdfStore = new RDFCSA(convertStringArrayToRDFGraphData(data.slice()), 10);

    NTriplesFormat.add(nonNull(contents), rdfStore);

    t.snapshot(rdfStore.execute(QUERY_ALL));
});
