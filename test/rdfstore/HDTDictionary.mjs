// @ts-check
import test from 'ava';
import { HDTDictionary } from '../../js/lib/rdfstore/HDTDicitonary.mjs';
import { convertStringArrayToRDFGraphData } from '../lib/rdfcsa-conversion.mjs';
import { nonNull } from '../../js/lib/assertions.mjs';
import { EditingCapactityError } from '../../js/lib/error/EditingCapacityError.mjs';

// the following data is the Example Data from the Paper "Space/time-efficient RDF stores based on circular suffix sorting"
/**
 * @type {readonly string[]}
 */
const correctSO = ['Inception', 'L.A.'];
/**
 * @type {readonly string[]}
 */
const correctS = ['E.Page', 'J.Gordon', 'L.DiCaprio'];
/**
 * @type {readonly string[]}
 */
const correctP = ['appears in', 'awarded', 'born in', 'city of', 'filmed in', 'lives in'];
/**
 * @type {readonly string[]}
 */
const correctO = ['Canada', 'Oscar2015', 'USA'];

/**
 * @type {readonly string[]}
 */
const data = [
    'Inception',
    'filmed in',
    'L.A.',
    'L.A.',
    'city of',
    'USA',
    'E.Page',
    'appears in',
    'Inception',
    'L.DiCaprio',
    'appears in',
    'Inception',
    'J.Gordon',
    'appears in',
    'Inception',
    'J.Gordon',
    'born in',
    'USA',
    'J.Gordon',
    'lives in',
    'L.A.',
    'E.Page',
    'born in',
    'Canada',
    'L.DiCaprio',
    'born in',
    'USA',
    'L.DiCaprio',
    'awarded',
    'Oscar2015',
];

test('Dictionary Creation', (t) => {
    let dictionary = new HDTDictionary(convertStringArrayToRDFGraphData(data.slice()), 0);

    for (let i = 0; i < correctSO.length; i++) {
        t.is(correctSO[i], dictionary.SubjectObjectDictionary[i], `i = ${i}`);
    }
    for (let i = 0; i < correctS.length; i++) {
        t.is(correctS[i], dictionary.SubjectDictionary[i], `i = ${i}`);
    }
    for (let i = 0; i < correctP.length; i++) {
        t.is(correctP[i], dictionary.PredicateDictionary[i], `i = ${i}`);
    }
    for (let i = 0; i < correctO.length; i++) {
        t.is(correctO[i], dictionary.ObjectDictionary[i], `i = ${i}`);
    }

    t.pass();
});

test('Check Overrun Error when Dictionary addonCapacity is full', (t) => {
    let dictionary = new HDTDictionary(convertStringArrayToRDFGraphData(data.slice()), 1);

    const newSubject = 'ThisIsANewSubject';
    const newSubject2 = 'ThisIsANewSubject2';
    const newPredicate = 'ThisIsANewPredicate';
    const newPredicate2 = 'ThisIsANewPredicate2';
    const newObject = 'ThisIsANewObject';
    const newObject2 = 'ThisIsANewObject2';

    dictionary.addSubject(newSubject);
    dictionary.addPredicate(newPredicate);
    dictionary.addObject(newObject);

    t.throws(
        () => {
            dictionary.addSubject(newSubject2);
        },
        { instanceOf: EditingCapactityError },
    );
    t.throws(
        () => {
            dictionary.addPredicate(newPredicate2);
        },
        { instanceOf: EditingCapactityError },
    );
    t.throws(
        () => {
            dictionary.addObject(newObject2);
        },
        { instanceOf: EditingCapactityError },
    );
});

test('Add Subject that alread exists', (t) => {
    const oldSubject = 'Inception';
    const newSubject = 'ThisIsANewSubject';
    let dictionary = new HDTDictionary(convertStringArrayToRDFGraphData(data.slice()), 1);

    dictionary.addSubject(oldSubject); // nothing should be added here
    dictionary.addSubject(newSubject); // when old Subject dosent get Ignored the dicitonary would overrun

    //check every old Value is still available
    for (let i = 0; i < correctS.length + correctSO.length; i++) {
        const subject = dictionary.getSubject(i);
        if (i < correctSO.length) {
            t.is(correctSO[i], subject, `i = ${i}`);
        } else {
            t.is(correctS[i - correctSO.length], subject, `i = ${i}`);
        }
    }
    for (let i = 0; i < correctP.length; i++) {
        t.is(correctP[i], dictionary.getPredicate(i), `i = ${i}`);
    }
    for (let i = 0; i < correctO.length + correctSO.length; i++) {
        const object = dictionary.getObject(i);
        if (i < correctSO.length) {
            t.is(correctSO[i], object, `i = ${i}`);
        } else {
            t.is(correctO[i - correctSO.length], object, `i = ${i}`);
        }
    }

    t.is(
        correctSO.length + correctS.length,
        dictionary.getSubjectID(newSubject),
        'New Subject could not be found by ID',
    );

    t.is(
        newSubject,
        nonNull(dictionary.getSubject(correctSO.length + correctS.length)),
        'new Element could not be retreived',
    );
});

test('Add Predicate that alread exists', (t) => {
    const oldPredicate = 'appears in';
    const newPredicate = 'ThisIsANewPredicate';
    let dictionary = new HDTDictionary(convertStringArrayToRDFGraphData(data.slice()), 1);

    dictionary.addPredicate(oldPredicate); // nothing should be added here
    dictionary.addPredicate(newPredicate); // when old Predicate dosent get Ignored the dicitonary would overrun

    //check every old Value is still available
    for (let i = 0; i < correctS.length + correctSO.length; i++) {
        const subject = dictionary.getSubject(i);
        if (i < correctSO.length) {
            t.is(correctSO[i], subject, `i = ${i}`);
        } else {
            t.is(correctS[i - correctSO.length], subject, `i = ${i}`);
        }
    }
    for (let i = 0; i < correctP.length; i++) {
        t.is(correctP[i], dictionary.getPredicate(i), `i = ${i}`);
    }
    for (let i = 0; i < correctO.length + correctSO.length; i++) {
        const object = dictionary.getObject(i);
        if (i < correctSO.length) {
            t.is(correctSO[i], object, `i = ${i}`);
        } else {
            t.is(correctO[i - correctSO.length], object, `i = ${i}`);
        }
    }

    t.is(
        correctP.length + dictionary.CurrentPredicateGap,
        dictionary.getPredicateID(newPredicate),
        'New Predicate could not be found by ID',
    );

    t.is(
        newPredicate,
        nonNull(dictionary.getPredicate(correctP.length)),
        'new Element could not be retreived',
    );
});

test('Add Object that alread exists', (t) => {
    const oldObject = 'Canada';
    const newObject = 'ThisIsANewObject';
    let dictionary = new HDTDictionary(convertStringArrayToRDFGraphData(data.slice()), 1);

    dictionary.addObject(oldObject); // nothing should be added here
    dictionary.addObject(newObject); // when old Predicate dosent get Ignored the dicitonary would overrun

    //check every old Value is still available
    for (let i = 0; i < correctS.length + correctSO.length; i++) {
        const subject = dictionary.getSubject(i);
        if (i < correctSO.length) {
            t.is(correctSO[i], subject, `i = ${i}`);
        } else {
            t.is(correctS[i - correctSO.length], subject, `i = ${i}`);
        }
    }
    for (let i = 0; i < correctP.length; i++) {
        t.is(correctP[i], dictionary.getPredicate(i), `i = ${i}`);
    }
    for (let i = 0; i < correctO.length + correctSO.length; i++) {
        const object = dictionary.getObject(i);
        if (i < correctSO.length) {
            t.is(correctSO[i], object, `i = ${i}`);
        } else {
            t.is(correctO[i - correctSO.length], object, `i = ${i}`);
        }
    }

    t.is(
        correctSO.length + correctO.length + dictionary.CurrentObjectGap,
        dictionary.getObjectID(newObject),
        'New Object could not be found by ID',
    );

    t.is(
        newObject,
        nonNull(dictionary.getObject(correctSO.length + correctO.length)),
        'new Element could not be retreived',
    );
});

test('Add new Subject, Predicate and Object', (t) => {
    const newObject = 'ThisIsANewObject';
    const newPredicate = 'ThisIsANewPredicate';
    const newSubject = 'ThisIsANewSubject';

    let dictionary = new HDTDictionary(convertStringArrayToRDFGraphData(data.slice()), 1);

    dictionary.addSubject(newSubject);
    dictionary.addPredicate(newPredicate);
    dictionary.addObject(newObject);

    //check every old Value is still available
    for (let i = 0; i < correctS.length + correctSO.length; i++) {
        const subject = dictionary.getSubject(i);
        if (i < correctSO.length) {
            t.is(correctSO[i], subject, `i = ${i}`);
        } else {
            t.is(correctS[i - correctSO.length], subject, `i = ${i}`);
        }
    }
    for (let i = 0; i < correctP.length; i++) {
        t.is(correctP[i], dictionary.getPredicate(i), `i = ${i}`);
    }
    for (let i = 0; i < correctO.length + correctSO.length; i++) {
        const object = dictionary.getObject(i);
        if (i < correctSO.length) {
            t.is(correctSO[i], object, `i = ${i}`);
        } else {
            t.is(correctO[i - correctSO.length], object, `i = ${i}`);
        }
    }

    t.is(
        correctSO.length + correctS.length,
        dictionary.getSubjectID(newSubject),
        'New Subject could not be found by ID',
    );
    t.is(
        correctP.length + dictionary.CurrentPredicateGap,
        dictionary.getPredicateID(newPredicate),
        'New Predicate could not be found by ID',
    );
    t.is(
        correctSO.length + correctO.length + dictionary.CurrentObjectGap,
        dictionary.getObjectID(newObject),
        'New Object could not be found by ID',
    );

    t.is(
        newSubject,
        nonNull(dictionary.getSubject(correctSO.length + correctS.length)),
        'new Element could not be retreived',
    );
    t.is(
        newPredicate,
        nonNull(dictionary.getPredicate(correctP.length)),
        'new Element could not be retreived',
    );
    t.is(
        newObject,
        nonNull(dictionary.getObject(correctSO.length + correctO.length)),
        'new Element could not be retreived',
    );
});

test('Add new SubjectObject Subject', (t) => {
    const newSubject = 'Oscar2015';
    let dictionary = new HDTDictionary(convertStringArrayToRDFGraphData(data.slice()), 1);

    dictionary.addSubject(newSubject);

    //check every old Value is still available
    for (let i = 0; i < correctS.length + correctSO.length; i++) {
        const subject = dictionary.getSubject(i);
        if (i < correctSO.length) {
            t.is(correctSO[i], subject, `i = ${i}`);
        } else {
            t.is(correctS[i - correctSO.length], subject, `i = ${i}`);
        }
    }
    for (let i = 0; i < correctP.length; i++) {
        t.is(correctP[i], dictionary.getPredicate(i), `i = ${i}`);
    }
    for (let i = 0; i < correctO.length + correctSO.length; i++) {
        const object = dictionary.getObject(i);
        if (i < correctSO.length) {
            t.is(correctSO[i], object, `i = ${i}`);
        } else {
            t.is(correctO[i - correctSO.length], object, `i = ${i}`);
        }
    }

    t.is(
        correctSO.length + correctS.length,
        dictionary.getSubjectID(newSubject),
        'New Subject could not be found by ID',
    );

    t.is(
        newSubject,
        nonNull(dictionary.getSubject(correctSO.length + correctS.length)),
        'new Element could not be retreived',
    );

    t.is(
        dictionary.getSubjectID(newSubject),
        dictionary.getObjectNodeIDforGraphCreation(dictionary.getObjectID(newSubject)),
        'SO Object creation failed',
    );
});

test('Add new SubjectObject Object', (t) => {
    const newObject = 'E.Page';
    let dictionary = new HDTDictionary(convertStringArrayToRDFGraphData(data.slice()), 1);

    dictionary.addObject(newObject);

    //check every old Value is still available
    for (let i = 0; i < correctS.length + correctSO.length; i++) {
        const subject = dictionary.getSubject(i);
        if (i < correctSO.length) {
            t.is(correctSO[i], subject, `i = ${i}`);
        } else {
            t.is(correctS[i - correctSO.length], subject, `i = ${i}`);
        }
    }
    for (let i = 0; i < correctP.length; i++) {
        t.is(correctP[i], dictionary.getPredicate(i), `i = ${i}`);
    }
    for (let i = 0; i < correctO.length + correctSO.length; i++) {
        const object = dictionary.getObject(i);
        if (i < correctSO.length) {
            t.is(correctSO[i], object, `i = ${i}`);
        } else {
            t.is(correctO[i - correctSO.length], object, `i = ${i}`);
        }
    }

    t.is(
        correctSO.length + correctO.length + dictionary.CurrentObjectGap,
        dictionary.getObjectID(newObject),
        'New Object could not be found by ID',
    );

    t.is(
        newObject,
        nonNull(dictionary.getObject(correctSO.length + correctO.length)),
        'new Element could not be retreived',
    );

    t.is(
        dictionary.getSubjectID(newObject),
        dictionary.getObjectNodeIDforGraphCreation(dictionary.getObjectID(newObject)),
        'SO Object creation failed',
    );
});
