// @ts-check

import test from 'ava';
import { BitVector } from '../../js/lib/rdfstore/BitVector.mjs';
import { nonNull } from '../../js/lib/assertions.mjs';
import { IndexBitVector } from '../../js/lib/rdfstore/IndexBitVector.mjs';

/**
 * @returns {BitVector}
 */
function getVector() {
    const vector = new BitVector();
    for (const i of [true, true, false, true, true, false].values()) {
        vector.append(i);
    }

    return vector;
}

test('rank', (t) => {
    const v = getVector();

    t.is(v.rank(1), 1);
    t.is(v.rank(2), 2);
    t.is(v.rank(3), 2);
    t.is(v.rank(4), 3);
    t.is(v.rank(5), 4);
    t.is(v.rank(6), 4);
});

test('select', (t) => {
    const v = getVector();

    t.is(v.select(1), 1);
    t.is(v.select(2), 2);
    t.is(v.select(3), 4);
    t.is(v.select(4), 5);
});

test('rank:block:true', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 169; i++) {
        v.append(true);
    }

    for (let i = 1; i <= 169; i++) {
        t.is(v.rank(i), i, `i=${i}`);
    }
});

test('rank:superblock:true', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 1337; i++) {
        v.append(true);
    }

    for (let i = 1; i <= 1337; i++) {
        t.is(v.rank(i), i, `i=${i}`);
    }
});

test('select:block:true', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 169; i++) {
        v.append(true);
    }

    for (let i = 1; i <= 169; i++) {
        t.is(v.select(i), i, `i=${i}`);
    }
});

test('select:superblock:true', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 1337; i++) {
        v.append(true);
    }

    for (let i = 1; i <= 1337; i++) {
        t.is(v.select(i), i, `i=${i}`);
    }
});

test('rank:0', (t) => {
    const v = new BitVector();
    v.append(false);
    t.is(v.rank(1), 0);
});

test('rank:block:false', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 169; i++) {
        v.append(false);
    }

    for (let i = 1; i <= 169; i++) {
        t.is(v.rank(i), 0, `i=${i}`);
    }
});

test('rank:superblock:false', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 1337; i++) {
        v.append(false);
    }

    for (let i = 1; i <= 1337; i++) {
        t.is(v.rank(i), 0, `i=${i}`);
    }
});

test('rank:block:001', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 169; i += 3) {
        v.append(false);
        v.append(false);
        v.append(true);
    }

    for (let i = 1; i <= 169; i += 3) {
        const expected = Math.floor(i / 3);
        t.is(v.rank(i), expected, `i=${i}`);
        t.is(v.rank(i + 1), expected, `i=${i + 1}`);
        t.is(v.rank(i + 2), expected + 1, `i=${i + 2}`);
    }
});

test('rank:block:010', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 169; i += 3) {
        v.append(false);
        v.append(true);
        v.append(false);
    }

    for (let i = 1; i <= 169; i += 3) {
        const expected = Math.ceil(i / 3);
        t.is(v.rank(i), expected - 1, `i=${i}`);
        t.is(v.rank(i + 1), expected, `i=${i + 1}`);
        t.is(v.rank(i + 2), expected, `i=${i + 2}`);
    }
});

test('rank:block:100', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 169; i += 3) {
        v.append(true);
        v.append(false);
        v.append(false);
    }

    for (let i = 1; i <= 169; i += 3) {
        const expected = Math.ceil(i / 3);
        t.is(v.rank(i), expected, `i=${i}`);
        t.is(v.rank(i + 1), expected, `i=${i + 1}`);
        t.is(v.rank(i + 2), expected, `i=${i + 2}`);
    }
});

test('rank:block:011', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 169; i += 3) {
        v.append(false);
        v.append(true);
        v.append(true);
    }

    for (let i = 1; i <= 169; i += 3) {
        const expected = Math.ceil(i / 3) * 2;
        t.is(v.rank(i), expected - 2, `i=${i}`);
        t.is(v.rank(i + 1), expected - 1, `i=${i + 1}`);
        t.is(v.rank(i + 2), expected, `i=${i + 2}`);
    }
});

test('rank:block:101', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 169; i += 3) {
        v.append(true);
        v.append(false);
        v.append(true);
    }

    for (let i = 1; i <= 169; i += 3) {
        const expected = (Math.ceil(i / 3) * 2) - 1;
        t.is(v.rank(i), expected, `i=${i}`);
        t.is(v.rank(i + 1), expected, `i=${i + 1}`);
        t.is(v.rank(i + 2), expected + 1, `i=${i + 2}`);
    }
});

test('rank:block:110', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 169; i += 3) {
        v.append(true);
        v.append(true);
        v.append(false);
    }

    for (let i = 1; i <= 169; i += 3) {
        const expected = Math.ceil(i / 3) * 2;
        t.is(v.rank(i), expected - 1, `i=${i}`);
        t.is(v.rank(i + 1), expected, `i=${i + 1}`);
        t.is(v.rank(i + 2), expected, `i=${i + 2}`);
    }
});

test('rank:superblock:001', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 1337; i += 3) {
        v.append(false);
        v.append(false);
        v.append(true);
    }

    for (let i = 1; i <= 1337; i += 3) {
        const expected = Math.floor(i / 3);
        t.is(v.rank(i), expected, `i=${i}`);
        t.is(v.rank(i + 1), expected, `i=${i + 1}`);
        t.is(v.rank(i + 2), expected + 1, `i=${i + 2}`);
    }
});

test('rank:superblock:010', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 1337; i += 3) {
        v.append(false);
        v.append(true);
        v.append(false);
    }

    for (let i = 1; i <= 1337; i += 3) {
        const expected = Math.ceil(i / 3);
        t.is(v.rank(i), expected - 1, `i=${i}`);
        t.is(v.rank(i + 1), expected, `i=${i + 1}`);
        t.is(v.rank(i + 2), expected, `i=${i + 2}`);
    }
});

test('rank:superblock:100', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 1337; i += 3) {
        v.append(true);
        v.append(false);
        v.append(false);
    }

    for (let i = 1; i <= 1337; i += 3) {
        const expected = Math.ceil(i / 3);
        t.is(v.rank(i), expected, `i=${i}`);
        t.is(v.rank(i + 1), expected, `i=${i + 1}`);
        t.is(v.rank(i + 2), expected, `i=${i + 2}`);
    }
});

test('rank:superblock:011', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 1337; i += 3) {
        v.append(false);
        v.append(true);
        v.append(true);
    }

    for (let i = 1; i <= 1337; i += 3) {
        const expected = Math.ceil(i / 3) * 2;
        t.is(v.rank(i), expected - 2, `i=${i}`);
        t.is(v.rank(i + 1), expected - 1, `i=${i + 1}`);
        t.is(v.rank(i + 2), expected, `i=${i + 2}`);
    }
});

test('rank:superblock:101', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 1337; i += 3) {
        v.append(true);
        v.append(false);
        v.append(true);
    }

    for (let i = 1; i <= 1337; i += 3) {
        const expected = (Math.ceil(i / 3) * 2) - 1;
        t.is(v.rank(i), expected, `i=${i}`);
        t.is(v.rank(i + 1), expected, `i=${i + 1}`);
        t.is(v.rank(i + 2), expected + 1, `i=${i + 2}`);
    }
});

test('rank:superblock:110', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 1337; i += 3) {
        v.append(true);
        v.append(true);
        v.append(false);
    }

    for (let i = 1; i <= 1337; i += 3) {
        const expected = Math.ceil(i / 3) * 2;
        t.is(v.rank(i), expected - 1, `i=${i}`);
        t.is(v.rank(i + 1), expected, `i=${i + 1}`);
        t.is(v.rank(i + 2), expected, `i=${i + 2}`);
    }
});

test('select:notfound', (t) => {
    const v = new BitVector();
    for (let i = 0; i < 24; i++) {
        v.append(false);
    }
    t.is(v.select(10), 25);
});

test('select:block:notfound', (t) => {
    const v = new BitVector();
    for (let i = 0; i < 169; i++) {
        v.append(false);
    }
    t.is(v.select(10), 170);
});

test('select:superblock:notfound', (t) => {
    const v = new BitVector();
    for (let i = 0; i < 1337; i++) {
        v.append(false);
    }
    t.is(v.select(10), 1338);
});

test('select:0', (t) => {
    const v = new BitVector();

    v.append(true);

    t.is(v.select(0), -1);
});

test('select:block:001', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 169; i += 3) {
        v.append(false);
        v.append(false);
        v.append(true);
    }

    for (let i = 1; i <= 169; i += 3) {
        const rank = Math.floor(i / 3) + 1;
        t.is(v.select(rank), i + 2, `rank=${rank}`);
    }
});

test('select:block:010', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 169; i += 3) {
        v.append(false);
        v.append(true);
        v.append(false);
    }

    for (let i = 1; i <= 169; i += 3) {
        const rank = Math.floor(i / 3) + 1;
        t.is(v.select(rank), i + 1, `rank=${rank}`);
    }
});

test('select:block:100', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 169; i += 3) {
        v.append(true);
        v.append(false);
        v.append(false);
    }

    for (let i = 1; i <= 169; i += 3) {
        const rank = Math.ceil(i / 3);
        t.is(v.select(rank), i, `rank=${rank}`);
    }
});

test('select:block:011', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 169; i += 3) {
        v.append(false);
        v.append(true);
        v.append(true);
    }

    for (let i = 1; i <= 169; i += 3) {
        const rank = (Math.floor(i / 3) * 2) + 1;
        t.is(v.select(rank), i + 1, `rank=${rank}`);
        t.is(v.select(rank + 1), i + 2, `rank=${rank + 1}`);
    }
});

test('select:block:101', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 169; i += 3) {
        v.append(true);
        v.append(false);
        v.append(true);
    }

    for (let i = 1; i <= 169; i += 3) {
        const rank = (Math.floor(i / 3) * 2) + 1;
        t.is(v.select(rank), i, `rank=${rank}`);
        t.is(v.select(rank + 1), i + 2, `rank=${rank + 1}`);
    }
});

test('select:block:110', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 169; i += 3) {
        v.append(true);
        v.append(true);
        v.append(false);
    }

    for (let i = 1; i <= 169; i += 3) {
        const rank = (Math.floor(i / 3) * 2) + 1;
        t.is(v.select(rank), i, `rank=${rank}`);
        t.is(v.select(rank + 1), i + 1, `rank=${rank + 1}`);
    }
});

test('select:superblock:001', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 1337; i += 3) {
        v.append(false);
        v.append(false);
        v.append(true);
    }

    for (let i = 1; i <= 1337; i += 3) {
        const rank = Math.floor(i / 3) + 1;
        t.is(v.select(rank), i + 2, `i=${i + 2}, rank=${rank}`);
    }
});

test('select:superblock:010', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 1337; i += 3) {
        v.append(false);
        v.append(true);
        v.append(false);
    }

    for (let i = 1; i <= 1337; i += 3) {
        const rank = Math.floor(i / 3) + 1;
        t.is(v.select(rank), i + 1, `i=${i + 1}, rank=${rank}`);
    }
});

test('select:superblock:100', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 1337; i += 3) {
        v.append(true);
        v.append(false);
        v.append(false);
    }

    for (let i = 1; i <= 1337; i += 3) {
        const rank = Math.ceil(i / 3);
        t.is(v.select(rank), i, `rank=${rank}`);
    }
});

test('select:superblock:011', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 1337; i += 3) {
        v.append(false);
        v.append(true);
        v.append(true);
    }

    for (let i = 1; i <= 1337; i += 3) {
        const rank = (Math.floor(i / 3) * 2) + 1;
        t.is(v.select(rank), i + 1, `rank=${rank}`);
        t.is(v.select(rank + 1), i + 2, `rank=${rank + 1}`);
    }
});

test('select:superblock:101', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 1337; i += 3) {
        v.append(true);
        v.append(false);
        v.append(true);
    }

    for (let i = 1; i <= 1337; i += 3) {
        const rank = (Math.floor(i / 3) * 2) + 1;
        t.is(v.select(rank), i, `rank=${rank}`);
        t.is(v.select(rank + 1), i + 2, `rank=${rank + 1}`);
    }
});

test('select:superblock:110', (t) => {
    const v = new BitVector();

    for (let i = 0; i < 1337; i += 3) {
        v.append(true);
        v.append(true);
        v.append(false);
    }

    for (let i = 1; i <= 1337; i += 3) {
        const rank = (Math.floor(i / 3) * 2) + 1;
        t.is(v.select(rank), i, `rank=${rank}`);
        t.is(v.select(rank + 1), i + 1, `rank=${rank + 1}`);
    }
});

const D = [
    1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0,
];
test('paperdata', (t) => {
    const v = new IndexBitVector(new BitVector());
    for (let i = 0; i < D.length; i++) {
        v.append(nonNull(D[i] === 1));
    }
    t.is(v.select(9), 18);
    t.is(v.select(12), 23);
    t.is(v.select(16), 30);
});

// issue #190:
// prevent wrong indices being returned
// for more than block-spanning consecutive 0's.
// See BitVector.binSearchInexact for context.
test('invisble elements', (t) => {
    const v = new BitVector();
    const BLOCK_LENGTH = 32;

    for (let i = 0; i < BLOCK_LENGTH * 3; i += 3) {
        v.append(true);
        v.append(false);
        v.append(true);
    }

    for (let i = 0; i < BLOCK_LENGTH * 3; i++) {
        v.append(false);
    }

    for (let i = 0; i < BLOCK_LENGTH * 2; i += 3) {
        v.append(true);
        v.append(false);
        v.append(true);
    }

    const INDEX_UNCHANGED = BLOCK_LENGTH * 3;
    t.is(v.select(INDEX_UNCHANGED * (2 / 3)), INDEX_UNCHANGED);
});
