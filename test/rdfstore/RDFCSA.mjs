// @ts-check
import test from 'ava';
import {
    RDFCSA,
    createIDArrayFromRdfGraphData,
    addGapToArrayIndices,
    createAAndD,
} from '../../js/lib/rdfstore/RDFCSA.mjs';
import {
    convertStringArrayToRDFGraphData,
    convertIdArrayToTripleArray,
} from '../lib/rdfcsa-conversion.mjs';
import { nonNull } from '../../js/lib/assertions.mjs';
import { HDTDictionary } from '../../js/lib/rdfstore/HDTDicitonary.mjs';
import { QUERY_ALL } from '../../js/lib/query/SimpleQuery.mjs';
import { TriplePattern } from '../../js/lib/query/TriplePattern.mjs';
import { QueryTripleElement } from '../../js/lib/query/QueryTripleElement.mjs';
import { readFile } from 'node:fs/promises';

/** @typedef {import('../../js/lib/rdfstore/ImportTriple.mjs').ImportTriple} ImportTriple */

// the following data is the Example Data from the Paper "Space/time-efficient RDF stores based on circular suffix sorting"
/**
 * @type {readonly string[]}
 */
const correctSO = ['Inception', 'L.A.'];
/**
 * @type {readonly string[]}
 */
const correctS = ['E.Page', 'J.Gordon', 'L.DiCaprio'];
/**
 * @type {readonly string[]}
 */
const correctP = ['appears in', 'awarded', 'born in', 'city of', 'filmed in', 'lives in'];
/**
 * @type {readonly string[]}
 */
const correctO = ['Canada', 'Oscar2015', 'USA'];

/**
 * @type {readonly string[]}
 */
const data = [
    'Inception',
    'filmed in',
    'L.A.',
    'L.A.',
    'city of',
    'USA',
    'E.Page',
    'appears in',
    'Inception',
    'L.DiCaprio',
    'appears in',
    'Inception',
    'J.Gordon',
    'appears in',
    'Inception',
    'J.Gordon',
    'born in',
    'USA',
    'J.Gordon',
    'lives in',
    'L.A.',
    'E.Page',
    'born in',
    'Canada',
    'L.DiCaprio',
    'born in',
    'USA',
    'L.DiCaprio',
    'awarded',
    'Oscar2015',
];

/**
 * @type {readonly number[]}
 */
const correctIDArray = [
    0, 4, 1, 1, 3, 4, 2, 0, 0, 4, 0, 0, 3, 0, 0, 3, 2, 4, 3, 5, 1, 2, 2, 2, 4, 2, 4, 4, 1, 3,
];

/**
 * @type {readonly number[]}
 */
const correctOrderedIDs = [
    0, 4, 1, 1, 3, 4, 2, 0, 0, 2, 2, 2, 3, 0, 0, 3, 2, 4, 3, 5, 1, 4, 0, 0, 4, 1, 3, 4, 2, 4,
];

/**
 * @type {readonly number[]}
 */
const correctOrderedIdsWithGap = [
    0, 9, 12, 1, 8, 15, 2, 5, 11, 2, 7, 13, 3, 5, 11, 3, 7, 15, 3, 10, 12, 4, 5, 11, 4, 6, 14, 4, 7,
    15,
];

/**
 * @type {readonly number[]}
 */
const correctAOrder = [
    0, 3, 6, 9, 12, 15, 18, 21, 24, 27, 7, 13, 22, 25, 10, 16, 28, 4, 1, 19, 8, 14, 23, 2, 20, 11,
    26, 5, 17, 29,
];

/**
 * @type {readonly number[]}
 */
const correctD = [
    1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0,
];

/**
 * @type {readonly number[]}
 */
const correctPSI = [
    118, 117, 110, 114, 111, 115, 119, 112, 113, 116, 220, 221, 222, 226, 225, 228, 229, 227, 223,
    224, 2, 4, 7, 0, 6, 3, 8, 1, 5, 9,
];

const numberOfElements =
    (correctSO.length * 2) + correctS.length + correctP.length + correctO.length;

test('ID Array Creation', (t) => {
    const rdfGraphdata = convertStringArrayToRDFGraphData(data.slice());

    //Map dictionary to Array
    let idArray = createIDArrayFromRdfGraphData(rdfGraphdata, new HDTDictionary(rdfGraphdata, 0));

    for (let i = 0; i < correctIDArray.length; i = i + 3) {
        t.is(correctIDArray[i], nonNull(idArray[i / 3]).subject, `i = ${i}`);
        t.is(correctIDArray[i + 1], nonNull(idArray[i / 3]).predicate, `i = ${i + 1}`);
        t.is(correctIDArray[i + 2], nonNull(idArray[i / 3]).object, `i = ${i + 2}`);
    }

    t.pass();
});

test('Gap Function', (t) => {
    /**
     * @type {ImportTriple[]}
     */
    const correctOrderedIDsTrples = convertIdArrayToTripleArray(correctOrderedIDs.slice());

    let arrayWithGap = addGapToArrayIndices(
        correctOrderedIDsTrples,
        correctSO.length + correctS.length,
        correctSO.length + correctS.length + correctP.length,
    );

    for (let i = 0; i < correctOrderedIdsWithGap.length; i++) {
        t.is(correctOrderedIdsWithGap[i], arrayWithGap[i], `i = ${i}`);
    }

    t.pass();
});

test('A creation', (t) => {
    let createdAandD = createAAndD(correctOrderedIdsWithGap.slice());

    for (let i = 0; i < correctAOrder.length; i++) {
        t.is(correctAOrder[i], createdAandD[0][i], `i = ${i}`);
    }

    t.pass();
});

test('D creation', (t) => {
    let createdAandD = createAAndD(correctOrderedIdsWithGap.slice());
    for (let i = 0; i < correctD.length; i++) {
        t.is(correctD[i], createdAandD[1]._at(i) ? 1 : 0, `i = ${i}`);
    }

    t.pass();
});

test('RDFCSA creation', (t) => {
    const addonCapacity = 100;
    const rdfcsa = new RDFCSA(convertStringArrayToRDFGraphData(data.slice()), addonCapacity);

    // check dictionarys
    for (let i = 0; i < correctSO.length; i++) {
        t.is(correctSO[i], rdfcsa.dictionary.getSubject(i), `i = ${i}`);
        t.is(correctSO[i], rdfcsa.dictionary.getObject(i), `i = ${i}`);
    }

    for (let i = 0; i < correctS.length; i++) {
        t.is(correctS[i], rdfcsa.dictionary.getSubject(i + correctSO.length), `i = ${i}`);
    }

    for (let i = 0; i < correctP.length; i++) {
        t.is(correctP[i], rdfcsa.dictionary.getPredicate(i), `i = ${i}`);
    }

    for (let i = 0; i < correctO.length; i++) {
        t.is(correctO[i], rdfcsa.dictionary.getObject(i + correctSO.length), `i = ${i}`);
    }

    // check Data structures
    for (let i = 0; i < correctD.length; i++) {
        t.is(correctD[i], rdfcsa.indexD(i), `i = ${i}`);
    }

    for (let i = 0; i < correctPSI.length; i++) {
        if (i < correctPSI.length / 3) {
            t.is(correctPSI[i], rdfcsa.getPSIIndex(i), `i = ${i}`);
        } else if (i < (2 * correctPSI.length) / 3) {
            t.is(correctPSI[i], rdfcsa.getPSIIndex(i + addonCapacity), `i = ${i}`);
        } else {
            t.is(correctPSI[i], rdfcsa.getPSIIndex(i + (2 * addonCapacity)), `i = ${i}`);
        }
    }

    t.pass();
});

test('RDFCSA Rank', (t) => {
    const rdfcsa = new RDFCSA(convertStringArrayToRDFGraphData(data.slice()));

    for (let i = 0; i < correctIDArray.length; i++) {
        t.is(correctOrderedIdsWithGap[nonNull(correctAOrder[i])], rdfcsa.getDRank1(i), `i = ${i}`);
    }
});

test('RDFCSA Select', (t) => {
    const rdfcsa = new RDFCSA(convertStringArrayToRDFGraphData(data.slice()), 0);

    for (let i = 0; i <= numberOfElements; i++) {
        let selectResult = rdfcsa.getDSelect1(i);
        for (let j = 0; j < selectResult; j++) {
            const a = nonNull(correctAOrder[j]);
            const id = correctOrderedIDs[a];
            t.false(
                correctOrderedIdsWithGap[nonNull(correctAOrder[j])] === i,
                `i = ${i}, correctOrderedIdsWithGap = ${id},
                 selectResult = ${selectResult}, j = ${j}, correctAOrder = ${a}`,
            );
        }
    }
});

test('export RDFCSA native', (t) => {
    const rdfcsa = new RDFCSA(convertStringArrayToRDFGraphData(data.slice()));
    t.snapshot(rdfcsa.export(rdfcsa.execute(QUERY_ALL)));
});

test('import RDFCSA native', (t) => {
    const rdfcsaInitial = new RDFCSA(convertStringArrayToRDFGraphData(data.slice()));
    const rdfCsa = RDFCSA.import(rdfcsaInitial.export(rdfcsaInitial.execute(QUERY_ALL)));

    const initialQuery = rdfcsaInitial.execute(QUERY_ALL);

    // first cast for test, second for TypeScript
    if (t.assert(rdfCsa instanceof RDFCSA) && rdfCsa instanceof RDFCSA) {
        //check if all original Triples are in the newly imported Triple Store
        for (const value of initialQuery.links.values()) {
            const triplePattern = new TriplePattern(
                new QueryTripleElement(initialQuery.nodeAtId(value.subject).name, false),
                new QueryTripleElement(value.name, false),
                new QueryTripleElement(initialQuery.nodeAtId(value.object).name, false),
            );

            const triple = rdfCsa.retreiveSingleTriple(triplePattern);

            t.is(
                nonNull(triplePattern.predicate.value),
                nonNull(triple[0]).name,
                `Wrong Predicate`,
            );
            t.is(
                nonNull(triplePattern.subject.value),
                nonNull(rdfCsa.dictionary.getSubject(nonNull(triple[0]).subject)),
                `Wrong Subject`,
            );
            t.is(
                nonNull(triplePattern.object.value),
                nonNull(rdfCsa.dictionary.getObjectWithGap(nonNull(triple[0]).object)),
                `Wrong Object`,
            );
        }
    }
});

test.serial('add new File to existing Dataset', async (t) => {
    let contents = null;

    try {
        const filePath = new URL('./native.rdfcsa', import.meta.url);
        contents = await readFile(filePath);
    } catch (
        /**
         * @type {*}
         */
        err
    ) {
        console.error('Failed to read Sample Data from test.nt', err.message);
    }

    const rdfStore = new RDFCSA(convertStringArrayToRDFGraphData(data.slice()), 10);

    RDFCSA.add(nonNull(contents), rdfStore);

    t.snapshot(rdfStore.execute(QUERY_ALL));
});
