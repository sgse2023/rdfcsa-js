//@ts-check
import test from 'ava';
import { RDFCSA } from '../../../js/lib/rdfstore/RDFCSA.mjs';
import { convertStringArrayToRDFGraphData } from '../../lib/rdfcsa-conversion.mjs';
import { RawTriple } from '../../../js/lib/rdfstore/RawTriple.mjs';
import { QUERY_ALL } from '../../../js/lib/query/SimpleQuery.mjs';

/**
 * @type {readonly string[]}
 */
const data = [
    'Inception',
    'filmed in',
    'L.A.',
    'L.A.',
    'city of',
    'USA',
    'E.Page',
    'appears in',
    'Inception',
    'L.DiCaprio',
    'appears in',
    'Inception',
    'J.Gordon',
    'appears in',
    'Inception',
    'J.Gordon',
    'born in',
    'USA',
    'J.Gordon',
    'lives in',
    'L.A.',
    'E.Page',
    'born in',
    'Canada',
    'L.DiCaprio',
    'born in',
    'USA',
    'L.DiCaprio',
    'awarded',
    'Oscar2015',
];

test('single delete', (t) => {
    const rdfcsa = new RDFCSA(convertStringArrayToRDFGraphData(data.slice()));
    rdfcsa.deleteTriple(new RawTriple('E.Page', 'born in', 'Canada'));
    t.snapshot(rdfcsa.execute(QUERY_ALL));
});

test('single delete (edge case so -> s)', (t) => {
    const rdfcsa = new RDFCSA(convertStringArrayToRDFGraphData(data.slice()));
    rdfcsa.deleteTriple(new RawTriple('E.Page', 'appears in', 'Inception'));
    rdfcsa.deleteTriple(new RawTriple('L.DiCaprio', 'appears in', 'Inception'));
    rdfcsa.deleteTriple(new RawTriple('J.Gordon', 'appears in', 'Inception'));
    t.snapshot(rdfcsa.execute(QUERY_ALL));
});

test('single delete (edge case so -> o)', (t) => {
    const rdfcsa = new RDFCSA(convertStringArrayToRDFGraphData(data.slice()));
    rdfcsa.deleteTriple(new RawTriple('L.A.', 'city of', 'USA'));
    t.snapshot(rdfcsa.execute(QUERY_ALL));
});
