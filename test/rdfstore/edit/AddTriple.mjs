//@ts-check
import test from 'ava';
import { RDFCSA } from '../../../js/lib/rdfstore/RDFCSA.mjs';
import { convertStringArrayToRDFGraphData } from '../../lib/rdfcsa-conversion.mjs';
import { RawTriple } from '../../../js/lib/rdfstore/RawTriple.mjs';
import { QUERY_ALL, SimpleQuery } from '../../../js/lib/query/SimpleQuery.mjs';
import { TriplePattern } from '../../../js/lib/query/TriplePattern.mjs';
import { QueryTripleElement } from '../../../js/lib/query/QueryTripleElement.mjs';

/**
 * @type {readonly string[]}
 */
const data = [
    'Inception',
    'filmed in',
    'L.A.',
    'L.A.',
    'city of',
    'USA',
    'E.Page',
    'appears in',
    'Inception',
    'L.DiCaprio',
    'appears in',
    'Inception',
    'J.Gordon',
    'appears in',
    'Inception',
    'J.Gordon',
    'born in',
    'USA',
    'J.Gordon',
    'lives in',
    'L.A.',
    'E.Page',
    'born in',
    'Canada',
    'L.DiCaprio',
    'born in',
    'USA',
    'L.DiCaprio',
    'awarded',
    'Oscar2015',
];

test('single add', (t) => {
    const rdfcsa = new RDFCSA(convertStringArrayToRDFGraphData(data.slice()));
    rdfcsa.addTriple(new RawTriple('The Last of Us', 'has genre', 'Horror'));
    rdfcsa.execute(QUERY_ALL);
    t.snapshot(
        rdfcsa.execute(
            new SimpleQuery([
                new TriplePattern(
                    new QueryTripleElement('The Last of Us', false),
                    new QueryTripleElement('has genre', false),
                    new QueryTripleElement('Horror', false),
                ),
            ]),
        ),
    );
});

test('single add (previous found bug)', (t) => {
    const rdfcsa = new RDFCSA(convertStringArrayToRDFGraphData(data.slice()));
    rdfcsa.addTriple(new RawTriple('ein', 'tolles', 'triples'));
    t.snapshot(rdfcsa.execute(QUERY_ALL));
});

test('single add (edge case s -> so)', (t) => {
    const rdfcsa = new RDFCSA(convertStringArrayToRDFGraphData(data.slice()));
    rdfcsa.addTriple(new RawTriple('L.A.', 'city of', 'L.DiCaprio'));
    t.snapshot(rdfcsa.execute(QUERY_ALL));
});

test('single add (edge case o -> so)', (t) => {
    const rdfcsa = new RDFCSA(convertStringArrayToRDFGraphData(data.slice()));
    rdfcsa.addTriple(new RawTriple('USA', 'born in', 'North America'));
    t.snapshot(rdfcsa.execute(QUERY_ALL));
});

test('double add', (t) => {
    const rdfcsa = new RDFCSA(convertStringArrayToRDFGraphData(data.slice()));
    rdfcsa.addTriple(new RawTriple('The Last of Us', 'has genre', 'Horror'));
    rdfcsa.addTriple(new RawTriple('Cat', 'eats', 'fish'));
    rdfcsa.execute(QUERY_ALL);
    t.snapshot(
        rdfcsa.execute(
            new SimpleQuery([
                new TriplePattern(
                    new QueryTripleElement('The Last of Us', false),
                    new QueryTripleElement('has genre', false),
                    new QueryTripleElement('Horror', false),
                ),
            ]),
        ),
    );
    t.snapshot(
        rdfcsa.execute(
            new SimpleQuery([
                new TriplePattern(
                    new QueryTripleElement('Cat', false),
                    new QueryTripleElement('eats', false),
                    new QueryTripleElement('fish', false),
                ),
            ]),
        ),
    );
});

test('double add (previous bug)', (t) => {
    // subject not same, predicate same, object does not matter -> query all -> hdtdict object 7 not in database
    const rdfcsa = new RDFCSA(convertStringArrayToRDFGraphData(data.slice()));
    rdfcsa.addTriple(new RawTriple('The Last of Us', 'has genre', 'Horror'));
    //rdfcsa.addTriple(new RawTriple('The Last of Us', 'has genre', 'Horror2'));
    rdfcsa.addTriple(new RawTriple('The Witcher', 'has genre', 'Fantasy'));
    //rdfcsa.addTriple(new RawTriple('The Witcher', 'has genre', 'Fantasy2'));
    rdfcsa.execute(QUERY_ALL);
    t.snapshot(
        rdfcsa.execute(
            new SimpleQuery([
                new TriplePattern(
                    new QueryTripleElement('The Last of Us', false),
                    new QueryTripleElement('has genre', false),
                    new QueryTripleElement('Horror', false),
                ),
            ]),
        ),
    );
    /*t.snapshot(
        rdfcsa.execute(
            new SimpleQuery([
                new TriplePattern(
                    new QueryTripleElement('The Last of Us', false),
                    new QueryTripleElement('has genre', false),
                    new QueryTripleElement('Horror2', false),
                ),
            ]),
        ),
    );*/
    t.snapshot(
        rdfcsa.execute(
            new SimpleQuery([
                new TriplePattern(
                    new QueryTripleElement('The Witcher', false),
                    new QueryTripleElement('has genre', false),
                    new QueryTripleElement('Fantasy', false),
                ),
            ]),
        ),
    );
    /*t.snapshot(
        rdfcsa.execute(
            new SimpleQuery([
                new TriplePattern(
                    new QueryTripleElement('The Witcher', false),
                    new QueryTripleElement('has genre', false),
                    new QueryTripleElement('Fantasy2', false),
                ),
            ]),
        ),
    );*/
});

test('double add (s+o -> so)', (t) => {
    const rdfcsa = new RDFCSA(convertStringArrayToRDFGraphData(data.slice()));
    rdfcsa.addTriple(new RawTriple('The Last of Us', 'has genre', 'Horror'));
    rdfcsa.addTriple(new RawTriple('Horror', 'is genre of', 'The Last of Us'));
    t.snapshot(rdfcsa.execute(QUERY_ALL));
});

test('double add (o+s -> so)', (t) => {
    const rdfcsa = new RDFCSA(convertStringArrayToRDFGraphData(data.slice()));
    rdfcsa.addTriple(new RawTriple('Horror', 'is genre of', 'The Last of Us'));
    rdfcsa.addTriple(new RawTriple('The Last of Us', 'has genre', 'Horror'));
    t.snapshot(rdfcsa.execute(QUERY_ALL));
});
