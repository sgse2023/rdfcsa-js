/* eslint-disable */
import test from 'ava';
import { RDFCSA } from '../../../js/lib/rdfstore/RDFCSA.mjs';
import { convertStringArrayToRDFGraphData } from '../../lib/rdfcsa-conversion.mjs';
import { RawTriple } from '../../../js/lib/rdfstore/RawTriple.mjs';
import { QUERY_ALL, SimpleQuery } from '../../../js/lib/query/SimpleQuery.mjs';
import { TriplePattern } from '../../../js/lib/query/TriplePattern.mjs';
import { QueryTripleElement } from '../../../js/lib/query/QueryTripleElement.mjs';
import {
    PsiBitVector,
    wrapIndex,
    unwrapIndex,
    wrapIndexForInsert,
} from '../../../js/lib/rdfstore/PsiBitVector.mjs';

// these test should only be run manually, because they are very time intensive

/**
 * @type {readonly string[]}
 */
const data = [
    'Inception',
    'filmed in',
    'L.A.',
    'L.A.',
    'city of',
    'USA',
    'E.Page',
    'appears in',
    'Inception',
    'L.DiCaprio',
    'appears in',
    'Inception',
    'J.Gordon',
    'appears in',
    'Inception',
    'J.Gordon',
    'born in',
    'USA',
    'J.Gordon',
    'lives in',
    'L.A.',
    'E.Page',
    'born in',
    'Canada',
    'L.DiCaprio',
    'born in',
    'USA',
    'L.DiCaprio',
    'awarded',
    'Oscar2015',
];

const validWrapUnwrap = test.macro({
    exec(t, input, expected) {
        t.timeout(60 * 1000);
        const { addedTriples, addonCapacity, origTripleCount } = input;
        const maxElementsPerType = addonCapacity + origTripleCount;
        for (let i = 0; i < maxElementsPerType; i++) {
            const wrappedIndex = wrapIndex(i, addedTriples, addonCapacity, origTripleCount);
            const unwrappedIndex = unwrapIndex(
                wrappedIndex,
                addedTriples,
                addonCapacity,
                maxElementsPerType,
            );
            t.is(unwrappedIndex, i);
        }
    },
    title(providedTitle = '', input, expected) {
        const { addedTriples, addonCapacity, origTripleCount } = input;
        return `wrap then unwrap test(addedTriples=${addedTriples}, addonCapacity=
        ${addonCapacity}, origTripleCount=${origTripleCount})`;
    },
});

const validUnwrapWrap = test.macro({
    exec(t, input, expected) {
        t.timeout(60 * 1000);
        const { addedTriples, addonCapacity, origTripleCount } = input;
        const maxElementsPerType = addonCapacity + origTripleCount;
        // subjects
        for (let i = 0; i < origTripleCount + addedTriples; i++) {
            const unwrappedIndex = unwrapIndex(i, addedTriples, addonCapacity, maxElementsPerType);
            const wrappedIndex = wrapIndex(
                unwrappedIndex,
                addedTriples,
                addonCapacity,
                origTripleCount,
            );
            t.is(wrappedIndex, i);
        }
        // predicates
        for (
            let i = addonCapacity + origTripleCount;
            i < addonCapacity + (2 * origTripleCount) + addedTriples;
            i++
        ) {
            const unwrappedIndex = unwrapIndex(i, addedTriples, addonCapacity, maxElementsPerType);
            const wrappedIndex = wrapIndex(
                unwrappedIndex,
                addedTriples,
                addonCapacity,
                origTripleCount,
            );
            t.is(wrappedIndex, i);
        }
        // objects
        for (
            let i = (2 * addonCapacity) + (2 * origTripleCount);
            i < (2 * addonCapacity) + (3 * origTripleCount) + addedTriples;
            i++
        ) {
            const unwrappedIndex = unwrapIndex(i, addedTriples, addonCapacity, maxElementsPerType);
            // if (i === 3958) {
            //     t.log(`unwrappedIndex=${unwrappedIndex}`);
            //     t.log(`maxElementsPerType=${maxElementsPerType}`);
            //     t.log(`psi: subjects[0..=${origTripleCount + addedTriples - 1}], predicates[${addonCapacity + origTripleCount}..=${addonCapacity + 2 * origTripleCount + addedTriples - 1}], objects[${2 * addonCapacity + 2 * origTripleCount}..=${2 * addonCapacity + 3 * origTripleCount + addedTriples - 1}]`)
            //     t.log(`d: subjects[0..=${origTripleCount + addedTriples - 1}], predicates[${origTripleCount + addedTriples}..=${(origTripleCount + addedTriples) * 2 - 1}], objects[${(origTripleCount + addedTriples) * 2}..=${(origTripleCount + addedTriples) * 3 - 1}]`)
            // }
            const wrappedIndex = wrapIndex(
                unwrappedIndex,
                addedTriples,
                addonCapacity,
                origTripleCount,
            );
            t.is(wrappedIndex, i);
        }
    },
    title(providedTitle = '', input, expected) {
        const { addedTriples, addonCapacity, origTripleCount } = input;
        return `unwrap then wrap test(addedTriples=${addedTriples}, addonCapacity=
        ${addonCapacity}, origTripleCount=${origTripleCount})`;
    },
});

// for (const addonCapacity of [0, 1, 2, 10, 50, 100, 150, 200, 250].values()) {
//     for (let addedTriples = 0; addedTriples < addonCapacity; addedTriples++) {
//         for (const origTriplesCount of [0, 1, 2, 10, 33, 44, 50, 53, 97, 1000, 1729].values()) {
//             if (addedTriples == 0 && origTriplesCount == 0) {
//                 continue;
//             }
//             test(validWrapUnwrap, {addedTriples: addedTriples, addonCapacity: addonCapacity, origTripleCount: origTriplesCount}, null);
//             test(validUnwrapWrap, {addedTriples: addedTriples, addonCapacity: addonCapacity, origTripleCount: origTriplesCount}, null);
//         }
//     }
// }

// test(validUnwrapWrap, {addedTriples: 249, addonCapacity: 250, origTripleCount: 1729}, null);

// test('wrapIndexForInsert', (t) => {
//     const input = {addedTriples: 0, addonCapacity: 100, origTripleCount: 10};
//         const {
//             addedTriples,
//             addonCapacity,
//             origTripleCount
//         } = input;
//         const maxElementsPerType = addonCapacity + origTripleCount;
//         for (let i = 0; i < (addedTriples + origTripleCount) * 3; i++) {
//             const wrappedIndex = wrapIndexForInsert(i, addedTriples, addonCapacity, origTripleCount);
//             if (i==11) {
//             t.log(`wrappedIndex=${wrappedIndex}`)
//             t.log(`maxElementsPerType=${maxElementsPerType}`);
//             t.log(`psi: subjects[0..=${origTripleCount + addedTriples - 1}], predicates[${addonCapacity + origTripleCount}..=${addonCapacity + 2 * origTripleCount + addedTriples - 1}], objects[${2 * addonCapacity + 2 * origTripleCount}..=${2 * addonCapacity + 3 * origTripleCount + addedTriples - 1}]`)
//             t.log(`d: subjects[0..=${origTripleCount + addedTriples - 1}], predicates[${origTripleCount + addedTriples}..=${(origTripleCount + addedTriples) * 2 - 1}], objects[${(origTripleCount + addedTriples) * 2}..=${(origTripleCount + addedTriples) * 3 - 1}]`)
//             }
//             const unwrappedIndex = unwrapIndex(wrappedIndex, addedTriples, addonCapacity, maxElementsPerType);

//             // if (i === (addedTriples + origTripleCount)) {
//             // }
//             t.is(unwrappedIndex, i, `i=${i}`);
//         }
// });

// for (const _addonCapacity of [0, 1, 2, 10, 50, 100, 150, 200, 250].values()) {
//     for (let _addedTriples = 0; _addedTriples < _addonCapacity; _addedTriples++) {
//         for (const _origTriplesCount of [0, 1, 2, 10, 33, 44, 50, 53, 97, 1000, 1729].values()) {
//             if (_addedTriples == 0 && _origTriplesCount == 0) {
//                 continue;
//             }
// test(`wrapIndexForInsert${_addonCapacity},${_addedTriples},${_origTriplesCount}`, (t) => {
//     const input = {addedTriples: _addedTriples, addonCapacity: _addonCapacity, origTripleCount:
//         _origTriplesCount};
//         const {
//             addedTriples,
//             addonCapacity,
//             origTripleCount
//         } = input;
//         const maxElementsPerType = addonCapacity + origTripleCount;
//         for (let i = 0; i < (addedTriples + origTripleCount) * 3; i++) {
//             const wrappedIndex = wrapIndexForInsert(i, addedTriples, addonCapacity, origTripleCount);
//             const unwrappedIndex = unwrapIndex(wrappedIndex, addedTriples, addonCapacity, maxElementsPerType);

//             // if (i === (addedTriples + origTripleCount)) {
//             // }
//             t.is(unwrappedIndex, i, `i=${i}`);
//         }
// });
//         }
//     }
// }
