//@ts-check
import test from 'ava';
import { RDFCSA } from '../../../js/lib/rdfstore/RDFCSA.mjs';
import { convertStringArrayToRDFGraphData } from '../../lib/rdfcsa-conversion.mjs';
import { RawTriple } from '../../../js/lib/rdfstore/RawTriple.mjs';
import { QUERY_ALL, SimpleQuery } from '../../../js/lib/query/SimpleQuery.mjs';
import { TriplePattern } from '../../../js/lib/query/TriplePattern.mjs';
import { QueryTripleElement } from '../../../js/lib/query/QueryTripleElement.mjs';

/**
 * @type {readonly string[]}
 */
const data = [
    'Inception',
    'filmed in',
    'L.A.',
    'L.A.',
    'city of',
    'USA',
    'E.Page',
    'appears in',
    'Inception',
    'L.DiCaprio',
    'appears in',
    'Inception',
    'J.Gordon',
    'appears in',
    'Inception',
    'J.Gordon',
    'born in',
    'USA',
    'J.Gordon',
    'lives in',
    'L.A.',
    'E.Page',
    'born in',
    'Canada',
    'L.DiCaprio',
    'born in',
    'USA',
    'L.DiCaprio',
    'awarded',
    'Oscar2015',
];

test('single edit (delete then add)', (t) => {
    const rdfcsa = new RDFCSA(convertStringArrayToRDFGraphData(data.slice()));
    rdfcsa.editTriple(
        new RawTriple('Inception', 'filmed in', 'L.A.'),
        new RawTriple('Inception', 'filmed in', 'L.V.'),
    );
    rdfcsa.execute(QUERY_ALL);
    t.like(
        rdfcsa
            .execute(
                new SimpleQuery([
                    new TriplePattern(
                        new QueryTripleElement('Inception', false),
                        new QueryTripleElement('filmed in', false),
                        new QueryTripleElement('L.A.', false),
                    ),
                ]),
            )
            .toPojo(),
        {
            nodes: [],
            links: [],
        },
    );
    t.snapshot(
        rdfcsa.execute(
            new SimpleQuery([
                new TriplePattern(
                    new QueryTripleElement('Inception', false),
                    new QueryTripleElement('filmed in', false),
                    new QueryTripleElement('L.V.', false),
                ),
            ]),
        ),
    );
});

test('single edit (add then delete)', (t) => {
    const rdfcsa = new RDFCSA(convertStringArrayToRDFGraphData(data.slice()));
    rdfcsa.addTriple(new RawTriple('Inception', 'filmed in', 'L.V.'));
    rdfcsa.deleteTriple(new RawTriple('Inception', 'filmed in', 'L.A.'));
    rdfcsa.execute(QUERY_ALL);
    t.like(
        rdfcsa
            .execute(
                new SimpleQuery([
                    new TriplePattern(
                        new QueryTripleElement('Inception', false),
                        new QueryTripleElement('filmed in', false),
                        new QueryTripleElement('L.A.', false),
                    ),
                ]),
            )
            .toPojo(),
        {
            nodes: [],
            links: [],
        },
    );
    t.snapshot(
        rdfcsa.execute(
            new SimpleQuery([
                new TriplePattern(
                    new QueryTripleElement('Inception', false),
                    new QueryTripleElement('filmed in', false),
                    new QueryTripleElement('L.V.', false),
                ),
            ]),
        ),
    );
});

test('single edit (but unchanged)', (t) => {
    const rdfcsa = new RDFCSA(convertStringArrayToRDFGraphData(data.slice()));
    rdfcsa.editTriple(
        new RawTriple('Inception', 'filmed in', 'L.A.'),
        new RawTriple('Inception', 'filmed in', 'L.A.'),
    );
    rdfcsa.execute(QUERY_ALL);
    t.snapshot(
        rdfcsa.execute(
            new SimpleQuery([
                new TriplePattern(
                    new QueryTripleElement('Inception', false),
                    new QueryTripleElement('filmed in', false),
                    new QueryTripleElement('L.A.', false),
                ),
            ]),
        ),
    );
});
