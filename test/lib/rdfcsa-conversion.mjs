//@ts-check
import { Triple } from '../../js/lib/rdfstore/Triple.mjs';
import { ImportTriple } from '../../js/lib/rdfstore/ImportTriple.mjs';
import { RdfGraphData } from '../../js/lib/rdfstore/RdfGraphData.mjs';
import { SubjectObject } from '../../js/lib/rdfstore/SubjectObject.mjs';
import { nonNull } from '../../js/lib/assertions.mjs';

/**
 * Converts Sting Array from Format [Subject1, Predicate1, Object1, ..., Subjectn, Predicaten, Objectn] to RDFGraph Data
 * should only be used for testing purposes, the created RDGGraphData can not be used for Visualisiation Purposes and is only intended for
 * creation of a RDFStore
 * @param {string[]} array
 * @returns {RdfGraphData}
 */
function convertStringArrayToRDFGraphData(array) {
    const subjectObject = [];
    const triples = [];
    for (let i = 0; i < array.length; i = i + 3) {
        subjectObject.push(new SubjectObject(i, nonNull(array[i])));
        subjectObject.push(new SubjectObject(i + 2, nonNull(array[i + 2])));
        triples.push(new Triple(i, i + 2, i + 1, nonNull(array[i + 1]), i));
    }

    return new RdfGraphData(subjectObject, triples);
}

/**
 * Converts a ID Array in the form of (SubjectID1, PredicateID1, ObjectId1,...SubjectIDn, PredicateIDn, ObjectIdn,) into an Array of Triples where Each Triple corresponds to 1 element
 * @param {number[]} array
 * @returns {ImportTriple[]}
 */
function convertIdArrayToTripleArray(array) {
    /**
     * @type {ImportTriple[]}
     */
    const tripleArray = [];
    for (let i = 0; i < array.length; i = i + 3) {
        tripleArray.push(
            new ImportTriple(nonNull(array[i]), nonNull(array[i + 1]), nonNull(array[i + 2])),
        );
    }
    return tripleArray;
}

export { convertStringArrayToRDFGraphData, convertIdArrayToTripleArray };
