// @ts-check
import test from 'ava';
import { createSuffixArray } from '../js/lib/SuffixArray.mjs';
import { convertIdArrayToTripleArray } from './lib/rdfcsa-conversion.mjs';
import { nonNull } from '../js/lib/assertions.mjs';

/**
 * @type {readonly number[]}
 */
const correctIDs = [
    0, 4, 1, 1, 3, 4, 2, 0, 0, 2, 2, 2, 3, 0, 0, 3, 2, 4, 3, 5, 1, 4, 0, 0, 4, 1, 3, 4, 2, 4,
];

test('createSuffixArray correct ordering Numbers', (t) => {
    const unorderedIDs = [
        0, 4, 1, 1, 3, 4, 2, 0, 0, 4, 0, 0, 3, 0, 0, 3, 2, 4, 3, 5, 1, 2, 2, 2, 4, 2, 4, 4, 1, 3,
    ];

    const orderedIDs = createSuffixArray(convertIdArrayToTripleArray(unorderedIDs));

    for (let i = 0; i < correctIDs.length; i = i + 3) {
        t.is(correctIDs[i], nonNull(orderedIDs[i / 3]).subject);
        t.is(correctIDs[i + 1], nonNull(orderedIDs[i / 3]).predicate);
        t.is(correctIDs[i + 2], nonNull(orderedIDs[i / 3]).object);
    }

    t.pass();
});
