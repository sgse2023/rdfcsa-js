// @ts-check

import test from 'ava';

import { QueryTripleElement } from '../../js/lib/query/QueryTripleElement.mjs';
import { TriplePattern } from '../../js/lib/query/TriplePattern.mjs';
import { TextQuery, parseTripleElement } from '../../js/lib/query/TextQuery.mjs';

const validTripleElement = test.macro({
    /**
     * @param {string} input
     * @param {QueryTripleElement} expected
     */
    exec(t, input, expected) {
        const element = parseTripleElement(input);
        t.deepEqual(element, expected);
    },
    /**
     * @param {QueryTripleElement} expected
     */
    // implements external interface, therefore:
    // eslint-disable-next-line default-param-last
    title(providedTitle = '', input, expected) {
        let kind;
        if (expected.isBound) {
            kind = 'bound';
        } else if (expected.isVariable) {
            kind = 'variable';
        } else {
            kind = 'unbound';
        }
        return `TripleElement:${kind} ${providedTitle}${providedTitle ? ' ' : ''}'${input}'`;
    },
});

test(validTripleElement, 'Car', new QueryTripleElement('Car', false));

test(validTripleElement, '[Manufacturer]', new QueryTripleElement('Manufacturer', true));

test(validTripleElement, '?', new QueryTripleElement(null, false));

test('Starting with Numbers', validTripleElement, '123', new QueryTripleElement('123', false));

test(
    'Contains Dots',
    validTripleElement,
    'Some.thing',
    new QueryTripleElement('Some.thing', false),
);

test('Strips Space', validTripleElement, ' Something ', new QueryTripleElement('Something', false));

// ()[], disallowed, because they would need escaping
test(
    'Allows ASCII Symbols',
    validTripleElement,
    "!*';:@&=+$/?#%-<>",
    new QueryTripleElement("!*';:@&=+$/?#%-<>", false),
);

test('Allows non-ASCII Symbols', validTripleElement, 'üöÄé', new QueryTripleElement('üöÄé', false));

/**
 * @typedef {object} ValidQueryExpectation
 * @prop {QueryTripleElement[]} variables
 * @prop {TriplePattern[]} patterns
 */

const validQuery = test.macro({
    /**
     * @param {string} input
     * @param {ValidQueryExpectation} expected
     */
    exec(t, input, expected) {
        const query = new TextQuery(input);

        const variables = query.getVariables();
        t.deepEqual(variables, expected.variables);

        const patterns = query.getTriplePatterns();
        t.deepEqual(patterns, expected.patterns);
    },
    // implements external interface, therefore:
    // eslint-disable-next-line default-param-last
    title(providedTitle = '', input, expected) {
        expected;
        return `${providedTitle} '${input}'`;
    },
});

test('Car as Subject', validQuery, '(Car, ?, ?)', {
    variables: [],
    patterns: [
        new TriplePattern(
            new QueryTripleElement('Car', false),
            new QueryTripleElement(null, false),
            new QueryTripleElement(null, false),
        ),
    ],
});

test(
    'German Manufacturers',
    validQuery,
    '(Car, manufactured by, [Manufacturer]) X ([Manufacturer], from, Germany)',
    {
        variables: [new QueryTripleElement('Manufacturer', true)],
        patterns: [
            new TriplePattern(
                new QueryTripleElement('Car', false),
                new QueryTripleElement('manufactured by', false),
                new QueryTripleElement('Manufacturer', true),
            ),
            new TriplePattern(
                new QueryTripleElement('Manufacturer', true),
                new QueryTripleElement('from', false),
                new QueryTripleElement('Germany', false),
            ),
        ],
    },
);

test('Join Symbol Casing', validQuery, '(?, ?, ?) X (?,?,?) x (?,?, ?)', {
    variables: [],
    patterns: [
        new TriplePattern(
            new QueryTripleElement(null, false),
            new QueryTripleElement(null, false),
            new QueryTripleElement(null, false),
        ),
        new TriplePattern(
            new QueryTripleElement(null, false),
            new QueryTripleElement(null, false),
            new QueryTripleElement(null, false),
        ),
        new TriplePattern(
            new QueryTripleElement(null, false),
            new QueryTripleElement(null, false),
            new QueryTripleElement(null, false),
        ),
    ],
});
