//@ts-check

import { ElementNotFoundError } from '../../js/lib/error/ElementNotFoundError.mjs';
import { TextQuery } from '../../js/lib/query/TextQuery.mjs';
import { RDFCSA } from '../../js/lib/rdfstore/RDFCSA.mjs';
import { convertStringArrayToRDFGraphData } from '../lib/rdfcsa-conversion.mjs';
import test from 'ava';

/**
 * @type {readonly string[]}
 */
const data = [
    'Inception',
    'filmed in',
    'L.A.',
    'L.A.',
    'city of',
    'USA',
    'E.Page',
    'appears in',
    'Inception',
    'L.DiCaprio',
    'appears in',
    'Inception',
    'J.Gordon',
    'appears in',
    'Inception',
    'J.Gordon',
    'born in',
    'USA',
    'J.Gordon',
    'lives in',
    'L.A.',
    'E.Page',
    'born in',
    'Canada',
    'L.DiCaprio',
    'born in',
    'USA',
    'L.DiCaprio',
    'awarded',
    'Oscar2015',
];
/**
 * RDFCSA uses internal mutable state for queries.
 * Thus each test needs its own instance, to prevent race conditions.
 */
const rdfcsa = () => new RDFCSA(convertStringArrayToRDFGraphData(data.slice()));

const SingleTripleQueryFound = new TextQuery('(Inception,filmed in,L.A.)');
const SingleTripleQueryNotFound = new TextQuery('(Inception,filmed in,Minden)');
const OneBoundQueryFound = new TextQuery('(Inception,?,?)');
const TwoBoundQueryFound = new TextQuery('(?,born in,USA)');
const TwoBoundQueryNotFound = new TextQuery('(E.Page,?,USA)');
const FullGraphQuery = new TextQuery('(?,?,?)');
const BasicJoinNotFoundQuery = new TextQuery('(Inception,filmed in,[x]) X ([x],born in,USA)');

const TripleJoinDifferentElementQuery = new TextQuery(
    '(L.DiCaprio,appears in,[x]) X ([x],filmed in,[y]) X ([y],city of,USA)',
);
const TripleJoinQuery = new TextQuery(
    '([x],filmed in,L.A.) X (L.DiCaprio,appears in,[x]) X (E.Page,appears in,[x])',
);
const BasicJoinDifferentElementQuery = new TextQuery('([x],filmed in,L.A.) X (?,appears in,[x])');
const BasicJoinQuery = new TextQuery('([x],appears in,Inception) X ([x],born in,USA)');

test('SingleTripleFound ', (t) => {
    t.snapshot(rdfcsa().execute(SingleTripleQueryFound));
});

test('SingleTripleNotFound ', (t) => {
    t.throws(() => rdfcsa().execute(SingleTripleQueryNotFound), {
        instanceOf: ElementNotFoundError,
    });
});

test('OneBoundFound ', (t) => {
    t.snapshot(rdfcsa().execute(OneBoundQueryFound));
});

test('TwoBoundFound ', (t) => {
    t.snapshot(rdfcsa().execute(TwoBoundQueryFound));
});

test('TwoBoundNotFound ', (t) => {
    t.snapshot(rdfcsa().execute(TwoBoundQueryNotFound));
});

test('FullGraph', (t) => {
    t.snapshot(rdfcsa().execute(FullGraphQuery));
});

test('BasicJoin', (t) => {
    const BasicJoinQuery = new TextQuery('([x],appears in,Inception) X ([x],born in,USA)');
    t.snapshot(rdfcsa().execute(BasicJoinQuery));
});

test('BasicJoinNotFound', (t) => {
    t.snapshot(rdfcsa().execute(BasicJoinNotFoundQuery));
});

test('BasicJoinDifferentElement', (t) => {
    t.snapshot(rdfcsa().execute(BasicJoinDifferentElementQuery));
});

test('BasicJoinMerge', (t) => {
    // eslint-disable-next-line no-undefined
    t.snapshot(rdfcsa().executeMergeJoin(BasicJoinQuery, undefined));
});

test('BasicJoinNotFoundMerge', (t) => {
    // eslint-disable-next-line no-undefined
    t.snapshot(rdfcsa().executeMergeJoin(BasicJoinNotFoundQuery, undefined));
});

test('BasicJoinLeftChaining', (t) => {
    // eslint-disable-next-line no-undefined
    t.snapshot(rdfcsa().executeLeftChainingJoin(BasicJoinQuery, undefined));
});

test('BasicJoinNotFoundLeftChaining', (t) => {
    // eslint-disable-next-line no-undefined
    t.snapshot(rdfcsa().executeLeftChainingJoin(BasicJoinNotFoundQuery, undefined));
});

test('BasicJoinDifferentElementLeftChaining', (t) => {
    // eslint-disable-next-line no-undefined
    t.snapshot(rdfcsa().executeLeftChainingJoin(BasicJoinDifferentElementQuery, undefined));
});

test('BasicJoinRightChaining', (t) => {
    // eslint-disable-next-line no-undefined
    t.snapshot(rdfcsa().executeRightChainingJoin(BasicJoinQuery, undefined));
});

test('BasicJoinNotFoundRightChaining', (t) => {
    // eslint-disable-next-line no-undefined
    t.snapshot(rdfcsa().executeRightChainingJoin(BasicJoinNotFoundQuery, undefined));
});

test('BasicJoinDifferentElementRightChaining', (t) => {
    // eslint-disable-next-line no-undefined
    t.snapshot(rdfcsa().executeRightChainingJoin(BasicJoinDifferentElementQuery, undefined));
});

test('BoundElementNotFound', (t) => {
    const rdfcsa = new RDFCSA(convertStringArrayToRDFGraphData(['a', 'has', '1', 'b', 'has', '2']));
    t.throws(() => rdfcsa.execute(new TextQuery('(?,is,?)')), { instanceOf: ElementNotFoundError });
    t.throws(() => rdfcsa.execute(new TextQuery('(?,?,c)')), { instanceOf: ElementNotFoundError });
    t.throws(() => rdfcsa.execute(new TextQuery('(c,?,?)')), { instanceOf: ElementNotFoundError });
});

test('TripleJoinDifferentElement', (t) => {
    t.snapshot(rdfcsa().execute(TripleJoinDifferentElementQuery));
});

test('TripleJoin', (t) => {
    t.snapshot(rdfcsa().execute(TripleJoinQuery));
});
