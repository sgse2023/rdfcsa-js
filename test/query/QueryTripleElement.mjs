// @ts-check

import test from 'ava';

import { QueryTripleElement } from '../../js/lib/query/QueryTripleElement.mjs';

/**
 * @typedef {object} KindExpectation
 * @prop {boolean} isVariable
 * @prop {boolean} isBound
 */

const elementKind = test.macro({
    /**
     * @param {QueryTripleElement} input
     * @param {KindExpectation} expected
     */
    exec(t, input, expected) {
        t.deepEqual({ isVariable: input.isVariable, isBound: input.isBound }, expected);
    },
    title(providedTitle, input, expected) {
        input;
        expected;
        return `ElementKind:${providedTitle}`;
    },
});

test('bound', elementKind, new QueryTripleElement('Car', false), {
    isVariable: false,
    isBound: true,
});

test('unbound', elementKind, new QueryTripleElement(null, false), {
    isVariable: false,
    isBound: false,
});

test('variable', elementKind, new QueryTripleElement('Manufacturer', true), {
    isVariable: true,
    isBound: false,
});
