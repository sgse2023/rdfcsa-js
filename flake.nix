{
  inputs.flake-compat = {
    url = "github:edolstra/flake-compat";
    flake = false;
  };

  inputs.nixpkgs.url = "github:NixOs/nixpkgs/nixos-unstable";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  inputs.nix-filter.url = "github:numtide/nix-filter";

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    ...
  } @ inputs:
    flake-utils.lib.eachSystem flake-utils.lib.allSystems (
      system: let
        pkgs = import nixpkgs {inherit system;};
      in rec {
        formatter = pkgs.alejandra;

        packages.default = pkgs.callPackage ./nix/packages/rdfcsa-js.nix {
          nix-filter = import inputs.nix-filter;
        };

        packages.docker = pkgs.callPackage ./nix/packages/docker.nix {
          rdfcsa-js = packages.default;
        };

        packages.prefetch-npm-deps = pkgs.prefetch-npm-deps;

        devShells.default = pkgs.callPackage ./nix/dev-shells/rdfcsa-js.nix {
          rdfcsa-js = packages.default;
        };

        devShells.ci = pkgs.callPackage ./nix/dev-shells/ci.nix {
          rdfcsa-js = packages.default;
          nix-filter = import inputs.nix-filter;
        };
      }
    );
}
