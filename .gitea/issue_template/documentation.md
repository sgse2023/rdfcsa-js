---
name: 'Report Documentation Issues'
about: 'Template for documentation changes.'
title: '[<Component>] <Topic Summary>'
labels:
    - 'status/todo'
    - 'kind/documentation'
---

<!--
- To make content visible, remove the html comment markers
- Don't forget to assign a responsible person
- Add this issue to the correct milestone
- Feel free to leave sections empty, if you don't have reasonable input for it
-->

# What?

<!-- What needs to be done? -->

# Why?

<!-- Why does it need to be done? -->

# Sources of Confusion

<!-- Topics that need clarification -->
