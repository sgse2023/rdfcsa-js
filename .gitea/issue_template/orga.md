---
name: 'Report an organizational issue'
about: 'Template for everything related to project organisation.'
title: '<Summary>'
labels:
    - 'status/todo'
    - 'kind/orga'
---

<!--
- To make content visible, remove the html comment markers
- Don't forget to assign a responsible person
- Add this issue to the correct milestone
- Feel free to leave sections empty, if you don't have reasonable input for it
-->

# What

<!-- What needs to be done? -->

# Why

<!-- Why do we want to this, i.e. what is the motivation? -->
