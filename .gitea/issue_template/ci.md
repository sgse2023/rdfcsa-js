---
name: 'Report a Tooling Issue'
about: 'Template for everything related to ci, linting or building.'
title: '[Component] <Problem Summary>'
labels:
    - 'status/todo'
    - 'kind/ci'
---

<!--
- To make content visible, remove the html comment markers
- Don't forget to assign a responsible person
- Add this issue to the correct milestone
- Feel free to leave sections empty, if you don't have reasonable input for it
-->
