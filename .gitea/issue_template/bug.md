---
name: 'Report a Bug'
about: 'Template for reporting bugs.'
title: '[<Component>] <Bug Summary>'
labels:
    - 'status/todo'
    - 'kind/bug'
---

<!--
- To make content visible, remove the html comment markers
- Don't forget to assign a responsible person
- Add this issue to the correct milestone
- Feel free to leave sections empty, if you don't have reasonable input for it
-->

# Reproduction

<!-- how to cause this issue to appear -->

# Possible Solution(s)

<!-- ideas for fixing the problem -->

# Workarounds

<!-- how to work around the bug (e.g. when testing other stuff) -->

# Sources of Confusion

<!-- Topics that need clarification -->

<hr>

<!--Caused-by: !xyz-->
