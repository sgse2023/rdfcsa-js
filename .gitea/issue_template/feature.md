---
name: 'Request a Feature'
about: 'Template for (new) features.'
title: '[<Component>] <Feature Summary>'
labels:
    - 'status/todo'
    - 'kind/feature'
---

<!--
- To make content visible, remove the html comment markers
- Don't forget to assign a responsible person
- Add this issue to the correct milestone
- Feel free to leave sections empty, if you don't have reasonable input for it
-->

# Acceptance Criteria

<!-- When is this feature 'ready'? -->

# Implementation idea(s)

<!-- ideas for implementing the feature -->

# Alternatives

<!--
    if we do not need to have this exact feature implemented,
    what are the alternatives
-->

# Sources of Confusion

<!-- Topics that need clarification -->

<hr>

<!--Follow-up of: !xyz-->
