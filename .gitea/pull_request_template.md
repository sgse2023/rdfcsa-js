---
name: 'PR Template'
about: 'Default content for PR descriptions'
title: '[<Component>] <Issue title or PR topic>'
labels:
    - 'status/acceptance test'
---

<!--
- To make content visible, remove the html comment markers
- Don't forget to assign a responsible person
- Add this issue to the correct milestone
- Feel free to leave sections empty, if you don't have reasonable input for it
-->

# Goal

<!-- what is the problem this PR solves -->

# Method of implementation

<!-- noteworthy implementation notes -->

# Sources of confusion

<!-- Topics that need clarification -->

<hr>

Closes: #

<!--Follow-up of: !xyz-->

(to be continously updated by assignee)
